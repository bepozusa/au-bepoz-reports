﻿using System;

using Xamarin.Forms;

namespace bepoz_reports.Common
{
	public class ConstantManager
	{
		public static int login_main_logo_XConstraint = Xamarin.Forms.Device.OnPlatform<int> (-100, -100, -100);
		public static int remember_me_label_XConstraint = Xamarin.Forms.Device.OnPlatform<int> (-120, -120, -100);
		public static int error_label_XConstant = Xamarin.Forms.Device.OnPlatform<int> (-90, -60, -90);
		public static int error_label_YConstant = Xamarin.Forms.Device.OnPlatform<int> (50, 70, 50);

		public ConstantManager ()
		{
		}
	}
}

