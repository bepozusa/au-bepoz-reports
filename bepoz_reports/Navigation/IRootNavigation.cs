﻿using System;

using Xamarin.Forms;

using bepoz_reports.PageModels;

namespace bepoz_reports.Navigation
{
    public interface IRootNavigation 
    {
        void PushPage(Page page, BasePageModel model);

        void PopPage();
    }
}

