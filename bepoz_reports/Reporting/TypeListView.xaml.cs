﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.ViewModels;
using bepoz_reports.Controls;
using bepoz_reports.Mvvm;
using bepoz_reports.Models;
using bepoz_reports.Manager;

namespace bepoz_reports
{
	public partial class TypeListView : ContentView
	{
		TypeListViewModel ViewModel = new TypeListViewModel();

		private SearchBar searchBar;

		private bool searchBarAdded = false;

		public TypeListView ()
		{
			InitializeComponent ();

			BindingContext = ViewModel;
		
			item_list.GroupHeaderTemplate = new DataTemplate(typeof(HeaderViewCell));
			item_list.ItemTemplate = new DataTemplate(typeof(TillViewCell));

			// lesson item when selected navigates to lesson page
			item_list.ItemTapped += (sender, e) => 
			{
				switch (NavManager.Type)
				{
					case 0:
						App.BankingManager.InitGridModel ();
						App.BankingManager.CurrentItem = (e.Item as BankingItem);
						break;
					case 1:
						App.DiscountsManager.InitGridModel ();
						App.DiscountsManager.CurrentItem = (e.Item as DiscountsItem);
						break;
					case 2:
						App.DailyTotalsManager.InitGridModel ();
						App.DailyTotalsManager.CurrentItem = (e.Item as DailyTotalsItem);
						break;
				}

				App.Navigation.PushAsync (ViewFactory.CreatePage<ReportSummaryViewModel> ());
			};
		}
	}
}

