﻿using System;
using System.Diagnostics;

using Xamarin.Forms;

using bepoz_reports.Pages;
using bepoz_reports;
using bepoz_reports.Services.Serialization;
using bepoz_reports.Mvvm;
using bepoz_reports.Services.DI;
using bepoz_reports.XForms;
using bepoz_reports.ViewModels;
using bepoz_reports.Manager;
using bepoz_reports.DeviceProperties;

namespace bepoz_reports
{
	/// <summary>
	/// Class App.
	/// </summary>
	public class App
	{
		public static INavigation Navigation;
		public static IDevice Device = Resolver.Resolve<IDevice>();
		public static DashboardManager DashboardManager = new DashboardManager();
		public static BankingManager BankingManager = new BankingManager();
		public static DailyTotalsManager DailyTotalsManager = new DailyTotalsManager();
		public static DiscountsManager DiscountsManager = new DiscountsManager();

		/// <summary>
		/// Initializes the application.
		/// </summary>
		public static void Init ()
		{

			var app = Resolver.Resolve<IXFormsApp> ();
			if (app == null) {
				return;
			}
		
			app.Closing += (o, e) => Debug.WriteLine ("Application Closing");
			app.Error += (o, e) => Debug.WriteLine ("Application Error");
			app.Initialize += (o, e) => Debug.WriteLine ("Application Initialized");
			app.Resumed += (o, e) => Debug.WriteLine ("Application Resumed");
			app.Rotation += (o, e) => Debug.WriteLine ("Application Rotated");
			app.Startup += (o, e) => Debug.WriteLine ("Application Startup");
			app.Suspended += (o, e) => Debug.WriteLine ("Application Suspended");
		}

		/// <summary>
		/// Gets the main page.
		/// </summary>
		/// <returns>The Main Page.</returns>
		public static Xamarin.Forms.Page GetMainPage ()
		{
			AccountManager.InitCurrentUserInfo ();

			// Register our views with our view models
			ViewFactory.Register<LoginPage, LoginViewModel>();
			ViewFactory.Register<DashboardPage, DashboardViewModel>();
			ViewFactory.Register<RootPage, RootViewModel>();
			ViewFactory.Register<MenuPage, MenuViewModel> ();
			ViewFactory.Register<ReportDisplayPage, ReportDisplayViewModel>();
			ViewFactory.Register<ReportSettingsPage, ReportSettingsViewModel>();
			ViewFactory.Register<VenueSettingsPage, VenueSettingsViewModel>();
			ViewFactory.Register<StoreSettingsPage, StoreSettingsViewModel>();
			ViewFactory.Register<ReportSummaryPage, ReportSummaryViewModel>();

			NavigationPage navPage = new NavigationPage (ViewFactory.CreatePage<RootViewModel> ());
			Navigation = navPage.Navigation;

			return navPage;
		}
	}
}
