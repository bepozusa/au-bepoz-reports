﻿using System;
using System.Net;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace bepoz_reports.WebServices
{
	public static class HttpGetRequest
	{
		public static bool requestCompleted;

		// Define other methods and classes here
		public static Task<string> CreateRequest(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json; charset=utf-8";

			Task<WebResponse> task = Task.Factory.FromAsync(request.BeginGetResponse,
				asyncResult => request.EndGetResponse(asyncResult),
				(object)null);

			return task.ContinueWith(t => ReadStreamFromResponse(t.Result));
		}

		private static string ReadStreamFromResponse(WebResponse response)
		{
			using (Stream responseStream = response.GetResponseStream())

			using (StreamReader sr = new StreamReader(responseStream))
			{
				//Need to return this response 
				string strContent = sr.ReadToEnd();

				return strContent;
			}
		}
	}
}