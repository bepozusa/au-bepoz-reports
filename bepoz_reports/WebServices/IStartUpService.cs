using System;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;

namespace bepoz_reports.Services
{
	public interface IStartUpService
    {
		Task<string> Start();
    }
}