﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using bepoz_reports.Helpers;

namespace bepoz_reports.DeviceProperties
{
    public interface ISensor
    {
        event EventHandler<EventArgs<Vector3>> ReadingAvailable;

        Vector3 LatestReading { get; }

        AccelerometerInterval Interval { get; set; }
    }
}
