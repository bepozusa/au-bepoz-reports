﻿using System;
using System.Globalization;

using Xamarin.Forms;

namespace bepoz_reports
{
	public class DoubleConverter : IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			double theDouble = (double)value;
			return theDouble.ToString ();
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			string strValue = value as string;

			if (string.IsNullOrEmpty (strValue))
				return 0;

			double resultDouble;

			if (double.TryParse (strValue, out resultDouble)) {
				return resultDouble;
			}

			return 0;
		}
	}
}

