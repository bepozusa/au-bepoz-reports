﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Mvvm;
using bepoz_reports.ViewModels;
using bepoz_reports.Services;
using bepoz_reports.Manager;
using bepoz_reports.Controls;
using System.Threading.Tasks;

namespace bepoz_reports.Pages
{	
	public partial class LoginPage : ContentPage
	{	
		public bool keyboardIsShowing = false;

		private Timer bannerTimer;

		private List<Image> Banners = new List<Image> ();
		private List<string> fileNames = new List<string> () {"banner2.png", "banner1.png", "banner2.png"};

		private int numberOfImages = 3, bannerIndex = 0;

		public LoginPage ()
		{
			InitializeComponent ();

			Title = "";

			NavigationPage.SetHasNavigationBar (this, true);

			switch (DeviceManager.DeviceType) {
				case 0:
					login_button.BackgroundColor = Color.Gray;
					break;
				case 1:
					break;
			}

			UsernameEntry.Completed += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("Enter pressed on username entry.");
			};

			// if enter pressed
			PasswordEntry.Completed += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("Enter pressed on password, trying to login...");

				login();
			};

			initBannerLists ();
		}

		private void initBannerLists()
		{
			Banners.Add (banner_1);

			for (int i = 0; i < numberOfImages; i++) {
				Banners.Add (createImageTemplate (fileNames[i]));
			}
		}

		private void createToolBarItems()
		{
			ToolbarItem settingsButton = new ToolbarItem ("settings", "settings.png", async () => {
				System.Diagnostics.Debug.WriteLine ("Report settings pressed.");

				App.Navigation.PushAsync(ViewFactory.CreatePage<ReportSettingsViewModel> ());
			});

			ToolbarItems.Add(settingsButton);
		}

		private Image createImageTemplate(string source)
		{
			Image image_template = new Image(){Source = source};
			image_template.Opacity = 0.0f;

			image_relative_layout.Children.Add (image_template,
				Constraint.RelativeToParent((parent) =>
					{
						return parent.X;
					}),
				Constraint.RelativeToParent((parent) =>
					{
						return parent.Y;
					}),
				Constraint.RelativeToParent((parent) =>
					{
						return parent.Width;
					}));

			return image_template;
		}

		private async void login()
		{
			ErrorLabel.Opacity = 0;
			ErrorLabel.Text = "";

			if (UsernameEntry.Text.Length == 0)
				ErrorLabel.Text = "Please enter username.";

			else if (PasswordEntry.Text.Length == 0)
				ErrorLabel.Text = "Please enter password.";

			// if any issues with entries
			if (ErrorLabel.Text.Length > 0) {
				ErrorLabel.Opacity = 1;
				return;
			}

			activity_view.IsVisible = true;

			try
			{
				System.Diagnostics.Debug.WriteLine("Login successful.");

				login_button.IsEnabled = false;
				ServiceManager.AccessToken = await ServiceManager.LoginService.Login(UsernameEntry.Text, PasswordEntry.Text);
				ServiceManager.IsLoggedIn = true;
				AccountManager.Username = UsernameEntry.Text;

				if (!TaskManager.StoresVenuesLoaded) 
				{
					// user info retrieval http request
					System.Diagnostics.Debug.WriteLine("Starting task to retrieve user info.");

					TaskManager.HTTPRequestTask = AccountManager.CreateUserInfoRequest ();
					await TaskManager.HTTPRequestTask;

					System.Diagnostics.Debug.WriteLine("Finished task to retrieve user info.");

					loadStoresAndVenues ();
				}

				activity_view.IsVisible = false;

				NavManager.RootPage.IsGestureEnabled = true;
				NavManager.Visible = true;
				NavManager.LoggedIn = true;

				return;
			}
			catch
			{
				System.Diagnostics.Debug.WriteLine("Login failed.");

				ServiceManager.AccessToken = "";
				ServiceManager.IsLoggedIn = false;
				activity_view.IsVisible = false;

				ErrorLabel.Opacity = 1;
				ErrorLabel.Text = "Invalid login details.";
			}
			finally
			{
				login_button.IsEnabled = true;
			}				
		}

		private async void loadStoresAndVenues()
		{
			System.Diagnostics.Debug.WriteLine ("Starting task to venues and stores.");

			// function creates two requests to retrieve venues and stores information
			TaskManager.TaskComplete = false;
		
			// store retrieval http request
			TaskManager.HTTPRequestTask = StoreManager.CreateStoreDataRequest ();
			await TaskManager.HTTPRequestTask;

			// venue retrieval http request
			TaskManager.HTTPRequestTask = VenueManager.CreateVenueDataRequest ();
			await TaskManager.HTTPRequestTask;

			TaskManager.TaskComplete = true;
			TaskManager.StoresVenuesLoaded = true;

			System.Diagnostics.Debug.WriteLine ("Finished task to retrieve venues and stores.");
		}

		public async void LoginClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Login pressed.");

			login ();
		}

		private async void animateBetweenImages(Image fromImage, Image toImage)
		{
			System.Diagnostics.Debug.WriteLine ("Animating between images.");

			fromImage.FadeTo (0.0f, 200);
			await toImage.FadeTo (1.0f, 200);
		}

		private void resetAnimateLoop()
		{
			Banners [(Banners.Count - 1)].Opacity = 0;
			Banners [0].Opacity = 1;
			bannerIndex = 0;

			System.Diagnostics.Debug.WriteLine ("Reset animate loop.");
		}

		public void AnimateBanner(object args)
		{
			if (bannerIndex == (Banners.Count - 1)) {
				resetAnimateLoop ();
				return;
			}

			System.Diagnostics.Debug.WriteLine ("Changing to next banner from current banner " + bannerIndex);

			// fade to next banner
			//banner_1.FadeTo (0, 400);
			//Banners [bannerIndex].FadeTo (0.0f);
			//Banners [bannerIndex + 1].FadeTo (1.0f);

			//animateBetweenImages (Banners [bannerIndex], Banners [bannerIndex + 1]);
			Banners [bannerIndex].Opacity = 0;
			Banners [bannerIndex + 1].Opacity = 1;

			//Banners[bannerIndex].LayoutTo (new Rectangle (Banners[bannerIndex].Bounds.X - Banners[bannerIndex].Bounds.Width, Banners[bannerIndex].Bounds.Y, Banners[bannerIndex].Bounds.Width, Banners[bannerIndex].Bounds.Height), 150, null);
			//Banners[bannerIndex + 1].LayoutTo (new Rectangle (Banners[bannerIndex + 1].Bounds.X - Banners[bannerIndex + 1].Bounds.Width, Banners[bannerIndex - 1].Bounds.Y, Banners[bannerIndex - 1].Bounds.Width, Banners[bannerIndex - 1].Bounds.Height), 150, null);
			bannerIndex++;
		}

		override protected void OnAppearing ()
		{
			System.Diagnostics.Debug.WriteLine ("Login page appearing.");

			Banners [0].FadeTo (1.0f, 400);

			// start timer for changing banner images
			if(bannerTimer == null)
				bannerTimer = new Timer(AnimateBanner, Banners, 5000, 5000);

			if (AccountManager.RememberUsername)
				UsernameEntry.Text = AccountManager.Username;
		}

		override protected void OnDisappearing ()
		{
			System.Diagnostics.Debug.WriteLine ("Login page disappearing.");

			// stop timer for changing banner images
			bannerTimer.Dispose();
			bannerTimer = null;
		}
	}
}

