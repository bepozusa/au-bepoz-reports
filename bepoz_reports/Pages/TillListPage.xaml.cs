﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.ViewModels;
using bepoz_reports.Report;
using bepoz_reports.Controls;
using bepoz_reports.Models;
using bepoz_reports.Banking;
using bepoz_reports.Tasks;
using System.Threading.Tasks;
using bepoz_reports.Mvvm;
using bepoz_reports.Device;
using bepoz_reports.Nav;

namespace bepoz_reports.Pages
{	
	public partial class TillListPage : ContentPage
	{	
		public TypeListViewModel ViewModel = new TypeListViewModel();

		private SearchBar searchBar;

		private bool searchBarAdded = false;

		private LoadingView loadingView;
		private NoDataView noDataView;
		private View mainView;

		public TillListPage()
		{
			InitializeComponent ();

			BindingContext = ViewModel;

			loadingView = new LoadingView ();
			noDataView = new NoDataView();

			mainView = Content;
			Content = loadingView;

			// set icon for bottom tab based on device type
			switch (DeviceManager.DeviceType) {
				case 0:
					break;
				case 1:
					Icon = "till.png";
					break;
			}


			Title = ReportManager.CurrentPeriod.Title;

			till_list.GroupHeaderTemplate = new DataTemplate(typeof(HeaderViewCell));
			till_list.ItemTemplate = new DataTemplate(typeof(TillViewCell));

			// lesson item when selected navigates to lesson page
			till_list.ItemSelected += (sender, e) => {
				//NavManager.CurrentTitle = (e.SelectedItem as BankingItem).Till;

				//App.Navigation.PushAsync(ViewFactory.CreatePage<GraphSelectionViewModel> ());
			};
		}

		private async Task<bool> WaitToLoadAsync()
		{
			Task<bool> work = Task<bool>.Factory.StartNew (() => {
				while (!TaskManager.TaskComplete){}

				return true;
			});

			await work;

			loadPage ();

			return true;
		}

		private void loadPage()
		{
			System.Diagnostics.Debug.WriteLine("Displaying content for till list page.");

			if (DataManager.Items.Count > 0) {
				Content = mainView;
			} else
				Content = noDataView;
		}

		private void createToolBarItems()
		{
			/*ToolbarItems.Add(new ToolbarItem("search", "search.png", async () =>
				{
					System.Diagnostics.Debug.WriteLine ("Lesson search button pressed.");

					if (searchBarAdded)
						library_stack.Children.Remove(searchBar);
					else
						library_stack.Children.Insert(0, searchBar);

					searchBarAdded = !searchBarAdded;

					library_stack.ForceLayout();
				} ));*/
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			System.Diagnostics.Debug.WriteLine ("Till list page appeared.");

			createToolBarItems ();

			if (!TaskManager.TaskComplete)
				WaitToLoadAsync ();
			else {
				loadPage ();
			}
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			System.Diagnostics.Debug.WriteLine ("Till list page disappeared.");

			ToolbarItems.Clear ();
		}
	}
}
