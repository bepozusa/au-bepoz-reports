﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.ViewModels;
using bepoz_reports.Mvvm;
using bepoz_reports.Manager;

namespace bepoz_reports.Pages
{	
	public partial class RootPage : MasterDetailPage
	{	
		RootViewModel ViewModel = new RootViewModel ();

		public Action<bool> LoginChanged;

		public RootPage ()
		{
			InitializeComponent ();

			ViewModel = BindingContext as RootViewModel;

			NavigationPage.SetHasNavigationBar (this, false);

			NavManager.RootPage = this;
			TaskManager.TaskComplete = false;

			bool showNavBar = false;

			switch (DeviceManager.DeviceType) {
				case 0:
					showNavBar = true;
					break;
				case 1:
					showNavBar = false;
					break;
			}
				
			NavManager.LoginChanged += (s, e) => 
			{
				System.Diagnostics.Debug.WriteLine("Logged in as " + AccountManager.CurrentUserInfo.FirstName);

				switchDetailView(true);
			};
				
			Master = ViewFactory.CreatePage<MenuViewModel> ();

			switch (DeviceManager.DeviceType) 
			{
				case 0:
					Detail = ViewFactory.CreatePage<LoginViewModel> ();
					break;
				case 1:
					Detail = new NavigationPage(ViewFactory.CreatePage<LoginViewModel> ());
					break;
			}

			(Master as MenuPage).Menu.ItemTapped += (sender, args) => {
				if (!NavManager.LoggedIn) {
					DisplayAlert ("Bepoz Reports", "Not logged in.", "OK");
					return;
				}
					
				if (args == null)
					return; 

				NavManager.DisplayReport = false;
				MenuItem selectedItem = (MenuItem)args.Item;

				if (selectedItem.Title.Contains("Nett Sales") || selectedItem.Title.Contains("Sales Type"))
					return;

				TaskManager.ReportType = selectedItem.ReportType;

				System.Diagnostics.Debug.WriteLine ("Menu selected - " + selectedItem.Title);

				setCurrentReport (selectedItem);

				bool isWelcome = (selectedItem.Title.Contains("Dashboard"));

				switchDetailView (isWelcome);

				IsPresented = false;
			};
		}

		private void switchDetailView(bool isWelcome)
		{
			Page detailPage;

			if (isWelcome) {
				if (NavManager.LoggedIn)
					detailPage = ViewFactory.CreatePage<DashboardViewModel> ();
				else
					detailPage = ViewFactory.CreatePage<LoginViewModel> ();
			}
			else
				detailPage = ViewFactory.CreatePage<ReportDisplayViewModel> ();

			switch (DeviceManager.DeviceType) 
			{
				case 0:
					Detail = detailPage;
					break;
				case 1:
					Detail = new NavigationPage(detailPage);
					break;
			}
		}
			
		public void setCurrentReport(MenuItem menuItem)
		{
			NavManager.CurrentTitle = menuItem.Title;
		}

		override protected void OnAppearing ()
		{
			System.Diagnostics.Debug.WriteLine ("Root page appearing.");

			if (ServiceManager.IsLoggedIn && NavManager.DisplayReport)
				switchDetailView (false);
				
			IsGestureEnabled = true;

			base.OnAppearing ();
		}
	}
}