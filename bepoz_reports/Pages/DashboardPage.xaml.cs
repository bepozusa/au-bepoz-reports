﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Manager;
using bepoz_reports.ViewModels;
using bepoz_reports.Mvvm;

namespace bepoz_reports.Pages
{	
	public partial class DashboardPage : ContentPage
	{	
		DashboardViewModel ViewModel = new DashboardViewModel();

		private bool animating = false, showingComboBoxView = false, dateChanged = false;

		private double combo_layout_startY = 0f;

		private ToolbarItem settingsButton;

		public DashboardPage ()
		{
			InitializeComponent ();
			//createToolBarItems ();

			BindingContext = ViewModel;

			combo_layout_startY = combo_layout.Bounds.Y;

			combo_box_date_view.DateSelected += (sender, e) => {
				System.Diagnostics.Debug.WriteLine ("Date changed on dashboard.");

				dateChanged = true;
			};

			SetLabels ();
		}

		private void SetLabels()
		{
			username_label.Text = String.Format("Hi {0}", AccountManager.CurrentUserInfo.FirstName);
			organisation_label.Text = AccountManager.CurrentUserInfo.OrganisationName;
		}

		private void createToolBarItems()
		{
			settingsButton = new ToolbarItem ("settings", "settings.png", async () => {
				if (showingComboBoxView)
					return;

				System.Diagnostics.Debug.WriteLine ("Report settings pressed.");

				App.Navigation.PushAsync(ViewFactory.CreatePage<ReportSettingsViewModel> ());
			});

			ToolbarItems.Add(settingsButton);
		}

		public void RefreshClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Refreshed pressed.");

			startRequest ();
		}

		public void DoneClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Done pressed on combo box view.");

			ReportManager.SetDateText ();

			if (ReportManager.CurrentDate != null)
				time_button.Text = ReportManager.CurrentDate.ToUpper();
				
			showingComboBoxView = false;

			if (NavManager.DateChanged) {
				animateComboBoxView (-combo_layout_startY, 0);
				NavManager.DateChanged = false;
			}
			else
				animateComboBoxView(-combo_layout.Bounds.Height, 0);

			// if we are notified of date change, perform another request
			if (dateChanged)
				startRequest ();
		}

		public void TimeClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Time pressed on combo box view.");

			if (showingComboBoxView)
				return;

			showingComboBoxView = true;

			animateComboBoxView(combo_layout.Bounds.Height, 0.3f);
		}

		private void animateComboBoxView(double movement, float opacity)
		{
			if (animating)
				return;
				
			animating = true;
			black_overlay_view.FadeTo (opacity, 50);
			combo_layout.LayoutTo(new Rectangle (combo_layout.Bounds.X, combo_layout.Bounds.Y - movement, combo_layout.Bounds.Width, combo_layout.Bounds.Height), 150, null);

			animating = false;
		}

		private async void startRequest()
		{
			net_figure_label.Text = "Retrieving Nett...";
			activity_view.IsVisible = true;

			ViewModel.SetLoadingValues ();
			BindingContext = ViewModel;

			TaskManager.TaskComplete = false;
			TaskManager.HTTPRequestTask = App.DashboardManager.CreateDataRequest ("Dashboard");
			await TaskManager.HTTPRequestTask;
			TaskManager.TaskComplete = true;

			activity_view.IsVisible = false;

			App.DashboardManager.CalculateTotalFigures();

			if (App.DashboardManager.TotalDashboardItem.NettTotal > 0) {
				net_figure_label.Text = App.DashboardManager.TotalDashboardItem.NettTotal.ToString ("#,##0");
				ViewModel.AssignValues (App.DashboardManager.TotalDashboardItem);
			}
			else
				net_figure_label.Text = "No data available";

			BindingContext = ViewModel;

			System.Diagnostics.Debug.WriteLine ("Task manager dashboard request complete.");
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			System.Diagnostics.Debug.WriteLine ("Dashboard page appeared.");

			NavManager.OnDashboardPage = true;
			time_button.Text = ReportManager.CurrentDate.ToUpper();

			startRequest ();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			System.Diagnostics.Debug.WriteLine ("Dashboard page disappeared.");

			NavManager.OnDashboardPage = false;
		}
	}
}

