﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Manager;
using bepoz_reports.Colours;
using bepoz_reports.Models;
using bepoz_reports.Controls;
using bepoz_reports.WebServices;
using bepoz_reports.ViewModels;
using bepoz_reports.Mvvm;

namespace bepoz_reports.Pages
{
	public partial class ReportSettingsPage : ContentPage
	{
		private bool showingComboBoxView = false, dateShowing = false, customDateShowing = false, animating = false, isStore = false, firstTap = true;

		private ReportSettingsViewModel ViewModel = new ReportSettingsViewModel ();

		public ReportSettingsPage()
		{
			InitializeComponent ();

			combo_box_mixed_view.Type = 0;

			BackgroundColor = Color.White;

			ViewModel = new ReportSettingsViewModel();
			BindingContext = ViewModel;

			NavigationPage.SetHasNavigationBar (this, true);

			// set icon for bottom tab
			//Title = "Settings";
			Title = "";

			// set icon for bottom tab based on device type
			switch (DeviceManager.DeviceType) {
				case 0:
					break;
				case 1:
					Icon = "calendar.png";
					break;
			}

			combo_box_view_date.DateSelected += (sender, e) => {
				if (ReportManager.ReportPeriod.Contains("Custom"))
					date_layout_interval_button.IsEnabled = false;
				else
					date_layout_interval_button.IsEnabled = true;

				custom_label.IsVisible = !date_layout_interval_button.IsEnabled;
			};
				
			var storeTapGestureRecognizer = new TapGestureRecognizer ();

			storeTapGestureRecognizer.Tapped += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("Store label tap gesture occured.");

				setEntriesEnabled(false);
				display_button.IsVisible = false;

				NavManager.PickerChanger.ChangePickerModels (1);
				NavManager.RootPage.IsGestureEnabled = false;

				tapGestureFunction(0);
			};

			var venueTapGestureRecognizer = new TapGestureRecognizer ();

			venueTapGestureRecognizer.Tapped += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("Venue label tap gesture occured.");

				setEntriesEnabled(false);
				display_button.IsVisible = false;

				NavManager.PickerChanger.ChangePickerModels (0);
				NavManager.RootPage.IsGestureEnabled = false;

				tapGestureFunction(1);
			};

			var dateTapGestureRecognizer = new TapGestureRecognizer ();

			dateTapGestureRecognizer.Tapped += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("Date label or frame tap gesture occured.");

				date_layout_custom_view.IsVisible = true;
				date_layout_interval_view.IsVisible = true;
				display_button.IsVisible = false;

				setEntriesEnabled(false);

				NavManager.PickerChanger.ChangePickerModels (2);
				NavManager.RootPage.IsGestureEnabled = false;

				tapGestureFunctionDate();
			};

			var typeTapGestureRecognizer = new TapGestureRecognizer ();

			typeTapGestureRecognizer.Tapped += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("Type label or frame tap gesture occured.");

				display_button.IsVisible = false;
				setEntriesEnabled(false);

				NavManager.PickerChanger.ChangePickerModels (2);
				NavManager.RootPage.IsGestureEnabled = false;

				tapGestureFunction(2);
			};

			date_layout_custom_view.SwipeRightOccured += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("date_layout_custom_view swipe occured.");
				animatePrevPage();
			};

			date_layout_interval_view.SwipeLeftOccured += (sender, e) => {
				System.Diagnostics.Debug.WriteLine("date_layout_interval_view swipe occured.");
				animateNextPage();
			};

			store_entry.GestureRecognizers.Add (storeTapGestureRecognizer);
			venue_entry.GestureRecognizers.Add (venueTapGestureRecognizer);
			date_label.GestureRecognizers.Add (dateTapGestureRecognizer);
			date_frame.GestureRecognizers.Add (dateTapGestureRecognizer);
			type_label.GestureRecognizers.Add (typeTapGestureRecognizer);
			type_frame.GestureRecognizers.Add (typeTapGestureRecognizer);
		}

		private void setEntriesEnabled(bool enabled)
		{
			store_entry.IsEnabled = enabled;
			venue_entry.IsEnabled = enabled;
			date_label.IsEnabled = enabled;
			date_frame.IsEnabled = enabled;
		}

		private async void tapGestureFunction(int type)
		{
			if (animating)
				return;
				
			switch (type) 
			{
				case 0:
					combo_label.Text = "Select Store:";
					break;
				case 1:
					combo_label.Text = "Select Venue:";
					break;
				case 2:
					combo_label.Text = "Select Type:";
					break;
			}

			combo_box_mixed_view.Type = type;

			animateComboBoxView(combo_layout.Bounds.Height, 0.3f);

			showingComboBoxView = !showingComboBoxView;
		}

		private void animateNextPage()
		{
			date_layout_interval_view.LayoutTo(new Rectangle (date_layout_interval_view.Bounds.X - date_layout_interval_view.Bounds.Width, date_layout_interval_view.Bounds.Y, date_layout_interval_view.Bounds.Width, date_layout_interval_view.Bounds.Height), 150, null);
			date_layout_custom_view.LayoutTo(new Rectangle (date_layout_custom_view.Bounds.X - date_layout_custom_view.Bounds.Width, date_layout_custom_view.Bounds.Y, date_layout_custom_view.Bounds.Width, date_layout_custom_view.Bounds.Height), 150, null);
			customDateShowing = true;
		}

		private void animatePrevPage()
		{
			date_layout_custom_view.LayoutTo(new Rectangle (date_layout_custom_view.Bounds.X + date_layout_custom_view.Bounds.Width, date_layout_custom_view.Bounds.Y, date_layout_custom_view.Bounds.Width, date_layout_custom_view.Bounds.Height), 150, null);
			date_layout_interval_view.LayoutTo(new Rectangle (date_layout_interval_view.Bounds.X + date_layout_interval_view.Bounds.Width, date_layout_interval_view.Bounds.Y, date_layout_interval_view.Bounds.Width, date_layout_interval_view.Bounds.Height), 150, null);
			customDateShowing = false;
		}

		private void tapGestureFunctionDate()
		{
			animateDateView (1, 0.3f);
			dateShowing = true;
		}

		private void animateComboBoxView(double movement, float opacity)
		{
			animating = true;
			black_overlay_view.FadeTo (opacity, 50);
			combo_layout.LayoutTo(new Rectangle (combo_layout.Bounds.X, combo_layout.Bounds.Y - movement, combo_layout.Bounds.Width, combo_layout.Bounds.Height), 150, null);
			animating = false;
		}

		private void animateDateView(float opacityDate, float opacityBlackOverlay)
		{
			animating = true;
			black_overlay_view.FadeTo (opacityBlackOverlay, 50);
			date_layout_interval_view.FadeTo (opacityDate, 100);
			date_layout_custom_view.FadeTo (opacityDate, 100);
			animating = false;
		}

		public void ComboDoneClicked(object sender, EventArgs args)
		{
			display_button.IsVisible = true;

			setEntriesEnabled(true);

			NavManager.RootPage.IsGestureEnabled = true;

			System.Diagnostics.Debug.WriteLine ("Done pressed on combo box view.");

			SetLabels ();

			animateComboBoxView(combo_layout.Bounds.Height * -1, 0);
		}

		private void SetLabels()
		{
			date_label.Text = ReportManager.CurrentDate;
			store_entry.Text = ReportManager.CurrentStore;
			venue_entry.Text = ReportManager.CurrentVenue;
			type_entry.Text = ReportManager.CurrentTypeStr;
		}

		public void DateDoneClicked(object sender, EventArgs args)
		{
			if (customDateShowing && !settings_custom_date_view.BothDatesSelected) {
				DisplayAlert ("Bepoz Reports", "Please select both dates.", "OK");
				return;
			}

			display_button.IsVisible = true;

			setEntriesEnabled(true);

			NavManager.RootPage.IsGestureEnabled = true;

			ReportManager.isCustom = customDateShowing;
			ReportManager.SetDateText ();
			date_label.Text = ReportManager.CurrentDate;

			System.Diagnostics.Debug.WriteLine ("Done pressed on date view.");

			date_layout_custom_view.IsVisible = false;
			date_layout_interval_view.IsVisible = false;

			animateDateView (0, 0);

			dateShowing = false;
			customDateShowing = false;
		}

		public async void DisplayClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Display pressed.");

			NavManager.DisplayReport = true;

			App.Navigation.PopAsync();

			//App.Navigation.PushAsync (ViewFactory.CreatePage<GraphSelectionViewModel> ());
		}
			
		protected override void OnAppearing()
		{
			base.OnAppearing();

			System.Diagnostics.Debug.WriteLine ("Report settings page appeared.");

			SetLabels ();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			System.Diagnostics.Debug.WriteLine ("Report settings page disappeared.");
		}
	}
}