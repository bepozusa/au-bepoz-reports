﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.ViewModels;
using bepoz_reports.Report;
using bepoz_reports.Models;
using bepoz_reports.Discount;
using bepoz_reports.Tasks;
using bepoz_reports.Banking;
using bepoz_reports.Controls;
using bepoz_reports.Device;
using bepoz_reports.DailyTotals;
using bepoz_reports.Nav;

namespace bepoz_reports.Pages
{	
	public partial class LineGraphPage : ContentPage
	{	
		LineGraphViewModel ViewModel = new LineGraphViewModel ();

		private bool graphLoaded = false;

		private LoadingView loadingView;
		private NoDataView noDataView;

		private View mainView;

		public LineGraphPage ()
		{
			Title = "Line Graph";

			// set icon for bottom tab based on device type
			switch (DeviceManager.DeviceType) {
				case 0:
					break;
				case 1:
					Icon = "line.png";
					break;
			}

			InitializeComponent ();

			this.BindingContext = ViewModel;

			loadingView = new LoadingView ();
			noDataView = new NoDataView();

			loadingView.Layout (Content.Bounds);
			noDataView.Layout (Content.Bounds);

			mainView = Content;
			Content = loadingView;

			this.hybridWebView.RegisterCallback("dataCallback", t =>
				System.Diagnostics.Debug.WriteLine(t)
			);

			this.hybridWebView.LoadFinished += (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Finished loading web view.");

				this.hybridWebView.CallJsFunction ("CreateChart", ViewModel);
			};
		}

		private void createChartData()
		{
			// create chart based on report type
			switch (TaskManager.ReportType)
			{
			case 0:
				createDataTill ();
				break;
			case 1:
				createDataDiscounts ();
				break;
			case 2:
				createDataDailyTotals();
				break;
			}
		}

		private void createDataTill()
		{
			foreach (TillItem tillItem in BankingManager.Items) 
			{
				if (tillItem.Theoretical < 0)
					tillItem.Theoretical *= -1;

				if (!BankingManager.UsingShifts)
					ViewModel.Labels.Add (tillItem.Till);
				else
					ViewModel.Labels.Add (tillItem.ShiftDate.ToString());

				ViewModel.Values.Add (tillItem.Theoretical);
			}
		}

		private void createDataDailyTotals()
		{
			foreach (DailyTotalsItem dailyTotalsItem in DailyTotalsManager.DailyTotals) 
			{
				if (dailyTotalsItem.NettTotal < 0)
					dailyTotalsItem.NettTotal *= -1;

				ViewModel.Labels.Add (dailyTotalsItem.ShiftID + "");
				ViewModel.Values.Add (dailyTotalsItem.NettTotal);
			}
		}

		private void createDataDiscounts()
		{
			bool isVenues = false;

			if (NavManager.CurrentTitle.Contains ("Venues"))
				isVenues = true;

			foreach (DiscountsItem discountItem in DiscountsManager.Discounts) 
			{
				//if (isVenues) 
				{
				}

				if (discountItem.Amount < 0)
					discountItem.Amount *= -1;

				ViewModel.Labels.Add (discountItem.Discount);
				ViewModel.Values.Add (discountItem.Amount);
			}
		}

		private async Task<bool> WaitToLoadAsync()
		{
			Task<bool> work = Task<bool>.Factory.StartNew (() => {
				while (!TaskManager.TaskComplete){}

				return true;
			});

			await work;

			loadPage ();

			return true;
		}

		private void loadPage()
		{
			System.Diagnostics.Debug.WriteLine("Displaying content for pie graph page.");

			createChartData ();

			if (ViewModel.Values.Count > 1) {
				Content = mainView;
				hybridWebView.LoadFromContent ("HTML/line.html");

				if (DeviceManager.DeviceType == 0 && !graphLoaded) {
					this.hybridWebView.CallJsFunction ("CreateChart", ViewModel);
					graphLoaded = true;
				}
			} else
				Content = noDataView;
		}

		protected override void OnAppearing()
		{
			System.Diagnostics.Debug.WriteLine ("Loading html for line chart from local cshtml.");

			if (!TaskManager.TaskComplete)
				WaitToLoadAsync ();
			else {
				loadPage ();
			}
		}
	}
}

