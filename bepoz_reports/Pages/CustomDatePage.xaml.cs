﻿using System;
using System.Windows.Input;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Report;
using bepoz_reports.ViewModels;
using bepoz_reports.Mvvm;
using bepoz_reports.Controls;
using bepoz_reports.Date;
using bepoz_reports.Nav;

namespace bepoz_reports.Pages
{	
	public partial class CustomDatePage : ContentPage
	{	
		private bool dateFromSelected = false, bothDatesSelected = false;

		async void SubmitClicked(object sender, EventArgs args)
		{
			if (!bothDatesSelected) {
				DisplayAlert ("Bepoz Reports", "Please select both dates.", "OK");
				return;
			}

			ReportManager.SetCustomDates (DateFromEntry.Text, DateToEntry.Text);

			if (NavManager.CurrentTitle.Equals ("Till Summary"))
				App.Navigation.PushAsync(ViewFactory.CreatePage<ReportSummaryViewModel> ());
			else
				App.Navigation.PushAsync(ViewFactory.CreatePage<GraphSelectionViewModel> ());
		}

		async void ClearClicked(object sender, EventArgs args)
		{
			resetCalendarSelection ();
		}

		async void PrevClicked(object sender, EventArgs args)
		{
			//calender_view.
		}

		async void NextClicked(object sender, EventArgs args)
		{
			//resetCalenderSelection ();
		}

		public CustomDatePage ()
		{
			//Title = "Custom Date";
			Title = "";

			InitializeComponent ();

			calendar_view.DateSelected += (object sender, DateTime e) =>
			{
				if (!dateFromSelected) {
					dateFromSelected = true;
					DateFromEntry.Text = DateConverter.FormatDateFromCalendar(e.ToString("d"));
				}
				else
				{
		 			DateToEntry.Text = DateConverter.FormatDateFromCalendar(e.ToString("d"));
					bothDatesSelected = true;
				}
			};
		}

		protected override void OnAppearing()
		{
			System.Diagnostics.Debug.WriteLine ("Custom date page appeared.");

			resetCalendarSelection ();

			NavManager.RootPage.IsGestureEnabled = false;
		}

		protected override void OnDisappearing()
		{
			System.Diagnostics.Debug.WriteLine ("Custom date page disappeared.");

			NavManager.RootPage.IsGestureEnabled = false;
		}

		private void resetCalendarSelection()
		{
			DateFromEntry.Text = "";
			DateToEntry.Text = "";
			DateFromEntry.Placeholder = "YYYY-MM-DD";
			DateToEntry.Placeholder = "YYYY-MM-DD";

			dateFromSelected = false;
			bothDatesSelected = false;
		}
	}
}