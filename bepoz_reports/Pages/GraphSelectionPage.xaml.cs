﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.ViewModels;
using bepoz_reports.Report;
using bepoz_reports.Discount;
using bepoz_reports.DailyTotals;
using bepoz_reports.Tasks;
using bepoz_reports.Mvvm;
using bepoz_reports.Banking;
using bepoz_reports.Models;
using bepoz_reports.Nav;

namespace bepoz_reports.Pages
{    
	public partial class GraphSelectionPage : TabbedPage
	{
		public GraphSelectionViewModel ViewModel = new GraphSelectionViewModel();

		public GraphSelectionPage ()
		{
			InitializeComponent ();

			BindingContext = ViewModel;

			Title = NavManager.CurrentTitle;

			initPage ();

			// add graphing pages
			Children[0] = (ViewFactory.CreatePage<BarGraphViewModel> ());
			Children[0].Title = "Bar Graph";

			Children[1] = (ViewFactory.CreatePage<PieGraphViewModel> ());
			Children[1].Title = "Pie Graph";

			Children[2] = (ViewFactory.CreatePage<LineGraphViewModel> ());
			Children[2].Title = "Line Graph";
		}

		public async void initPage()
		{
			TaskManager.TaskComplete = false;
			TaskManager.HTTPRequestTask = null;

			if (NavManager.CurrentTitle.Contains ("Banking Summary")) {
				System.Diagnostics.Debug.WriteLine ("Loading graphs for till summary.");

				//BankingManager.ShuffleListUniqueToShiftIDs ();
				TaskManager.HTTPRequestTask = BankingManager<TillItem>.CreateTillDataRequest ();

				TaskManager.ReportType = 0;
			} else if (NavManager.CurrentTitle.Contains ("Venues") || NavManager.CurrentTitle.Contains ("Stores")) {
				System.Diagnostics.Debug.WriteLine ("Loading graphs for discounts.");

				TaskManager.HTTPRequestTask = DiscountsManager.CreateDiscountDataRequest ();
				TaskManager.ReportType = 1;
			} else if (NavManager.CurrentTitle.Contains ("Daily Totals")) {
				System.Diagnostics.Debug.WriteLine ("Loading graphs for daily totals.");

				TaskManager.HTTPRequestTask = DailyTotalsManager.CreateDailyTotalsDataRequest ();
				TaskManager.ReportType = 2;
			}
				
			if (TaskManager.HTTPRequestTask != null) {
				await TaskManager.HTTPRequestTask;
			}

			TaskManager.TaskComplete = true;

			System.Diagnostics.Debug.WriteLine ("Task manager completed task.");
		}
	}
}

