﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using bepoz_reports.Device;

namespace bepoz_reports.Pages
{	
	public partial class GridGraphPage : ContentPage
	{	
		public GridGraphPage ()
		{
			// set icon for bottom tab based on device type
			switch (DeviceManager.DeviceType) {
				case 0:
					break;
				case 1:
					Icon = "till.png";
					break;
			}

			InitializeComponent ();
		}
	}
}

