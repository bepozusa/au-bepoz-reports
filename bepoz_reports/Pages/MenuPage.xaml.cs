﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Colours;
using bepoz_reports;
using bepoz_reports.ViewModels;
using bepoz_reports.Manager;
using bepoz_reports.Mvvm;
using bepoz_reports.Controls;

namespace bepoz_reports.Pages
{	
	public partial class MenuPage : ContentPage
	{	
		public MenuViewModel ViewModel = new MenuViewModel ();

		public ListView Menu { get; set; }

		public MenuPage ()
		{
			InitializeComponent ();

			BindingContext = ViewModel;

			NavManager.MenuPage = this;

			NavigationPage.SetHasNavigationBar (this, false);

			Title = " "; 

			// set icon for bottom tab based on device type
			switch (DeviceManager.DeviceType) {
				case 0:
					break;
				case 1:
					Icon = "menu.png";
					break;
			}

			//Menu.ItemTemplate = new DataTemplate(typeof(MenuCellContentView));
				
			header_image.Source = ImageSource.FromFile("account.png");
			logout_image.Source = ImageSource.FromFile("logout.png");

			Menu = menu_list;
		}

		// called from event handler in nav manager 
		public void SetLoginStatusOnMenu(bool visible)
		{
			if (visible) {
				header_image.Source = AccountManager.CurrentUserInfo.UserImageSource;
				user_label.Text = String.Format ("Welcome {0}", AccountManager.CurrentUserInfo.FirstName);
				org_label.Text = String.Format ("{0}", AccountManager.CurrentUserInfo.OrganisationName);
				org_label.IsVisible = true;
			} else {
				header_image.Source = ImageSource.FromFile("account.png");
				user_label.Text = String.Format ("Please login.");
				org_label.IsVisible = false;
			}

			menu_list.IsVisible = visible;
			footer_stack.IsVisible = visible;
		}

		async void LogoutClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Logout Pressed.");
				
			NavManager.LoggedIn = false;
			NavManager.Visible = false;
		}
	}
}

