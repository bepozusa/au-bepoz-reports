﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.ViewModels;
using bepoz_reports.Controls;
using bepoz_reports.Mvvm;
using bepoz_reports.Models;
using bepoz_reports.Manager;

namespace bepoz_reports.Pages
{
	public partial class TypeListPage : ContentView
	{
		public TypeListViewModel ViewModel;

		public TypeListPage (TypeListViewModel Model)
		{
			InitializeComponent ();

			BindingContext = ViewModel = Model;
					
			item_list.GroupHeaderTemplate = new DataTemplate(typeof(HeaderViewCell));
			item_list.ItemTemplate = new DataTemplate(typeof(TillViewCell));

			// lesson item when selected navigates to lesson page
			item_list.ItemTapped += (sender, e) => 
			{
				switch (NavManager.Type)
				{
					case 1:
						App.BankingManager.InitGridModel ();
						App.BankingManager.CurrentItem = (e.Item as BankingItem);
						break;
					case 2:
						App.DailyTotalsManager.InitGridModel ();
						App.DailyTotalsManager.CurrentItem = (e.Item as DailyTotalsItem);
						break;
					case 3:
						App.DiscountsManager.InitGridModel ();
						App.DiscountsManager.CurrentItem = (e.Item as DiscountsItem);
						break;
					case 4:
						App.NettSalesComparisonManager.InitGridModel ();
						App.NettSalesComparisonManager.CurrentItem = (e.Item as NettSalesComparisonItem);
						break;
				}

				ViewModel.ShowReport.Execute(null);
			};
		}
	}
}

