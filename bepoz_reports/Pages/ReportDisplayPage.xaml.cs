﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Mvvm;
using bepoz_reports.ViewModels;
using bepoz_reports.Controls;
using bepoz_reports.Manager;
using System.Threading.Tasks;
using bepoz_reports.Models;

namespace bepoz_reports.Pages
{	
	public partial class ReportDisplayPage : ContentPage
	{	
		private int ScreenIndex = 0;

		private bool animating = false;

		private List<SwipeView> Views = new List<SwipeView>();
		private SwipeView Current;

		private LoadingView loadingView;
		private View mainView;

		public ReportDisplayPage ()
		{
			InitializeComponent ();

			//Title = NavManager.CurrentTitle;
			Title = "";

			BackgroundColor = Color.White;

			foreach (SwipeView view in Views) {
				view.SwipeRightOccured += (sender, e) => {
					moveRight();
				};

				view.SwipeLeftOccured += (sender, e) => {
					moveLeft();
				};
			}

			if (NavManager.DisplayReport) {
				System.Diagnostics.Debug.WriteLine ("Displaying reports.");

				initPage ();
			} else {
				System.Diagnostics.Debug.WriteLine ("Showing report instructions.");

				Content = new ReportDisplayIntro ();
			}

			createToolBarItems ();
			CreateLayout ();
		}

		private void SetLoading()
		{
			System.Diagnostics.Debug.WriteLine ("Views are loading.");

			Content = new LoadingView ();
		}

		private void SetNoData()
		{
			System.Diagnostics.Debug.WriteLine ("Displaying no data view.");

			Content = new NoDataView ();
		}

		private void CreateLayout()
		{

		}

		private void moveRight()
		{
			if (ScreenIndex == 0)
				return;

			animateRight (Current, Views[ScreenIndex - 1]);
			Current = Views [ScreenIndex - 1];

			ScreenIndex--;
		}

		private void moveLeft()
		{
			if (ScreenIndex == (Views.Count - 1))
				return;

			animateLeft (Current, Views[ScreenIndex + 1]);
			Current = Views [ScreenIndex + 1];

			ScreenIndex++;
		}

		private void animateLeft(SwipeView toPop, SwipeView toPush)
		{
			animating = true;
			toPop.LayoutTo(new Rectangle (toPop.Bounds.X - toPop.Bounds.Width, toPop.Bounds.Y, toPop.Bounds.Width, toPop.Bounds.Height), 150, null);
			toPush.LayoutTo(new Rectangle (toPush.Bounds.X - toPush.Bounds.Width, toPush.Bounds.Y, toPush.Bounds.Width, toPush.Bounds.Height), 150, null);
			animating = false;
		}

		private void animateRight(SwipeView toPop, SwipeView toPush)
		{
			animating = true;
			toPop.LayoutTo(new Rectangle (toPop.Bounds.X + toPop.Bounds.Width, toPop.Bounds.Y, toPop.Bounds.Width, toPop.Bounds.Height), 150, null);
			toPush.LayoutTo(new Rectangle (toPush.Bounds.X + toPush.Bounds.Width, toPush.Bounds.Y, toPush.Bounds.Width, toPush.Bounds.Height), 150, null);
			animating = false;
		}

		private void createToolBarItems()
		{
			ToolbarItem settingsButton = new ToolbarItem ("settings", "settings.png", async () => {
				System.Diagnostics.Debug.WriteLine ("Report settings pressed.");

				App.Navigation.PushAsync(ViewFactory.CreatePage<ReportSettingsViewModel> ());
			});

			ToolbarItems.Add(settingsButton);
		}

		public async void initPage()
		{
			TaskManager.TaskComplete = false;
			TaskManager.HTTPRequestTask = null;

			if (NavManager.CurrentTitle.Contains ("Banking")) {
				System.Diagnostics.Debug.WriteLine ("Retriving data for banking summary.");

				TaskManager.HTTPRequestTask = App.BankingManager.CreateDataRequest ("Banking");
			} else if (NavManager.CurrentTitle.Contains ("Discounts")) {
				System.Diagnostics.Debug.WriteLine ("Retriving data for discounts.");

				TaskManager.HTTPRequestTask = App.DiscountsManager.CreateDataRequest ("Discounts");
			} else if (NavManager.CurrentTitle.Contains ("Daily Totals")) {
				System.Diagnostics.Debug.WriteLine ("Retriving data for daily totals.");

				TaskManager.HTTPRequestTask = App.DailyTotalsManager.CreateDataRequest ("DailyTotals");
			}

			if (TaskManager.HTTPRequestTask != null) {
				await TaskManager.HTTPRequestTask;
			}

			TaskManager.TaskComplete = true;

			loadPage ();

			System.Diagnostics.Debug.WriteLine ("Task manager completed task.");
		}

		private void loadPage()
		{
			if (NavManager.DisplayReport) 
			{
				if (NavManager.CurrentTitle.Contains ("Banking")) {
					NavManager.Type = 0;
					initListViewPage (App.BankingManager.Items.Count, new TypeListViewModel (0));
				} 
				else if (NavManager.CurrentTitle.Contains ("Discounts")) {
					NavManager.Type = 1;
					initListViewPage (App.DiscountsManager.Items.Count, new TypeListViewModel (1));
				}
				else if (NavManager.CurrentTitle.Contains ("Daily Totals")) {
					NavManager.Type = 2;
					initListViewPage (App.DailyTotalsManager.Items.Count, new TypeListViewModel (2));
				}
			}
		}

		private void initListViewPage(int numberOfItems, ViewModel Model)
		{
			if (numberOfItems > 0) 
			{
				System.Diagnostics.Debug.WriteLine ("Displaying content for till list page.");
				NavManager.RootPage.IsGestureEnabled = false;
				Content = new TypeListView () { BindingContext = Model};
			}
			else {
				NavManager.RootPage.IsGestureEnabled = true;
				SetNoData ();
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			System.Diagnostics.Debug.WriteLine ("Report display page appeared.");

			if (!TaskManager.TaskComplete) {
				SetLoading ();
			}
			else {
				loadPage ();
			}
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing ();

			System.Diagnostics.Debug.WriteLine ("Time selection page disappeared.");
		}
	}
}

