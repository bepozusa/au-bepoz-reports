﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

using bepoz_reports.ViewModels;
using bepoz_reports.Models;
using bepoz_reports.Controls;
using bepoz_reports.Manager;

namespace bepoz_reports.Pages
{	
	public partial class ReportSummaryPage : ContentPage
	{	
		BarGraphViewModel ViewModel = new BarGraphViewModel ();

		private bool animating = false;

		private LoadingView loadingView;
		private NoDataView noDataView;

		private View mainView;

		private List<SwipeView> SwipeViews = new List<SwipeView> ();
		private List<HybridWebView> WebViews = new List<HybridWebView> ();
		private List<Image> NavCircles = new List<Image> ();
		private List<string> Graphs = new List<string> ();
		private List<string> PageTitles = new List<string> ();
		private List<int> GraphDataSetFlags = new List<int>();

		private int pageIndex = 0;
		private int numberOfSwipeViews = 1;

		public ReportSummaryPage ()
		{
			InitializeComponent ();

			loadingView = new LoadingView ();
			noDataView = new NoDataView();

			mainView = Content;
			Content = loadingView;

			switch (NavManager.Type)
			{
				case 0:
					GraphDataSetFlags = App.BankingManager.GridModel.GraphDataSetFlags;
					numberOfSwipeViews = 2;
					heading_label.Text = "Theoretical vs Actual";
					createTillData ();
					break;
				case 1:
					GraphDataSetFlags = App.DiscountsManager.GridModel.GraphDataSetFlags;
					numberOfSwipeViews = 3;
					heading_label.Text = "Amount vs Qty";
					createDiscountData ();
					break;
				case 2:
					GraphDataSetFlags = App.DailyTotalsManager.GridModel.GraphDataSetFlags;
					numberOfSwipeViews = 3;
					heading_label.Text = "Cost of Sales vs Net Total";
					createDailyTotalsData ();
					break;
			}

			initViewLists (numberOfSwipeViews);
				
			seperator_swipe.SwipeLeftOccured += (s, e) =>
			{
				animateLeft();
			};

			seperator_swipe.SwipeRightOccured += (s, e) =>
			{
				animateRight();
			};
		}

		private SwipeView createSwipeViewTemplate(int currentIndex)
		{
			SwipeView swipe_template = new SwipeView ();
			swipe_template.BackgroundColor = Color.Transparent;

			swipe_relative_layout.Children.Add (swipe_template,
				Constraint.RelativeToParent((parent) =>
					{
						return parent.Width;
					}),
				Constraint.RelativeToParent((parent) =>
					{
						return parent.Y - 80;
					}),
				Constraint.RelativeToParent((parent) =>
					{
						return parent.Width;
					}),
				Constraint.RelativeToParent((parent) =>
					{
						return parent.Height * 0.95;
					}));
						
			swipe_template.Content = createWebViewTemplate(currentIndex);

			return swipe_template;
		}

		private Image createNavCircleTemplate()
		{
			Image navCircleImage = new Image () { Source = "circle.png", Opacity = 0.3f, WidthRequest = 10, HeightRequest = 10};

			// add nav circle image to stack layout
			circles_layout.Children.Add (navCircleImage);

			return navCircleImage;
		}

		private HybridWebView createWebViewTemplate(int currentWebIndex)
		{
			HybridWebView web_template = new HybridWebView ();
			web_template.BackgroundColor = Color.Transparent;

			web_template.LoadFinished += (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Finished loading web view.");

				web_template.CallJsFunction ("CreateChart", ViewModel, GraphDataSetFlags[currentWebIndex]);
			};

			WebViews.Add (web_template);

			return web_template;
		}

		private void initViewLists(int numberOfSwipeViews)
		{
			SwipeViews.Add (first_panel);
			NavCircles.Add (circle_1);

			for (int i = 0; i < numberOfSwipeViews; i++) {
				SwipeViews.Add (createSwipeViewTemplate (i));
				NavCircles.Add (createNavCircleTemplate());
			}

			circles_layout.ForceLayout ();
		}

		private async void animateLeft()
		{
			if (animating)
				return;

			if (pageIndex < numberOfSwipeViews) {
				animating = true;
				// animate navigation circle images
				animateNavCircles (NavCircles[pageIndex], NavCircles[pageIndex + 1]);

				// move views left
				SwipeViews[pageIndex].LayoutTo (new Rectangle (SwipeViews[pageIndex].Bounds.X - SwipeViews[pageIndex].Bounds.Width, SwipeViews[pageIndex].Bounds.Y, SwipeViews[pageIndex].Bounds.Width, SwipeViews[pageIndex].Bounds.Height), 150, null);
				await SwipeViews[pageIndex + 1].LayoutTo (new Rectangle (SwipeViews[pageIndex + 1].Bounds.X - SwipeViews[pageIndex + 1].Bounds.Width, SwipeViews[pageIndex + 1].Bounds.Y, SwipeViews[pageIndex + 1].Bounds.Width, SwipeViews[pageIndex + 1].Bounds.Height), 150, null);
				pageIndex++;
				heading_label.Text = PageTitles[pageIndex];

				loadWebView (pageIndex - 1);
				animating = false;
			}
		}

		private async void animateRight()
		{
			if (animating)
				return;

			if (pageIndex > 0) {
				animating = true;
				// animate navigation circle images
				animateNavCircles (NavCircles[pageIndex], NavCircles[pageIndex - 1]);

				// move views right
				SwipeViews[pageIndex].LayoutTo (new Rectangle (SwipeViews[pageIndex].Bounds.X + SwipeViews[pageIndex].Bounds.Width, SwipeViews[pageIndex].Bounds.Y, SwipeViews[pageIndex].Bounds.Width, SwipeViews[pageIndex].Bounds.Height), 150, null);
				await SwipeViews[pageIndex - 1].LayoutTo (new Rectangle (SwipeViews[pageIndex - 1].Bounds.X + SwipeViews[pageIndex - 1].Bounds.Width, SwipeViews[pageIndex - 1].Bounds.Y, SwipeViews[pageIndex - 1].Bounds.Width, SwipeViews[pageIndex - 1].Bounds.Height), 150, null);
				pageIndex--;
				heading_label.Text = PageTitles[pageIndex];

				if (pageIndex > 0)
					loadWebView (pageIndex - 1);

				animating = false;
			}
		}

		private async void animateNavCircles(Image circleFrom, Image circleTo)
		{
			circleFrom.FadeTo (0.3f);
			circleTo.FadeTo (1.0f);
		}

		private void createTillData()
		{
			Graphs = App.BankingManager.GridModel.Graphs;
			// add labels
			PageTitles = App.BankingManager.GridModel.PageTitles;
			ViewModel.Labels = App.BankingManager.GridModel.Titles;

			// add hex colours
			ViewModel.HexColours.Add ("#0c2d62");
			ViewModel.HexColours.Add ("#c3bbb2");

			// add values for different data sets
			ViewModel.Values.Add (new List<double> () {
				App.BankingManager.CurrentItem.Summary.TotalCashTheoretical,
				App.BankingManager.CurrentItem.Summary.TotalChequeTheoretical,
				App.BankingManager.CurrentItem.Summary.TotalEFTPOSTheoretical
			});

			ViewModel.Values.Add (new List<double> () {
				App.BankingManager.CurrentItem.Summary.TotalCashActual,
				App.BankingManager.CurrentItem.Summary.TotalChequeActual,
				App.BankingManager.CurrentItem.Summary.TotalEFTPOSActual
			});
				
			App.BankingManager.GridModel.LoadValues (ViewModel.Values[0], ViewModel.Values[1]);
			App.BankingManager.GridModel.CreateListData ();

			System.Diagnostics.Debug.WriteLine ("Creating banking summary data.");
		}

		private void createDailyTotalsData()
		{
			Graphs = App.DailyTotalsManager.GridModel.Graphs;
			PageTitles = App.DailyTotalsManager.GridModel.PageTitles;

			// add labels
			ViewModel.HexColours.Add ("#0c2d62");
			ViewModel.HexColours.Add ("#c3bbb2");

			List<double> CostOfSalesValues = new List<double> ();
			List<double> NetTotalValues = new List<double> ();

			// add labels
			foreach (DayTotalItem item in App.DailyTotalsManager.CurrentItem.Summary.DayTotalItems) {
				ViewModel.Labels.Add (item.Day);

				CostOfSalesValues.Add (item.CostOfSales);
				NetTotalValues.Add (item.NetTotal);
			}

			ViewModel.Values.Add (CostOfSalesValues);
			ViewModel.Values.Add (NetTotalValues);

			App.DailyTotalsManager.GridModel.Titles = ViewModel.Labels;
			App.DailyTotalsManager.GridModel.CreateItems ();
			App.DailyTotalsManager.GridModel.LoadValues (CostOfSalesValues, NetTotalValues);
			App.DailyTotalsManager.GridModel.CreateListData ();

			System.Diagnostics.Debug.WriteLine ("Creating daily totals data.");
		}

		private void createDiscountData()
		{
			Graphs = App.DiscountsManager.GridModel.Graphs;
			PageTitles = App.DiscountsManager.GridModel.PageTitles;

			ViewModel.HexColours.Add ("#0c2d62");
			ViewModel.HexColours.Add ("#c3bbb2");

			List<double> AmountValues = new List<double> ();
			List<double> QtyValues = new List<double> ();

			foreach (DiscountsItem discountsItem in App.DiscountsManager.CurrentItem.Summary.DiscountItems) {
				if (discountsItem.Amount < 0)
					discountsItem.Amount *= -1;

				ViewModel.Labels.Add (discountsItem.Discount);
				AmountValues.Add (discountsItem.Amount);
				QtyValues.Add (discountsItem.Qty);
			}

			ViewModel.Values.Add (AmountValues);
			ViewModel.Values.Add (QtyValues);

			App.DiscountsManager.GridModel.Titles = ViewModel.Labels;
			App.DiscountsManager.GridModel.CreateItems ();
			App.DiscountsManager.GridModel.LoadValues (AmountValues, QtyValues);
			App.DiscountsManager.GridModel.CreateListData ();

			System.Diagnostics.Debug.WriteLine ("Creating daily totals data.");
		}

		private void loadMainView()
		{		
			if (ViewModel.Values.Count > 0)
				Content = mainView;
			else
				Content = noDataView;

			System.Diagnostics.Debug.WriteLine ("Loaded main view." + ViewModel.Values);
		}

		private void loadWebView(int id)
		{
			ViewModel.Height = WebViews [id].Bounds.Height;
			ViewModel.Width = WebViews [id].Bounds.Width;

			WebViews[id].LoadFromContent ("HTML/" + Graphs [id] + ".html");
		}

		protected override void OnAppearing()
		{
			System.Diagnostics.Debug.WriteLine ("Summary display page appeared.");

			loadMainView ();
		}

		protected override void OnDisappearing()
		{
			System.Diagnostics.Debug.WriteLine ("Summary display page disappeared.");

			NavManager.DisplayReport = false;
		}
	}
}