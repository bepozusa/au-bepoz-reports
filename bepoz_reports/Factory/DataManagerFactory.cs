﻿using System;
using System.Collections;
using System.Collections.Generic;

using bepoz_reports.DeviceProperties;
using bepoz_reports.Manager;
using bepoz_reports.Models;

namespace bepoz_reports.Factory
{
	public class DataManagerFactory<T>
	{
		public static IList<GroupItem> GetListData(int rType)
		{
			switch (rType)
			{
				case 1:
					return App.BankingManager.ListData;
				case 2:
					return App.DailyTotalsManager.ListData;
				case 3:
					return App.DiscountsManager.ListData;
				case 4:
					return App.NettSalesComparisonManager.ListData;
				//case 5:
				//	return App.DailyTotalsManager;
			}

			return null;
		}

	}
}

