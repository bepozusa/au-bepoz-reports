﻿using System;
using System.Collections;
using System.Collections.Generic;

using bepoz_reports.DeviceProperties;
using bepoz_reports.Manager;
using bepoz_reports.Models;

namespace bepoz_reports.Factory
{
	public class DataManagerFactory
	{
		public static IList<GroupItem> GetListData(int rType)
		{
			switch (rType)
			{
				case 1:
					return App.BankingManager.ListData;
				case 2:
					return App.DailyTotalsManager.ListData;
				case 3:
					return App.DiscountsManager.ListData;
				case 4:
					return App.NettSalesComparisonManager.ListData;
				//case 5:
				//	return App.DailyTotalsManager;
			}

			return null;
		}

		public static SummaryChartModel RetrieveChartModel(int rType)
		{
			switch (rType)
			{
				case 1:
					return App.BankingManager.ChartModel;
				case 2:
					return App.DailyTotalsManager.ChartModel;
				case 3:
					return App.DiscountsManager.ChartModel;
				case 4:
					return App.NettSalesComparisonManager.ChartModel;
					//case 5:
					//	return App.DailyTotalsManager;
			}

			return new SummaryChartModel();
		}

		public static void CreateGridModel (int rType, ListItem cItem)
		{
			switch (rType)
			{
				case 1:
					App.BankingManager.InitSummaryModels ();
					App.BankingManager.CurrentItem = cItem as BankingItem;
					break;
				case 2:
					App.DailyTotalsManager.InitSummaryModels();
					App.DailyTotalsManager.CurrentItem = cItem as DailyTotalsItem;
					break;
				case 3:
					App.DiscountsManager.InitSummaryModels();
					App.DiscountsManager.CurrentItem = cItem as DiscountsItem;
					break;
				case 4:
					App.NettSalesComparisonManager.InitSummaryModels();
					App.NettSalesComparisonManager.CurrentItem = cItem as NettSalesComparisonItem;
					break;
			}
		}

		public static List<SummaryGridModel> RetrieveGridModels (int rType)
		{
			switch (rType)
			{
				case 1:
					return App.BankingManager.GridModels;
				case 2:
					return App.DailyTotalsManager.GridModels;
				case 3:
					return App.DiscountsManager.GridModels;
				case 4:
					return App.NettSalesComparisonManager.GridModels;
			}

			return new List<SummaryGridModel>();
		}
	}
}

