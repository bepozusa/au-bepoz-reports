﻿using System;
using System.Collections.Generic;
using System.IO;

using bepoz_reports.Models;

namespace bepoz_reports
{
	public class LessonLibrary
	{
		private List<PeriodItem> gettingStartedLibrary = new List<PeriodItem> ();
		private List<PeriodItem> utilisingBEPozLibrary = new List<PeriodItem> ();
		private List<PeriodItem> pointOfSaleLibrary = new List<PeriodItem> ();
		private List<PeriodItem> stockControlTrainingLibrary = new List<PeriodItem> ();
		private List<PeriodItem> upgradedUsersLibrary = new List<PeriodItem> ();
		private List<PeriodItem> whatsNewLibrary = new List<PeriodItem> ();
		private List<PeriodItem> allVideosLibrary = new List<PeriodItem> ();

		private List<TitleItem> gettingStartedLibraryTitles = new List<TitleItem> ();
		private List<TitleItem> utilisingBEPozLibraryTitles = new List<TitleItem> ();
		private List<TitleItem> pointOfSaleLibraryTitles = new List<TitleItem> ();
		private List<TitleItem> stockControlTrainingLibraryTitles = new List<TitleItem> ();
		private List<TitleItem> upgradedUsersLibraryTitles = new List<TitleItem> ();
		private List<TitleItem> whatsNewLibraryTitles = new List<TitleItem> ();
		private List<TitleItem> allVideosLibraryTitles = new List<TitleItem> ();

		private List<String> libraryTitles = new List<String>(){"Getting Started", "Utilising Bepoz", "Point of Sale", "Stockcontrol Training", "Upgraded Users", "Whats New?", "All Videos"};

		public string mp4Path;

		public int totalLibraries = 7;

		public LessonLibrary ()
		{
		}

		public void createAllVideosLibrary()
		{
			// create and append new list for all videos
			allVideosLibrary = new List<PeriodItem> ();

			// create list for all other lists
			allVideosLibrary.AddRange(gettingStartedLibrary);
			allVideosLibrary.AddRange(utilisingBEPozLibrary);
			allVideosLibrary.AddRange(pointOfSaleLibrary);
			allVideosLibrary.AddRange(stockControlTrainingLibrary);
			allVideosLibrary.AddRange(upgradedUsersLibrary);
			// create and append new list for all titles
			allVideosLibraryTitles = new List<TitleItem> ();

			// create list for all other lists
			allVideosLibraryTitles.AddRange(gettingStartedLibraryTitles);
			allVideosLibraryTitles.AddRange(utilisingBEPozLibraryTitles);
			allVideosLibraryTitles.AddRange(pointOfSaleLibraryTitles);
			allVideosLibraryTitles.AddRange(stockControlTrainingLibraryTitles);
			allVideosLibraryTitles.AddRange(upgradedUsersLibraryTitles);

			System.Diagnostics.Debug.WriteLine("Loading all videos Library via __ANDROID__");
		}

		public String getLibraryTitle(int index) {
			return libraryTitles[index];
		}

		public void loadLibrary(int libraryNum, String[] records)
		{
			// create new list
			List<PeriodItem> lessons = new List<PeriodItem> ();
			List<TitleItem> libraryTitles = new List<TitleItem> ();

			int indexTitleString = 0;

			// check we have data from file
			if (records.Length > 1) {
				// create menu items
				for (int index = 0; index < records.Length; index++) {
					if (records [index].Contains ("titles")) {
						indexTitleString = index;
						index++;
					}

					if (indexTitleString == 0) {
						// break up string by character into a seperate array
						string[] recordDetails = records [index].Split (',');

						// add new record and insert details from array above
						lessons.Add (new PeriodItem () {
							Title = recordDetails [0],
							LessonName = recordDetails [0],
							LibraryNumber = libraryNum,
							LessonNumber = Convert.ToInt32 (recordDetails [1]),
							Url = recordDetails [2],
							DownloadUrl = recordDetails [3],
							Downloaded = false,
							Downloading = false,
							DownloadProgress = 0,
						});
					} else {
						string[] recordDetails = records [index].Split (',');

						libraryTitles.Add (new TitleItem () {
							Title = recordDetails [0],
							LibraryNumber = libraryNum,
							LessonNumber = Convert.ToInt32 (recordDetails [1]),
						});
					}
				}
			}

			// set list according to library number
			switch(libraryNum)
			{
			case 1:
				gettingStartedLibrary = lessons;
				gettingStartedLibraryTitles = libraryTitles;

				break;
			case 2:
				utilisingBEPozLibrary = lessons;
				utilisingBEPozLibraryTitles = libraryTitles;

				break;
			case 3:
				pointOfSaleLibrary = lessons;
				pointOfSaleLibraryTitles = libraryTitles;

				break;
			case 4:
				stockControlTrainingLibrary = lessons;
				stockControlTrainingLibraryTitles = libraryTitles;

				break;
			case 5:
				upgradedUsersLibrary = lessons;
				upgradedUsersLibraryTitles = libraryTitles;

				break;
			case 6:
				whatsNewLibrary = lessons;
				whatsNewLibraryTitles = libraryTitles;

				break;
			case 7:
				allVideosLibrary = lessons;
				allVideosLibraryTitles = libraryTitles;

				break;
			}
		}

		public PeriodItem getLibraryElement(int libraryNum, int index)
		{
			// get library according to library number
			switch(libraryNum)
			{
			case 1:
				return gettingStartedLibrary[index];
			case 2:
				return utilisingBEPozLibrary[index];
			case 3:
				return pointOfSaleLibrary[index];
			case 4:
				return stockControlTrainingLibrary[index];
			case 5:
				return upgradedUsersLibrary[index];
			case 6:
				return whatsNewLibrary[index];
			case 7:
				return allVideosLibrary[index];
			}

			return null;
		}

		public List<PeriodItem> getLibrary(int libraryNum)
		{
			// get library according to library number
			switch(libraryNum)
			{
			case 1:
				return gettingStartedLibrary;
			case 2:
				return utilisingBEPozLibrary;
			case 3:
				return pointOfSaleLibrary;
			case 4:
				return stockControlTrainingLibrary;
			case 5:
				return upgradedUsersLibrary;
			case 6:
				return whatsNewLibrary;
			case 7:
				return allVideosLibrary;
			}

			return null;
		}

		public List<TitleItem> getLibraryTitles(int libraryNum)
		{
			// get library according to library number
			switch(libraryNum)
			{
			case 1:
				return gettingStartedLibraryTitles;
			case 2:
				return utilisingBEPozLibraryTitles;
			case 3:
				return pointOfSaleLibraryTitles;
			case 4:
				return stockControlTrainingLibraryTitles;
			case 5:
				return upgradedUsersLibraryTitles;
			case 6:
				return whatsNewLibraryTitles;
			case 7:
				return allVideosLibraryTitles;
			}

			return null;
		}

		public TitleItem getLibraryTitleElement(int libraryNum, int index)
		{
			// get library according to library number
			switch(libraryNum)
			{
			case 1:
				return gettingStartedLibraryTitles[index];
			case 2:
				return utilisingBEPozLibraryTitles[index];
			case 3:
				return pointOfSaleLibraryTitles[index];
			case 4:
				return stockControlTrainingLibraryTitles[index];
			case 5:
				return upgradedUsersLibraryTitles[index];
			case 6:
				return whatsNewLibraryTitles[index];
			case 7:
				return allVideosLibraryTitles[index];
			}

			return null;
		}
	}
}