﻿using System;

namespace bepoz_reports.Date
{
	public static class DateConverter
	{
		static DateConverter ()
		{
		}

		public static string FormatDateFromCalendar(string dateString)
		{
			string[] parts = dateString.Split (new char[]{'/'}, 3);

			Array.Reverse(parts);

			string reversedDateString = "";

			int count = 0;

			foreach (string part in parts) 
			{
				if (part.Length < 2)
					parts[count] = "0" + part;

				count++;
			}

			if (parts.Length == 3)
				reversedDateString = parts [0] + "-" + parts [2] + "-" + parts [1];

			return reversedDateString;
		}

		// formats date string according to odata url
		public static string CreateDateString(string dateString)
		{
			// format string according to odata url
			dateString = dateString.Replace (" AM", ".000Z");
			dateString = dateString.Replace (" PM", ".000Z");
			dateString = dateString.Replace (" ", "-");
			dateString = dateString.Replace ("/", "-");
			dateString = dateString.Replace (":", "-");

			string[] parts = dateString.Split (new char[]{'-'}, 6);

			System.Diagnostics.Debug.WriteLine ("Date string " + dateString + " has been split into " + parts.Length + " parts.");

			// if we have six string parts from split
			if (parts.Length == 6) 
			{
				int count = 0;

				// if we have a single digit to represent a month, day, minute or hour, add '0' on the front to make it 2 digits
				foreach (string part in parts) 
				{
					if (part.Length < 2)
						parts[count] = "0" + part;

					count++;
				}

				// piece date string together according to formatting of odata url
				dateString = parts [2] + "-" + parts [0] + "-" + parts [1] + "T" + parts [3] + ":" + parts [4] + ":" + parts [5];
			}

			return dateString;
		}
	}
}

