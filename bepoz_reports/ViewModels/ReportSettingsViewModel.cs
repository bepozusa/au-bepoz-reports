﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.Manager;
using bepoz_reports.Mvvm;
using bepoz_reports.Pages;

namespace bepoz_reports.ViewModels
{
	[ViewType(typeof(ReportSettingsPage))]
	public class ReportSettingsViewModel : SettingsViewModel
    {			
		public ReportSettingsViewModel ()
        {
        }
    }
}
