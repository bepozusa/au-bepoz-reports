﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.Mvvm;
using bepoz_reports.Pages;
using bepoz_reports.Manager;

namespace bepoz_reports.ViewModels
{
	public class TypeListModel : BaseViewModel
    {
		public int Type = 0;

		private ObservableCollection<GroupItem> items = new ObservableCollection<GroupItem>();

		public ObservableCollection<GroupItem> Items
		{
			get {
				return items;
			}

			set {
				if (value != items)
				{
					items = value;
					RaisePropertyChanged("Items");
				}
			}
		}

		public Command<object> ShowReport
		{
			get {
				return new Command<object>((data) => {
					PushViewModel<ReportSummaryViewModel>(data);
				});
			}
		}
			
		public TypeListModel (int type)
		{
			Type = type;

			switch (type) 
			{
				case 1:
					Items = App.BankingManager.ListData;

					break;
				case 2:
					Items = App.DiscountsManager.ListData;

					break;
				case 3:
					Items = App.DailyTotalsManager.ListData;

					break;
			}
		}

		public void Init(object data)
		{
			if (data != null) {
				Data = data as Dictionary<string, object>;
			} else {
			}
		}

		public TypeListModel ()
        {
        }
    }
}
