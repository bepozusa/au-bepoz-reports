﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Input;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.Mvvm;
using bepoz_reports.Pages;
using bepoz_reports.Manager;

namespace bepoz_reports.ViewModels
{
	[ViewType(typeof(MenuPage))]
	public class MenuViewModel : ViewModel
	{
		public ObservableCollection<MenuItem> MenuItems { get; set; }

		public static EventHandler VisibleChanged;

		public List<string> Titles = new List<string> (){"Dashboard", "Banking Summary", "Daily Totals", "Discounts", "Nett Sales Comparison", "Sales Type Breakdown"};
		public List<string> IconFiles = new List<string> (){"dashboard.png", "discount.png", "banking_summary.png", "daily_total.png", "net_sales.png", "sales_type.png"};

		private bool visible;

		public bool Visible
		{
			get {
				visible = NavManager.Visible;

				return visible;
			}

			set
			{
				visible = value;

				SetProperty (ref visible, value);
			}
		}

		public MenuViewModel ()
		{
			MenuItems = new ObservableCollection<MenuItem> ();

			int index = 0;

			foreach (var title in Titles) {
				MenuItem newMenuItem = new MenuItem (title, IconFiles[index]);
				MenuItems.Add (newMenuItem);

				index++;
			}

			Visible = false;
				
			System.Diagnostics.Debug.WriteLine ("Loading menu with " + MenuItems.Count + " items");
		}

		private void OnVisibleChanged()
		{
			System.Diagnostics.Debug.WriteLine ("Updating Menu Items");

			Visible = true;

			if (VisibleChanged != null)
				VisibleChanged(null, EventArgs.Empty);
		}
	}
}