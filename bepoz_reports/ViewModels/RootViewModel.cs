﻿using System;

using Xamarin.Forms;

using bepoz_reports.Pages;
using bepoz_reports.Mvvm;
using bepoz_reports.Manager;

namespace bepoz_reports.ViewModels
{
	[ViewType(typeof(RootPage))]
	public class RootViewModel : ViewModel
	{
		public RootViewModel()
		{
		}
	}
}

