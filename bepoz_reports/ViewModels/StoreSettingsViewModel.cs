﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.Mvvm;
using bepoz_reports.Pages;
using bepoz_reports.Manager;

namespace bepoz_reports.ViewModels
{
	[ViewType(typeof(StoreSettingsPage))]
	public class StoreSettingsViewModel : SettingsViewModel
    {			
		public StoreSettingsViewModel ()
        {
        }
    }
}
