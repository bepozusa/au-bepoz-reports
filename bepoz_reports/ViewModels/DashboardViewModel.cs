﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Mvvm;
using bepoz_reports.Pages;
using bepoz_reports.Models;

namespace bepoz_reports.ViewModels
{
	[ViewType(typeof(DashboardPage))]
	public class DashboardViewModel : ViewModel
	{
		public ObservableCollection<DashboardMenuItem> MenuItems { get; set; }

		private string payments = "...";
		public string Payments
		{
			get { 
				return payments; 
			}
			set
			{
				payments = value;
				SetProperty(ref payments, value);
				NotifyPropertyChanged();
			}
		}

		private string discounts = "...";
		public string Discounts
		{
			get { 
				return discounts; 
			}
			set
			{
				discounts = value;
				SetProperty(ref discounts, value);
				NotifyPropertyChanged();
			}
		}

		private string noSales = "...";
		public string NoSales
		{
			get { 
				return noSales; 
			}
			set
			{
				noSales = value;
				SetProperty(ref noSales, value);
				NotifyPropertyChanged();
			}
		}

		private string cancelled = "...";
		public string Cancelled
		{
			get { 
				return cancelled; 
			}
			set
			{
				cancelled = value;
				SetProperty(ref cancelled, value);
				NotifyPropertyChanged();
			}
		}

		private string returns = "...";
		public string Returns
		{
			get { 
				return returns; 
			}
			set
			{
				returns = value;
				SetProperty(ref returns, value);
				NotifyPropertyChanged();
			}
		}

		private string covers = "...";
		public string Covers
		{
			get { 
				return covers; 
			}
			set
			{
				covers = value;
				SetProperty(ref covers, value);
				NotifyPropertyChanged();
			}
		}
			
		public DashboardViewModel ()
		{
			MenuItems = new ObservableCollection<DashboardMenuItem>();
		}

		public void SetLoadingValues()
		{
			Payments = "...";
			Discounts = "...";
			NoSales = "...";
			Cancelled = "...";
			Returns = "...";
			Covers = "...";

			System.Diagnostics.Debug.WriteLine ("Values set to loading to dashboard item.");
		}

		public void AssignValues(DashboardItem totalItem)
		{
			NoSales = totalItem.NoSales.ToString ("#,##0");
			Cancelled = totalItem.Cancelled.ToString ("#,##0");
			Returns = (totalItem.Returns * -1).ToString ("#,##0");
			Covers = totalItem.Covers.ToString ("#,##0");

			System.Diagnostics.Debug.WriteLine ("Values assigned to dashboard item.");
		}
	}
}

