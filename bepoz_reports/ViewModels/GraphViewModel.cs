﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.Manager;
using bepoz_reports.Mvvm;

namespace bepoz_reports.ViewModels
{
	public class GraphViewModel : ViewModel
    {
		private double height = -1;

		public double Height
		{
			get
			{
				return height;
			}
			set
			{
				height = value;
				SetProperty(ref height, value);
			}
		}

		private double width = -1;

		public double Width
		{
			get
			{
				return width;
			}
			set
			{
				width = value;
				SetProperty(ref width, value);
			}
		}

		private List<string> labels = new List<string>();

		public List<string> Labels
		{
			get
			{
				return labels;
			}
			set
			{
				labels = value;
				SetProperty(ref labels, value);
			}
		}

		private List<string> hexColours = new List<string>();

		public List<string> HexColours
		{
			get
			{
				return hexColours;
			}
			set
			{
				hexColours = value;
				SetProperty(ref hexColours, value);
			}
		}

		private List<string> hexHighlightColours = new List<string>();

		public List<string> HexHighlightColours
		{
			get
			{
				return hexHighlightColours;
			}
			set
			{
				hexHighlightColours = value;
				SetProperty(ref hexHighlightColours, value);
			}
		}

		private List<List<double>> values = new List<List<double>>();

		public List<List<double>> Values
		{
			get
			{
				return values;
			}
			set
			{
				values = value;
				SetProperty(ref values, value);
			}
		}

		public PeriodItem CurrentPeriod;

		public GraphViewModel ()
        {

        }
    }
}

