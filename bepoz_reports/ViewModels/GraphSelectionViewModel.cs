﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;

using Xamarin.Forms;

using bepoz_reports.Mvvm;
using bepoz_reports.Pages;
using bepoz_reports.Tasks;

namespace bepoz_reports.ViewModels
{
	[ViewType(typeof(GraphSelectionPage))]
	public class GraphSelectionViewModel : ViewModel
	{
		private bool dataChange;

		public bool DataChange
		{
			get {
				dataChange = TaskManager.TaskComplete;

				return dataChange;
			}

			set
			{
				dataChange = value;

				this.NotifyPropertyChanged();
			}
		}

		public GraphSelectionViewModel ()
		{
		}

		public void Init()
		{
		}
	}
}

