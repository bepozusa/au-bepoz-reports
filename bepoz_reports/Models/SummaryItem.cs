using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PropertyChanged;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class SummaryItem : PropertyControl
    {
		private double val1;
		public double Val1
		{
			get { 
				return val1; 
			}
			set
			{
				if (value != val1)
				{
					val1 = value;
					OnPropertyChanged("Val1");
				}
			}
		}

		private double val2;
		public double Val2
		{
			get { 
				return val2; 
			}
			set
			{
				if (value != val2)
				{
					val2 = value;
					OnPropertyChanged("Val2");
				}
			}
		}

		private string title;
		public string Title
		{
			get { 
				return title; 
			}
			set
			{
				if (value != title)
				{
					title = value;
					OnPropertyChanged("Title");
				}
			}
		}

		public SummaryItem(string title, double value1, double value2)
		{
			Title = title;

			Val1 = value1;
			Val2 = value2;
		}
    }
}

