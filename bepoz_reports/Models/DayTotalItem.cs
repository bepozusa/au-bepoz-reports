using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Models
{
	public class DayTotalItem : ListItem
	{
		private string dayName = "";
		public string Day
		{
			get
			{
				return dayName;
			}
			set
			{
				dayName = value;
				OnPropertyChanged("DayName");
			}
		}

		private double costOfSales = 0;
		public double CostOfSales
		{
			get
			{
				return costOfSales;
			}
			set
			{
				costOfSales = value;
				OnPropertyChanged("CostOfSales");
			}
		}

		private double netTotal = 0;
		public double NetTotal
		{
			get
			{
				return netTotal;
			}
			set
			{
				netTotal = value;
				OnPropertyChanged("NetTotal");
			}
		}

		public DayTotalItem()
		{
		}
	}
}

