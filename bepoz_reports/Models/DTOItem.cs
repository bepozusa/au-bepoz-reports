using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class DTOItem : ListItem
	{
		private int groupID;
		public int GroupID
		{
			get { 
				return groupID; 
			}
			set
			{
				if (value != groupID)
				{
					groupID = value;
					OnPropertyChanged("GroupID");
				}
			}
		}

		private int id;
		public int ID
		{
			get { 
				return id; 
			}
			set
			{
				if (value != id)
				{
					id = value;
					OnPropertyChanged("ID");
				}
			}
		}

		private string name;
		public string Name
		{
			get { 
				return name; 
			}
			set
			{
				if (value != name)
				{
					name = value;
					OnPropertyChanged("Name");
				}
			}
		}

		public DTOItem()
		{
		}
	}
}

