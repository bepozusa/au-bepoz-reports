using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class DTOGroupItem : ListItem
	{
		private DTOItem[] children;
		public DTOItem[] Children
		{
			get { 
				return children; 
			}
			set
			{
				if (value != children)
				{
					children = value;
					OnPropertyChanged("Children");
				}
			}
		}

		private int groupID;
		public int GroupID
		{
			get { 
				return groupID; 
			}
			set
			{
				if (value != groupID)
				{
					groupID = value;
					OnPropertyChanged("GroupID");
				}
			}
		}

		private string name;
		public string Name
		{
			get { 
				return name; 
			}
			set
			{
				if (value != name)
				{
					name = value;
					OnPropertyChanged("Name");
				}
			}
		}

		public DTOGroupItem()
		{
		}
	}
}

