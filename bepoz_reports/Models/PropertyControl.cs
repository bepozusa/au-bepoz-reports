﻿using System;
using System.ComponentModel;

namespace bepoz_reports.Models
{
	public abstract class PropertyControl : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler(this, e);
		}

		protected void OnPropertyChanged(string propertyLessonName)
		{
			OnPropertyChanged(new PropertyChangedEventArgs(propertyLessonName));
		}
	}
}

