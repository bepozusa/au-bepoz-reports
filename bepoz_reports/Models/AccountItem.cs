using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;
using System.IO;

namespace bepoz_reports.Models
{
	public class AccountItem : ListItem
	{
		private string welcome;
		public string Welcome
		{
			get { 
				return welcome; 
			}
			set
			{
				if (value != welcome)
				{
					welcome = value;
					OnPropertyChanged("Welcome");
				}
			}
		}

		private string firstName;
		public string FirstName
		{
			get { 
				return firstName; 
			}
			set
			{
				if (value != firstName)
				{
					firstName = value;
					OnPropertyChanged("FirstName");
				}
			}
		}

		private string lastName;
		public string LastName
		{
			get { 
				return lastName; 
			}
			set
			{
				if (value != lastName)
				{
					lastName = value;
					OnPropertyChanged("LastName");
				}
			}
		}

		private string organisationName;
		public string OrganisationName
		{
			get { 
				return organisationName; 
			}
			set
			{
				if (value != organisationName)
				{
					organisationName = value;
					OnPropertyChanged("OrganisationName");
				}
			}
		}

		private string image;
		public string Image
		{
			get { 
				return image; 
			}
			set
			{
				if (value != image)
				{
					image = value;
					OnPropertyChanged("Image");
				}
			}
		}

		private ImageSource userImageSource;
		public ImageSource UserImageSource
		{
			get { 
				return userImageSource; 
			}
			set
			{
				if (value != userImageSource)
				{
					userImageSource = value;
					OnPropertyChanged("UserImageSource");
				}
			}
		}

		public AccountItem()
		{
		}

		public void CreateImageFromBase64()
		{
			string[] parts;

			try {
				Image = Image.Replace ("data:", "");

				parts = Image.Split (new char[]{','}, 2);

				System.Diagnostics.Debug.WriteLine ("Creating image from base 64 string.");

				if (parts.Length > 1);
					byte[] bytes = Convert.FromBase64String (parts[1]);

				UserImageSource = ImageSource.FromStream(() => new MemoryStream(bytes));
			}
			catch (Exception e) {
				System.Diagnostics.Debug.WriteLine ("Failed to create image.");
			}
		}
	}
}

