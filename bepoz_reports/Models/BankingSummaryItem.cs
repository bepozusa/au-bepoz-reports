using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Models
{
	public class BankingSummaryItem : ListItem
	{
		private double totalCashActual = 0;
		public double TotalCashActual
		{
			get { 
				return totalCashActual; 
			}
			set
			{
				if (value != totalCashActual)
				{
					totalCashActual = value;
					OnPropertyChanged("TotalCashActual");
				}
			}
		}

		private double totalCashTheoretical = 0;
		public double TotalCashTheoretical
		{
			get { 
				return totalCashTheoretical; 
			}
			set
			{
				if (value != totalCashTheoretical)
				{
					totalCashTheoretical = value;
					OnPropertyChanged("TotalCashTheoretical");
				}
			}
		}

		private double totalChequeActual = 0;
		public double TotalChequeActual
		{
			get { 
				return totalChequeActual; 
			}
			set
			{
				if (value != totalChequeActual)
				{
					totalChequeActual = value;
					OnPropertyChanged("totalChequeActual");
				}
			}
		}

		private double totalChequeTheoretical = 0;
		public double TotalChequeTheoretical
		{
			get { 
				return totalChequeTheoretical; 
			}
			set
			{
				if (value != totalChequeTheoretical)
				{
					totalChequeTheoretical = value;
					OnPropertyChanged("totalChequeTheoretical");
				}
			}
		}

		private double totalEFTPOSActual = 0;
		public double TotalEFTPOSActual
		{
			get { 
				return totalEFTPOSActual; 
			}
			set
			{
				if (value != totalEFTPOSActual)
				{
					totalEFTPOSActual = value;
					OnPropertyChanged("TotalEFTPOSActual");
				}
			}
		}

		private double totalEFTPOSTheoretical = 0;
		public double TotalEFTPOSTheoretical
		{
			get { 
				return totalEFTPOSTheoretical; 
			}
			set
			{
				if (value != totalEFTPOSTheoretical)
				{
					totalEFTPOSTheoretical = value;
					OnPropertyChanged("TotalEFTPOSTheoretical");
				}
			}
		}

		public BankingSummaryItem()
		{
		}
	}
}

