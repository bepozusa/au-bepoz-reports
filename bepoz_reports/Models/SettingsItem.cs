using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class SettingsItem : ListItem
	{
		private string title;
		public string Title
		{
			get { 
				return title; 
			}
			set
			{
				if (value != title)
				{
					title = value;
					OnPropertyChanged("Title");
				}
			}
		
		}

		private int settingsType;
		public int SettingsType
		{
			get { 
				return settingsType; 
			}
			set
			{
				if (value != settingsType)
				{
					settingsType = value;
					OnPropertyChanged("SettingsType");
				}
			}
		}

		public SettingsItem()
		{
		}

		public SettingsItem(int sType, string title)
		{
			SettingsType = sType;
			Title = title;
		}
	}
}

