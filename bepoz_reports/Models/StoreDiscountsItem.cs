using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class StoreDiscountsItem : DiscountsItem
	{
		private string store;
		public string Store
		{
			get { 
				return store; 
			}
			set
			{
				if (value != store)
				{
					store = value;
					OnPropertyChanged("Store");
				}
			}
		}
	}
}

