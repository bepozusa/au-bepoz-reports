using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Models
{
	public class DiscountsSummaryItem : ListItem
	{
		private IList<DiscountsItem> discountItems = new List<DiscountsItem>();

		public IList<DiscountsItem> DiscountItems
		{
			get
			{
				return discountItems;
			}
			set
			{
				discountItems = value;
				OnPropertyChanged("DiscountItems");
			}
		}

		public DiscountsSummaryItem()
		{
		}

		public void AddToDiscountItems(string discount, double amount, double qty)
		{
			foreach (DiscountsItem item in DiscountItems) 
			{
				if (item.Discount.Contains (discount)) {
					item.Amount += amount;
					item.Qty += qty;

					return;
				}
			}

			DiscountItems.Add (new DiscountsItem (){ Discount = discount, Amount = amount, Qty = qty});
		}
	}
}

