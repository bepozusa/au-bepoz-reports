using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class VenueDiscountsItem : DiscountsItem
	{
		private double amountValue;
		public double AmountValue
		{
			get { 
				return amountValue; 
			}
			set
			{
				if (value != amountValue)
				{
					amountValue = value;
					OnPropertyChanged("AmountValue");
				}
			}
		}
	}
}

