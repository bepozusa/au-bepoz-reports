﻿using System;

namespace bepoz_reports.Controls
{
	public class SelectedPickerObject
	{
		public string Title;
		public string ImageName;

		public int ImageId;
		public int Type;

		public CustomPickerObject ()
		{
		}
	}
}

