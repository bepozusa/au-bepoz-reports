using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class DailyTotalsItem : ListItem
	{
		private int workstationID;
		public int WorkstationID
		{
			get { 
				return workstationID; 
			}
			set
			{
				if (value != workstationID)
				{
					workstationID = value;
					OnPropertyChanged("WorkstationID");
				}
			}
		}

		private int shiftID;
		public int ShiftID
		{
			get { 
				return shiftID; 
			}
			set
			{
				if (value != shiftID)
				{
					shiftID = value;
					OnPropertyChanged("ShiftID");
				}
			}
		}

		private string day;
		public string Day
		{
			get { 
				return day; 
			}
			set
			{
				if (value != day)
				{
					day = value;
					OnPropertyChanged("Day");
				}
			}
		}

		private float costOfSales;
		public float CostOfSales
		{
			get { 
				return costOfSales; 
			}
			set
			{
				if (value != costOfSales)
				{
					costOfSales = value;
					OnPropertyChanged("CostOfSales");
				}
			}
		}

		private float nettTotal;
		public float NettTotal
		{
			get { 
				return nettTotal; 
			}
			set
			{
				if (value != nettTotal)
				{
					nettTotal = value;
					OnPropertyChanged("NettTotal");
				}
			}
		}

		private DailyTotalsSummaryItem summary = new DailyTotalsSummaryItem();
		public DailyTotalsSummaryItem Summary
		{
			get { 
				return summary; 
			}
			set
			{
				if (value != summary)
				{
					summary = value;
					OnPropertyChanged("Summary");
				}
			}
		}

		public DailyTotalsItem()
		{
		}
	}
}

