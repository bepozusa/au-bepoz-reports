using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class DiscountDetailItem : ListItem
	{
		private double amount;
		public double Amount
		{
			get { 
				return amount; 
			}
			set
			{
				if (value != amount)
				{
					amount = value;
					OnPropertyChanged("Amount");
				}
			}
		}

		private string discount;
		public string Discount
		{
			get { 
				return discount; 
			}
			set
			{
				if (value != discount)
				{
					discount = value;
					OnPropertyChanged("Discount");
				}
			}
		}

		private string qty;
		public string Qty
		{
			get { 
				return qty; 
			}
			set
			{
				if (value != qty)
				{
					qty = value;
					OnPropertyChanged("Qty");
				}
			}
		}
	}
}

