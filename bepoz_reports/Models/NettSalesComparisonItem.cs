using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Models
{
	public class NettSalesComparisonItem : ListItem
	{
		private int workStationID = -1;
		public int WorkStationID
		{
			get { 
				return workStationID; 
			}
			set
			{
				if (value != workStationID)
				{
					workStationID = value;
					OnPropertyChanged("WorkStationID");
				}
			}
		}
			
		private string payment = "";
		public string Payment
		{
			get { 
				return payment; 
			}
			set
			{
				if (value != payment)
				{
					payment = value;
					OnPropertyChanged("PaymentType");
				}
			}
		}

		private double theoretical = 0;
		public double Theoretical
		{
			get { 
				return theoretical; 
			}
			set
			{
				if (value != theoretical)
				{
					theoretical = value;
					OnPropertyChanged("Theoretical");
				}
			}
		}

		private double actual = 0;
		public double Actual
		{
			get { 
				return actual; 
			}
			set
			{
				if (value != actual)
				{
					actual = value;
					OnPropertyChanged("Actual");
				}
			}
		}

		private double shiftDate = 0;
		public double ShiftDate
		{
			get { 
				return shiftDate; 
			}
			set
			{
				if (value != shiftDate)
				{
					shiftDate = value;
					OnPropertyChanged("ShiftDate");
				}
			}
		}

		private BankingSummaryItem summary = new BankingSummaryItem ();
		public BankingSummaryItem Summary
		{
			get { 
				return summary; 
			}
			set
			{
				if (value != summary)
				{
					summary = value;
					OnPropertyChanged("Summary");
				}
			}
		}

		public NettSalesComparisonItem()
		{

		}
	}
}

