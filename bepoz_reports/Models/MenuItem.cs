using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PropertyChanged;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class MenuItem
    {
		public string Title { get; set; }
		public int ReportType { get; set; }
		public ImageSource IconSource { get; set; }

		public MenuItem(string title)
		{
			Title = title;
		}

		public MenuItem(string title, ImageSource source)
		{
			Title = title;
			IconSource = source;
		}

		public MenuItem(string title, int rType, string fileName)
		{
			Title = title;
			ReportType = rType;
			IconSource = ImageSource.FromFile(fileName);
		}
    }
}

