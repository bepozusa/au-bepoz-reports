using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PropertyChanged;

namespace bepoz_reports.Models
{
	public class DashboardMenuItem
    {
		public string Title { get; set; }
		public string Amount { get; set; }

		public DashboardMenuItem(string title, double amount)
		{
			Title = title;
			Amount = amount.ToString ("#,##0");
		}
    }
}

