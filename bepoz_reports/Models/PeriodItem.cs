using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class PeriodItem : ListItem
	{
		private string title;
		public string Title
		{
			get { 
				return title; 
			}
			set
			{
				if (value != title)
				{
					title = value;
					OnPropertyChanged("Title");
				}
			}
		
		}

		private int interval;
		public int Interval
		{
			get { 
				return interval; 
			}
			set
			{
				if (value != interval)
				{
					interval = value;
					OnPropertyChanged("Interval");
				}
			}
		}

		public string DateFrom_oData;
		public string DateTo_oData;

		private string dateFrom;
		public string DateFrom
		{
			get { 
				return dateFrom; 
			}
			set
			{
				if (value != dateFrom)
				{
					dateFrom = value;
					OnPropertyChanged("DateFrom");
				}
			}
		}

		private string dateTo;
		public string DateTo
		{
			get { 
				return dateTo; 
			}
			set
			{
				if (value != dateTo)
				{
					dateTo = value;
					OnPropertyChanged("DateTo");
				}
			}
		}

		private int reportType;
		public int ReportType
		{
			get { 
				return reportType; 
			}
			set
			{
				if (value != reportType)
				{
					reportType = value;
					OnPropertyChanged("ReportType");
				}
			}
		}
			
		private Color color;
		public Color Color
		{
			get { return color; }
			set
			{
				color = value;
				OnPropertyChanged("Color");
			}
		}

		public PeriodItem()
		{
		}

		public PeriodItem(int rType, string title, int anInterval)
		{
			ReportType = rType;
			Title = title;
			Interval = anInterval;
		}
	}
}

