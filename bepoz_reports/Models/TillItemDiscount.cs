using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class TillItemDiscount : BankingItem
	{
		private List<DiscountDetailItem> discountItems = new List<DiscountDetailItem>() ;
		public List<DiscountDetailItem> DiscountItems
		{
			get { 
				return discountItems; 
			}
			set
			{
				if (value != discountItems)
				{
					discountItems = value;
					OnPropertyChanged("DiscountItems");
				}
			}
		}
	
		public TillItemDiscount()
		{
		}
	}
}

