﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using bepoz_reports.Models;

namespace bepoz_reports
{
	public class SummaryChartModel : PropertyControl
	{
		public List<string> Graphs = new List<string> ();

		public List<int> GraphDataSIndexFlags = new List<int>();
		public List<int> GraphDataEIndexFlags = new List<int>();

		private double height = -1;
		public double Height
		{
			get	{
				return height;
			}
			set {
				if (value != height)
				{
					height = value;
					OnPropertyChanged("Height");
				}
			}
		}

		private double width = -1;
		public double Width
		{
			get {
				return width;
			}
			set {
				if (value != width)
				{
					width = value;
					OnPropertyChanged("Width");
				}
			}
		}

		private List<string> labels = new List<string>();
		public List<string> Labels
		{
			get {
				return labels;
			}
			set {
				if (value != labels)
				{
					labels = value;
					OnPropertyChanged("Labels");
				}
			}
		}

		private List<string> legendLabels = new List<string>();
		public List<string> LegendLabels
		{
			get {
				return legendLabels;
			}
			set {
				if (value != legendLabels)
				{
					legendLabels = value;
					OnPropertyChanged("LegendLabels");
				}
			}
		}

		private List<string> hexColours = new List<string>() {"#0c2d62", "#c3bbb2"};
		public List<string> HexColours
		{
			get
			{
				return hexColours;
			}
			set
			{
				if (value != hexColours)
				{
					hexColours = value;
					OnPropertyChanged("HexColours");
				}
			}
		}

		private List<string> hexHighlightColours = new List<string>();
		public List<string> HexHighlightColours
		{
			get
			{
				return hexHighlightColours;
			}
			set
			{
				if (value != hexHighlightColours)
				{
					hexHighlightColours = value;
					OnPropertyChanged("HexHighlightColours");
				}
			}
		}

		private List<List<double>> values = new List<List<double>>();
		public List<List<double>> Values
		{
			get
			{
				return values;
			}
			set
			{
				if (value != values)
				{
					values = value;
					OnPropertyChanged("Values");
				}
			}
		}

		public SummaryChartModel ()
		{
		}
	}
}

