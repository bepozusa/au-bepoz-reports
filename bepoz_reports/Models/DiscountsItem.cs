using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace bepoz_reports.Models
{
	public class DiscountsItem : ListItem
	{
		private int workstationID;
		public int WorkstationID
		{
			get { 
				return workstationID; 
			}
			set
			{
				if (value != workstationID)
				{
					workstationID = value;
					OnPropertyChanged("WorkstationID");
				}
			}
		}

		private int shiftID;
		public int ShiftID
		{
			get { 
				return shiftID; 
			}
			set
			{
				if (value != shiftID)
				{
					shiftID = value;
					OnPropertyChanged("ShiftID");
				}
			}
		}

		private double amount;
		public double Amount
		{
			get { 
				return amount; 
			}
			set
			{
				if (value != amount)
				{
					amount = value;
					OnPropertyChanged("Amount");
				}
			}
		}

		private string discount;
		public string Discount
		{
			get { 
				return discount; 
			}
			set
			{
				if (value != discount)
				{
					discount = value;
					OnPropertyChanged("Discount");
				}
			}
		}

		private double qty;
		public double Qty
		{
			get { 
				return qty; 
			}
			set
			{
				if (value != qty)
				{
					qty = value;
					OnPropertyChanged("Qty");
				}
			}
		}

		private DiscountsSummaryItem summary = new DiscountsSummaryItem();
		public DiscountsSummaryItem Summary
		{
			get { 
				return summary; 
			}
			set
			{
				if (value != summary)
				{
					summary = value;
					OnPropertyChanged("Summary");
				}
			}
		}
	}
}

