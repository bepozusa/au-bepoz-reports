using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Models
{
	public class DailyTotalsSummaryItem : ListItem
	{
		private IList<DayTotalItem> dayTotalItems = new List<DayTotalItem>();

		public IList<DayTotalItem> DayTotalItems
		{
			get
			{
				return dayTotalItems;
			}
			set
			{
				dayTotalItems = value;
				OnPropertyChanged("DayTotalItems");
			}
		}

		public DailyTotalsSummaryItem()
		{
		}

		public void AddToDayTotalItems(string day, double costOfSales, double netTotal)
		{
			foreach (DayTotalItem item in DayTotalItems) 
			{
				if (item.Day.Contains (day)) {
					item.CostOfSales += costOfSales;
					item.NetTotal += netTotal;

					return;
				}
			}

			DayTotalItems.Add (new DayTotalItem (){ Day = day, CostOfSales = costOfSales, NetTotal = netTotal});
		}
	}
}

