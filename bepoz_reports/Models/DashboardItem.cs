using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;
using System.IO;

namespace bepoz_reports.Models
{
	public class DashboardItem : ListItem
	{
		private double noSales = 0;
		public double NoSales
		{
			get { 
				return noSales; 
			}
			set
			{
				if (value != noSales)
				{
					noSales = value;
					OnPropertyChanged("NoSales");
				}
			}
		}

		private double cancelled = 0;
		public double Cancelled
		{
			get { 
				return cancelled; 
			}
			set
			{
				if (value != cancelled)
				{
					cancelled = value;
					OnPropertyChanged("Cancelled");
				}
			}
		}

		private double returns = 0;
		public double Returns
		{
			get { 
				return returns; 
			}
			set
			{
				if (value != returns)
				{
					returns = value;
					OnPropertyChanged("Returns");
				}
			}
		}

		private double covers = 0;
		public double Covers
		{
			get { 
				return covers; 
			}
			set
			{
				if (value != covers)
				{
					covers = value;
					OnPropertyChanged("Covers");
				}
			}
		}

		private double nettTotal = 0;
		public double NettTotal
		{
			get { 
				return nettTotal; 
			}
			set
			{
				if (value != nettTotal)
				{
					nettTotal = value;
					OnPropertyChanged("NettTotal");
				}
			}
		}

		private double qtyOrders = 0;
		public double QtyOrders
		{
			get { 
				return qtyOrders; 
			}
			set
			{
				if (value != qtyOrders)
				{
					qtyOrders = value;
					OnPropertyChanged("QtyOrders");
				}
			}
		}

		private double averageOrder = 0;
		public double AverageOrder
		{
			get { 
				return averageOrder; 
			}
			set
			{
				if (value != averageOrder)
				{
					averageOrder = value;
					OnPropertyChanged("AverageOrder");
				}
			}
		}

		public void AddDashboardTotals(DashboardItem item)
		{
			NoSales += item.NoSales;
			Cancelled += item.Cancelled;
			Returns += item.Returns;
			Covers += item.Covers;
			NettTotal += item.NettTotal;
			QtyOrders += item.QtyOrders;
			AverageOrder += item.AverageOrder;
		}

		public DashboardItem()
		{
		}
	}
}

