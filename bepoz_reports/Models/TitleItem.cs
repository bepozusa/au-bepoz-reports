using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace bepoz_reports.Models 
{
	public class TitleItem
	{
		public string Title;

		public int LessonNumber;
		public int LibraryNumber;
	}
}