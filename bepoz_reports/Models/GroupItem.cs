using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

namespace bepoz_reports.Models 
{
	public class GroupItem : List<ListItem>, INotifyPropertyChanged
	{
		private string headerTitle;
		public string HeaderTitle
		{
			get { 
				return headerTitle; 
			}
			set
			{
				if (value != headerTitle)
				{
					headerTitle = value;
					OnPropertyChanged("HeaderTitle");
				}
			}
		}

		private Color textcolor;
		public Color TextColor
		{
			get { return textcolor; }
			set
			{
				textcolor = value;
				OnPropertyChanged("TextColor");
			}
		}

		private Color backgroundColor;
		public Color BackgroundColor
		{
			get { return backgroundColor; }
			set
			{
				backgroundColor = value;
				OnPropertyChanged("BackgroundColor");
			}
		}
			
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler(this, e);
		}

		protected void OnPropertyChanged(string propertyLessonName)
		{
			OnPropertyChanged(new PropertyChangedEventArgs(propertyLessonName));
		}
	}
}