﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using bepoz_reports.Models;

namespace bepoz_reports
{
	public class GraphModel : PropertyControl
	{
		public List<string> Graphs = new List<string> ();

		public List<int> GraphDataSIndexFlags = new List<int>();
		public List<int> GraphDataEIndexFlags = new List<int>();

		public GraphModel ()
		{
		}
	}
}

