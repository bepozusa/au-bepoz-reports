﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using bepoz_reports.Models;

namespace bepoz_reports
{
	public class SummaryGridModel : PropertyControl
	{
		public List<string> Titles = new List<string>();
		public List<string> Graphs = new List<string>();
		public List<string> PageTitles = new List<string>();
		public List<int> GraphDataSetFlags = new List<int>();

		public List<SummaryItem> Items = new List<SummaryItem>();

		public ObservableCollection<SummaryItem> ListData { get; set; }

		private string heading1 = "";
		public string Heading1
		{
			get { 
				return heading1; 
			}
			set
			{
				if (value != heading1)
				{
					heading1 = value;
					OnPropertyChanged("Heading1");
				}
			}
		}

		private string heading2 = "";
		public string Heading2
		{
			get { 
				return heading2; 
			}
			set
			{
				if (value != heading2)
				{
					heading2 = value;
					OnPropertyChanged("Heading2");
				}
			}
		}

		public void CreateItems()
		{
			foreach (string title in Titles) {
				Items.Add (new SummaryItem (title, 0.0, 0.0));
			}

			System.Diagnostics.Debug.WriteLine ("Created " + Items.Count + " items for grid model.");
		}

		public void LoadValues(List<double> Values1, List<double> Values2)
		{
			int count = 0;

			foreach (double val in Values1) {
				Items[count].Val1 = val;
				count++;
			}

			count = 0;

			foreach (double val in Values2) {
				Items[count].Val2 = val;
				count++;
			}

			System.Diagnostics.Debug.WriteLine ("Loaded values for items in grid model.");
		}

		public void CreateListData()
		{
			ListData = new ObservableCollection<SummaryItem> ();

			foreach (SummaryItem item in Items) {
				ListData.Add (item);
			}

			System.Diagnostics.Debug.WriteLine ("Created list data for grid model.");
		}

		public SummaryGridModel ()
		{
		}
	}
}

