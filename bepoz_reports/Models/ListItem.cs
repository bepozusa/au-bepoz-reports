﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports.Manager;

namespace bepoz_reports
{
	public class ListItem : PropertyControl
	{
		private string till = "";
		public string Till
		{
			get { 
				return till; 
			}
			set
			{
				if (value != till)
				{
					till = value;
					OnPropertyChanged("Till");
				}
			}
		}

		private string store = "";
		public string Store
		{
			get { 
				return store; 
			}
			set
			{
				if (value != store)
				{
					store = value;
					OnPropertyChanged("Store");
				}
			}
		}

		private string venue = "";
		public string Venue
		{
			get { 
				return venue; 
			}
			set
			{
				if (value != venue)
				{
					venue = value;
					OnPropertyChanged("Venue");
				}
			}
		}

		private int storeGroup = -1;
		public int StoreGroup
		{
			get { 
				return storeGroup; 
			}
			set
			{
				if (value != storeGroup)
				{
					storeGroup = value;
					OnPropertyChanged("StoreGroup");
				}
			}
		}

		private int venueGroup = 1;
		public int VenueGroup
		{
			get { 
				return venueGroup; 
			}
			set
			{
				if (value != venueGroup)
				{
					venueGroup = value;
					OnPropertyChanged("VenueGroup");
				}
			}
		}

		private int venueID;
		public int VenueID
		{
			get { 
				return venueID; 
			}
			set
			{
				if (value != venueID)
				{
					venueID = value;
					OnPropertyChanged("VenueID");
				}
			}
		}

		private int storeID;
		public int StoreID
		{
			get { 
				return storeID; 
			}
			set
			{
				if (value != storeID)
				{
					storeID = value;
					OnPropertyChanged("StoreID");
				}
			}
		}

		private string title = "";
		public string Title
		{
			get { 
				return title; 
			}
			set
			{
				if (value != title)
				{
					title = value;
					OnPropertyChanged("Title");
				}
			}
		}

		private ImageSource iconSource;
		public ImageSource IconSource
		{
			get { 
				return iconSource; 
			}
			set
			{
				if (value != iconSource)
				{
					iconSource = value;
					OnPropertyChanged("IconSource");
				}
			}
		}
		public void SetContentItems()
		{
			switch (ReportManager.CurrentType)
			{
				case 0:
					Title = Till;
					IconSource = ImageSource.FromFile ("till.png");
					break;
				case 1:
					Title = Venue;
					IconSource = ImageSource.FromFile ("store.png");
					break;
				case 2:
					Title = Store;
					IconSource = ImageSource.FromFile ("store.png");
					break;				
			}
		}

		public ListItem ()
		{
		}
	}
}

