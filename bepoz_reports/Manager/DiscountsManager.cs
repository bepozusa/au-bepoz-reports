﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using bepoz_reports;
using Newtonsoft.Json;
using bepoz_reports.Models;
using bepoz_reports.Colours;
using bepoz_reports.WebServices;
using bepoz_reports.Manager;

namespace bepoz_reports.Manager
{
	public class DiscountsManager : DataManager<DiscountsItem>
	{
		public DiscountsManager()
		{
			System.Diagnostics.Debug.WriteLine ("Initiating discounts manager.");
			CreateListData = true;
		}

		public override void InitGridModel ()
		{
			GridModel = new SummaryGridModel ();
			GridModel.PageTitles = new List<string> (){"Amount vs Qty", "Qty Comparison", "Qty vs Amount", "Amount Comparison"};
			GridModel.Graphs = new List<string> (){"pie", "line", "bar"};
			GridModel.GraphDataSetFlags = new List<int> (){1, 0, 0};
			GridModel.Heading1 = "Amount";
			GridModel.Heading2 = "Qty";
		}

		public override void AddToExisting(DiscountsItem ListItem, DiscountsItem currentItem)
		{
			ListItem.Summary.AddToDiscountItems (currentItem.Discount, currentItem.Amount, currentItem.Qty);
		}
	}
}

