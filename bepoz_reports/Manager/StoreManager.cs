﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Newtonsoft.Json;

using bepoz_reports.WebServices;
using bepoz_reports.Models;

namespace bepoz_reports.Manager
{
	public static class StoreManager
	{
		public static IList<DTOGroupItem> Stores;

		public static int CurrentStoreGroup;
		public static int CurrentStore;

		public static string[] jsonstrings;
		public static string UnprocessedJSONString;

		static StoreManager ()
		{
			ResetCurrentFilter ();

			Stores = new List<DTOGroupItem> ();
		}

		public static void ResetCurrentFilter()
		{
			CurrentStoreGroup = -1;
			CurrentStore = -1;
		}

		public static async Task<bool> CreateStoreDataRequest()
		{
			System.Diagnostics.Debug.WriteLine ("Start Http request for store list");

			try {
				UnprocessedJSONString = await ServiceManager.DataService.GetValues (ServiceManager.AccessToken, "Stores");
	
				CreateStoreList ();
			}
			catch (Exception e) {
				return false;
			}

			return true;
		}

		public static void CreateStoreList()
		{
			// remove first and last characters
			string processJSON = UnprocessedJSONString.Remove(UnprocessedJSONString.Length - 1);
			processJSON = processJSON.Substring(1);

			// brake response string down into seperate json strings
			jsonstrings = processJSON.Split (new string[] { "]},{" }, StringSplitOptions.None);

			if (jsonstrings.Length < 1)
				return;

			System.Diagnostics.Debug.WriteLine ("Process " + jsonstrings.Length + " JSON strings.");

			if (jsonstrings.Length > 1) {
				jsonstrings [0] = jsonstrings [0] + "]}";

				// add in cosing bracks around each json string
				for (int i = 1; i < jsonstrings.Length - 1; i++) {
					jsonstrings [i] = "{" + jsonstrings [i] + "]}";
				}

				jsonstrings [jsonstrings.Length - 1] = "{" + jsonstrings [jsonstrings.Length - 1];
			}

			try {
				// for all json strings create unique stores list
				for (int i = 0; i < jsonstrings.Length; i++) {
					DTOGroupItem currentVenue = JsonConvert.DeserializeObject<DTOGroupItem> (jsonstrings [i]);	

					Stores.Add (currentVenue);
				}
			}
			catch (Exception e) {
				System.Diagnostics.Debug.WriteLine ("Error with deserializing " + e);
			}

			System.Diagnostics.Debug.WriteLine ("Total store groups " + Stores.Count);
		}
	}
}

