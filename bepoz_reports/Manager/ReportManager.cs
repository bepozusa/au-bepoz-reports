﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Models;
using bepoz_reports;
using bepoz_reports.Colours;
using bepoz_reports.WebServices;
using bepoz_reports.Date;

using Newtonsoft.Json;

namespace bepoz_reports.Manager
{
	public static class ReportManager
	{
		public static IList<string> ReportSettings = new List<string>() {"Till", "Venue", "Store"};
		public static IList<string> PeriodTitles = new List<string>() {"Today", "Yesterday", "Last Week", "Current Month", "Last Month", "Current Year", "Last Year", "All Time", "Custom"};
		public static IList<int> Intervals = new List<int>() {1, 6, 7, 3, 8, 4, 9, 98, 0};

		public static IList<GroupItem> PeriodData;
		public static IList<GroupItem> ReportSettingsData;

		public static PeriodItem CurrentPeriod;

		// string filters date, store, venue and type
		public static string CurrentDate;
		public static string CurrentStore;
		public static string CurrentVenue;
		public static string CurrentTypeStr;

		public static string ReportPeriod;
		public static string CustomDateFrom;
		public static string CustomDateTo;
		public static int CurrentInterval;
		public static int CurrentType;

		public static bool isCustom;

		public static GroupItem ReportingGroup;

		static ReportManager()
		{
			System.Diagnostics.Debug.WriteLine ("Initiating Report manager.");

			PeriodData = new List<GroupItem> ();
			ReportSettingsData = new List<GroupItem> ();

			createPeriodListData (PeriodData);
			createReportSettingsData (ReportSettingsData);

			CurrentDate = "Today";
			CurrentStore = "Select a Store";
			CurrentVenue = "Select a Venue";
			CurrentTypeStr = "Till";

			CurrentType = 0;

			CurrentPeriod = new PeriodItem () { Title = PeriodTitles [0], Interval = Intervals[0] };
		}

		public static void SetDateText()
		{
			if (isCustom) {
				SetCustomDates (CustomDateFrom, CustomDateTo);
				CurrentDate = ReportManager.CustomDateFrom + " - " + ReportManager.CustomDateTo;
				ReportManager.SetCurrentPeriodCustom ();
			} else 
				CurrentDate = ReportManager.ReportPeriod;
		}

		public static void SetCurrentPeriod()
		{
			CurrentPeriod = new PeriodItem(){Title = ReportPeriod, Interval = CurrentInterval};
			SetDateText ();
		}

		public static void SetCurrentPeriodCustom()
		{
			SetCustomDates (CustomDateFrom, CustomDateTo);
		}

		public static void SetCustomDates(string dateFrom, string dateTo)
		{
			System.Diagnostics.Debug.WriteLine ("Setting custom 'dateFrom' to " + dateFrom + " and 'dateTo' to " + dateTo);

			CurrentPeriod.DateFrom = dateFrom + " 00:00:00 AM";
			CurrentPeriod.DateFrom_oData = dateFrom + "T00:00:00.000Z";

			CurrentPeriod.DateTo = dateTo + " 00:00:00 AM";
			CurrentPeriod.DateTo_oData = dateTo + "T00:00:00.000Z";
		}

		public static void createPeriodListData(IList<GroupItem> Data)
		{
			System.Diagnostics.Debug.WriteLine ("Creating list of periods");

			ReportingGroup = new GroupItem () { HeaderTitle = "Reporting Period" };
			ReportingGroup.TextColor = Colour.header_text_colour.ToFormsColor();
			ReportingGroup.BackgroundColor = Colour.header_background_colour.ToFormsColor();

			for (int index = 0; index < PeriodTitles.Count; index++) 
			{
				PeriodItem newItem = new PeriodItem(index, PeriodTitles[index], Intervals[index]);

				ReportingGroup.Add (newItem);
			}
				
			Data.Add (ReportingGroup);
		}

		public static void createReportSettingsData(IList<GroupItem> Data)
		{
			System.Diagnostics.Debug.WriteLine ("Creating report settings data.");

			ReportingGroup = new GroupItem () { HeaderTitle = "Report Settings" };
			ReportingGroup.TextColor = Colour.header_text_colour.ToFormsColor();
			ReportingGroup.BackgroundColor = Colour.header_background_colour.ToFormsColor();

			for (int index = 0; index < ReportSettings.Count; index++) 
			{
				SettingsItem newItem = new SettingsItem(index, ReportSettings[index]);

				ReportingGroup.Add (newItem);
			}

			Data.Add (ReportingGroup);
		}
	}
}

