﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using bepoz_reports.Models;

namespace bepoz_reports.Manager
{
	public class NettSalesComparisonManager : DataManager<BankingItem>
	{
		public NettSalesComparisonManager()
		{
			System.Diagnostics.Debug.WriteLine ("Initiating nett sales comparison manager.");
			CreateListData = true;
		}

		public override double CalculateTotal()
		{
			double totalPayments = 0;

			foreach (BankingItem bItem in Items) {
				totalPayments += bItem.Theoretical;
			}

			System.Diagnostics.Debug.WriteLine ("Total payments value - " + totalPayments);

			return totalPayments;
		}

		public override void InitGridModel ()
		{
			GridModel = new SummaryGridModel ();
			GridModel.PageTitles = new List<string> (){"Theoretical vs Actual", "Theoretical vs Actual", "Theoretical"};
			GridModel.Titles = new List<string> (){"Cash", "Cheque", "EFTPOS"};
			GridModel.Graphs = new List<string> (){"pie", "bar"};
			GridModel.GraphDataSIndexFlags = new List<int> (){0, 0};
			GridModel.GraphDataEIndexFlags = new List<int> (){1, 1};
			GridModel.Heading1 = "Theoretical";
			GridModel.Heading2 = "Actual";

			GridModel.CreateItems ();

			System.Diagnostics.Debug.WriteLine ("Banking grid model created.");
		}

		public override void AddToExisting (BankingItem ListItem, BankingItem currentItem)
		{
			if (currentItem.Payment.Contains ("Cash")) {
				ListItem.Summary.TotalCashTheoretical += currentItem.Theoretical;
				ListItem.Summary.TotalCashActual += currentItem.Actual;
			}

			else if (currentItem.Payment.Contains ("Cheque")) {
				ListItem.Summary.TotalChequeTheoretical += currentItem.Theoretical;
				ListItem.Summary.TotalChequeActual += currentItem.Actual;
			}

			else if (currentItem.Payment.Contains ("EFTPOS")) {
				ListItem.Summary.TotalEFTPOSTheoretical += currentItem.Theoretical;
				ListItem.Summary.TotalEFTPOSActual += currentItem.Actual;
			}
		}
	}
}

