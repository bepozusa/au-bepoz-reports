﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Manager;

namespace bepoz_reports.Manager
{
	public class DashboardManager : DataManager<DashboardItem>
	{
		public DashboardItem TotalDashboardItem;

		public DashboardManager()
		{
			CreateListData = false;
		}

		public void CalculateTotalFigures()
		{
			TotalDashboardItem = new DashboardItem ();

			foreach (DashboardItem dashboardItem in Items)
			{
				TotalDashboardItem.AddDashboardTotals(dashboardItem);
			}
		}
	
		public override void InitGridModel (){}

		public override bool checkItemExistsInList(DashboardItem currentItem)
		{
			// for all items
			foreach (DashboardItem item in Items) 
			{
				// if item already exists add values to existing item based on specific payment types
				if (item.Venue.Equals (currentItem.Venue)) 
				{	
					AddToExisting (item, currentItem);
					return true;
				}
			}

			return false;
		}

		public override void AddToExisting (DashboardItem ListItem, DashboardItem currentItem)
		{
			ListItem.AddDashboardTotals(currentItem);
		}
	}
}

