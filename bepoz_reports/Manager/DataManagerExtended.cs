﻿using System;
using System.Collections.Generic;

using bepoz_reports.Models;

namespace bepoz_reports.Manager
{
	public class DateManagerExtended<T> : DataManager<T, List<T>> where T : ListItem
	{
		public override void createUniqueList(JSONItem<List<T>> jsonItem) 
		{
			// for all json strings create unique items
			for (int i = 0; i < jsonItem.Results.Count; i++) {
				CheckItemUniqueness (jsonItem.Results [i] as T);
			}

			System.Diagnostics.Debug.WriteLine ("DateManagerExtended: Total items: " + Items.Count);

			CreateListData ();
		}

		public override void CheckItemUniqueness(T currentItem)
		{
			if (!checkItemExistsInList (currentItem)) 
			{
				if (passStoreCriteria (currentItem)) 
				{
					currentItem.SetContentItems();
					Items.Add (currentItem);
				}
			}
		}
	}
}

