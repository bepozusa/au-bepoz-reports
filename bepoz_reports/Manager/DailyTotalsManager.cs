﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Newtonsoft.Json;

using bepoz_reports;
using bepoz_reports.Models;
using bepoz_reports.Colours;
using bepoz_reports.WebServices;
using bepoz_reports.Manager;

namespace bepoz_reports.Manager
{
	public class DailyTotalsManager : DataManager<DailyTotalsItem>
	{
		public DailyTotalsManager()
		{
			System.Diagnostics.Debug.WriteLine ("Initiating daily totals manager.");
			CreateListData = true;
		}

		public override void InitGridModel ()
		{
			GridModel = new SummaryGridModel ();
			GridModel.PageTitles = new List<string> (){"Cost of Sales vs Net Total", "Cost of Sales", "Nett Total", "Sales by Day"};
			GridModel.Graphs = new List<string> (){"pie", "pie", "bar"};
			GridModel.GraphDataSetFlags = new List<int> (){1, 1, 1};
			GridModel.Heading1 = "Cost Of Sales";
			GridModel.Heading2 = "Net Total";
		}

		public override void AddToExisting(DailyTotalsItem ListItem, DailyTotalsItem currentItem)
		{
			ListItem.Summary.AddToDayTotalItems (currentItem.Day, currentItem.CostOfSales, currentItem.NettTotal);
		}
	}
}

