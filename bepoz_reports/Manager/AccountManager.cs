﻿using System;
using System.Threading.Tasks;

using bepoz_reports.Manager;
using bepoz_reports.Models;

using Newtonsoft.Json;

using Xamarin.Forms;

namespace bepoz_reports.Manager
{
	public static class AccountManager
	{
		public static string[] jsonstrings;
		public static string UnprocessedJSONString;

		public static AccountItem CurrentUserInfo;

		public static string Username = "";
		public static bool RememberUsername;

		static AccountManager ()
		{
		}

		public static void InitCurrentUserInfo()
		{
			System.Diagnostics.Debug.WriteLine ("Initialising user info in Account Manager.");

			CurrentUserInfo = new AccountItem();
			CurrentUserInfo.OrganisationName = "(Organization Name)";
			CurrentUserInfo.FirstName = "(Username)";
			CurrentUserInfo.LastName = "";
		}

		public static async Task<bool> CreateUserInfoRequest()
		{
			System.Diagnostics.Debug.WriteLine ("Start Http request for store list");

			try {
				UnprocessedJSONString = await ServiceManager.DataService.GetValues (ServiceManager.AccessToken, "Account/UserInfo");

				CreateUserInfo();
			}
			catch (Exception e) {
				return false;
			}

			return true;
		}

		public static void CreateUserInfo()
		{
			CurrentUserInfo = JsonConvert.DeserializeObject<AccountItem> (UnprocessedJSONString);
			CurrentUserInfo.CreateImageFromBase64 ();
			CurrentUserInfo.Welcome = String.Format("Welcome {0}", CurrentUserInfo.FirstName);

			System.Diagnostics.Debug.WriteLine ("User Info created " + CurrentUserInfo.FirstName);
		}
	}
}

