﻿using System;
using System.Threading.Tasks;

using bepoz_reports.Controls;

namespace bepoz_reports.Manager
{
	public static class TaskManager
	{
		public static bool TaskComplete;
		public static bool StoresVenuesLoaded;

		public static Task<bool> HTTPRequestTask;

		public static int ReportType;

		static TaskManager ()
		{
			StoresVenuesLoaded = false;
		}
	}
}

