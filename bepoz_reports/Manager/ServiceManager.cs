﻿using System;

using bepoz_reports.Services;

namespace bepoz_reports.Manager
{
	public static class ServiceManager
	{
		public static string AccessToken = "";
		public static bool IsLoggedIn;

		public static IStartUpService StartUpService;
		public static ILoginService LoginService;
		public static IDataService DataService;
		public static IDataPeriodService DataPeriodService;

		static ServiceManager ()
		{
			IsLoggedIn = false;
		}
	}
}

