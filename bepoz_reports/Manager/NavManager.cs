﻿using System;

using bepoz_reports.Pages;
using bepoz_reports.ViewModels;
using bepoz_reports.Controls;

namespace bepoz_reports.Manager
{
	public static class NavManager
	{
		public static RootPage RootPage;
		public static RootViewModel RootViewModel;

		public static MenuPage MenuPage;

		public static EventHandler VisibleChanged;
		public static EventHandler LoginChanged;
		public static EventHandler OnDashboardChanged;

		public static IPickerModelChanger PickerChanger;

		public static bool DateChanged;
		public static bool DisplayReport;

		private static bool onDashboardPage;
		public static bool OnDashboardPage
		{
			get
			{
				return onDashboardPage;
			}
			set
			{
				onDashboardPage = value;
				NotifyDashboardChanged();
			}
		}


		private static bool visible;
		public static bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
				OnVisibleChanged();
			}
		}

		private static bool loggedIn;
		public static bool LoggedIn
		{
			get
			{
				return loggedIn;
			}
			set
			{
				loggedIn = value;
				OnLoginChanged();
			}
		}
			
		public static string CurrentTitle;

		public static int Type;

		private static void OnVisibleChanged()
		{
			if (MenuPage != null) {
				MenuPage.SetLoginStatusOnMenu (Visible);
				MenuPage.ViewModel.Visible = Visible;
			}

			if (VisibleChanged != null)
				VisibleChanged(null, EventArgs.Empty);
		}

		private static void OnLoginChanged()
		{
			if (LoginChanged != null)
				LoginChanged(null, EventArgs.Empty);
		}

		private static void NotifyDashboardChanged()
		{
			if (OnDashboardChanged != null)
				OnDashboardChanged(null, EventArgs.Empty);
		}

		static NavManager ()
		{
			Visible = false;
			DateChanged = false;
		}
	}
}

