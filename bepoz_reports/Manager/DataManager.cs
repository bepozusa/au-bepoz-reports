﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Newtonsoft.Json;

using bepoz_reports;
using bepoz_reports.Models;
using bepoz_reports.Colours;
using bepoz_reports.WebServices;
using bepoz_reports.Manager;

namespace bepoz_reports.Manager
{
	public abstract class DataManager<T> where T : ListItem
	{
		public ObservableCollection<GroupItem> ListData;
		public IList<T> Items;

		public PeriodItem CurrentPeriod;
		public T CurrentItem;

		public string[] jsonstrings;
		public string UnprocessedJSONString = "";
		public string jString;

		public SummaryGridModel GridModel;

		public bool CreateListData;

		public DataManager()
		{
			Items = new List<T> ();

			System.Diagnostics.Debug.WriteLine ("Initiating Banking Manager.");
		}

		public async Task<bool> CreateDataRequest(string dataType)
		{
			ListData = new ObservableCollection<GroupItem> ();

			System.Diagnostics.Debug.WriteLine ("Start Http request for till data");

			try {
				UnprocessedJSONString = await ServiceManager.DataPeriodService.GetValues (ServiceManager.AccessToken, dataType);
				UnprocessedJSONString = UnprocessedJSONString.Replace ("null", "0.0");

				CreateList ();
			}
			catch (Exception e) {
				return false;
			}

			return true;
		}

		public void CreateList()
		{
			Items = new List<T> ();

			if (UnprocessedJSONString.Length < 1)
				return;

			if (UnprocessedJSONString.Contains ("\"Results\":[]")) {
				System.Diagnostics.Debug.WriteLine ("No results.");
				return;
			}
				
			string[] startParts = UnprocessedJSONString.Split (new string[] { "[" }, StringSplitOptions.None);

			// brake response string down into seperate json strings
			jsonstrings = startParts [startParts.Length - 1].Split (new string[] { "},{" }, StringSplitOptions.None);

			if (jsonstrings.Length < 1)
				return;

			System.Diagnostics.Debug.WriteLine ("Process " + jsonstrings.Length + " JSON strings.");

			// for first json strng add closing brack
			jsonstrings [0] = jsonstrings [0] + "}";

			// add in cosing bracks around each json string
			for (int i = 1; i < jsonstrings.Length; i++)
				jsonstrings [i] = "{" + jsonstrings [i] + "}";

			// process end of response string as json string
			string[] lastJSONStrings = jsonstrings [jsonstrings.Length - 1].Split (']');
			jsonstrings [jsonstrings.Length - 1] = lastJSONStrings [0];

			// for all json strings create unique tills with total cash amounts
			for (int i = 0; i < (jsonstrings.Length - 1); i++) {
				T currentItem = JsonConvert.DeserializeObject<T> (jsonstrings [i]);	

				if (!checkItemExistsInList (currentItem)) 
				{
					if (passStoreCriteria (currentItem)) 
					{
						currentItem.SetContentItems ();
						Items.Add (currentItem);
					}
				}
			}

			System.Diagnostics.Debug.WriteLine ("Total items " + Items.Count);

			if (CreateListData)
				createListData (ListData);
		}

		public virtual bool checkItemExistsInList(T currentItem)
		{
			// for all items
			foreach (T item in Items) 
			{
				bool doesExist = false;

				switch (ReportManager.CurrentType) {
					case 0:
						doesExist = item.Till.Equals (currentItem.Till);
						break;
					case 1:
						doesExist = item.Venue.Equals (currentItem.Venue);
						break;
					case 2:
						doesExist = item.Store.Equals (currentItem.Store);
						break;				
				}

				// if item already exists add values to existing item based on specific payment types
				if (doesExist) 
				{	
					AddToExisting (item, currentItem);
					return true;
				}
			}

			return false;
		}

		public abstract void AddToExisting (T listItem, T currentItem);
		public abstract void InitGridModel ();

		private bool passStoreCriteria(T currentItem)
		{
			// check pass for venue filters
			if (VenueManager.CurrentVenueGroup == -1 || currentItem.VenueGroup == VenueManager.CurrentVenueGroup)
				return true;

			if (VenueManager.CurrentVenue == -1 || currentItem.VenueID == VenueManager.CurrentVenue)
				return true;

			// check pass for store filters
			if (StoreManager.CurrentStoreGroup == -1 || currentItem.StoreGroup == StoreManager.CurrentStoreGroup)
				return true;

			if (StoreManager.CurrentStore == -1 || currentItem.StoreID == StoreManager.CurrentStore)
				return true;
				
			return false;
		}

		public void ReSort()
		{
			System.Diagnostics.Debug.WriteLine ("Sorting list by type.");

			if (Items != null)
				Items.Clear ();

			CreateList ();
		}

		public void ResetListData()
		{
			ListData = new ObservableCollection<GroupItem> ();
			Items = new List<T> ();
		}

		public void createListData(IList<GroupItem> Data)
		{
			System.Diagnostics.Debug.WriteLine ("Creating list data for items");

			GroupItem newGroup = new GroupItem () { HeaderTitle = ReportManager.CurrentTypeStr + "s" };

			newGroup.TextColor = Colour.header_text_colour.ToFormsColor();
			newGroup.BackgroundColor = Colour.header_background_colour.ToFormsColor();

			foreach (T item in Items) 
				newGroup.Add (item);

			Data.Add (newGroup);
		}
	}
}

