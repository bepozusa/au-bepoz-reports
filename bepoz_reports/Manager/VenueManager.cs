﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Newtonsoft.Json;

using bepoz_reports.WebServices;
using bepoz_reports.Models;

namespace bepoz_reports.Manager
{
	public static class VenueManager
	{
		public static IList<DTOGroupItem> Venues;

		public static int CurrentVenueGroup;
		public static int CurrentVenue;

		public static string[] jsonstrings;
		public static string UnprocessedJSONString;

		static VenueManager ()
		{
			ResetCurrentFilter ();

			Venues = new List<DTOGroupItem> ();
		}

		public static void ResetCurrentFilter()
		{
			CurrentVenueGroup = -1;
			CurrentVenue = -1;
		}

		public static async Task<bool> CreateVenueDataRequest()
		{
			System.Diagnostics.Debug.WriteLine ("Start Http request for store list");

			try {
				UnprocessedJSONString = await ServiceManager.DataService.GetValues (ServiceManager.AccessToken, "Venues");

				CreateVenueList ();
			}
			catch (Exception e) {
				return false;
			}

			return true;
		}
			
		public static void CreateVenueList()
		{
			// remove first and last characters
			string processJSON = UnprocessedJSONString.Remove(UnprocessedJSONString.Length - 1);
			processJSON = processJSON.Substring(1);

			// brake response string down into seperate json strings
			jsonstrings = processJSON.Split (new string[] { "]},{" }, StringSplitOptions.None);

			if (jsonstrings.Length < 1)
				return;

			System.Diagnostics.Debug.WriteLine ("Process " + jsonstrings.Length + " JSON strings.");

			if (jsonstrings.Length > 1) {
				jsonstrings [0] = jsonstrings [0] + "]}";

				// add in cosing bracks around each json string
				for (int i = 1; i < jsonstrings.Length - 1; i++) {
					jsonstrings [i] = "{" + jsonstrings [i] + "]}";
				}

				jsonstrings [jsonstrings.Length - 1] = "{" + jsonstrings [jsonstrings.Length - 1];
			}

			try {
				// for all json strings create unique stores list
				for (int i = 0; i < jsonstrings.Length; i++) {
					DTOGroupItem currentVenue = JsonConvert.DeserializeObject<DTOGroupItem> (jsonstrings [i]);	

					Venues.Add (currentVenue);
				}
			}
			catch (Exception e) {
				System.Diagnostics.Debug.WriteLine ("Error with deserializing " + e);
			}

	
			System.Diagnostics.Debug.WriteLine ("Total venue groups " + Venues.Count);
		}
	}
}

