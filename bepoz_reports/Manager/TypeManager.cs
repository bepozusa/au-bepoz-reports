﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Newtonsoft.Json;

using bepoz_reports.WebServices;
using bepoz_reports.Models;

namespace bepoz_reports.Manager
{
	public static class TypeManager
	{
		public static int CurrentType;

		static TypeManager ()
		{
		}

		public static void ResetCurrentFilter()
		{
			CurrentType = -1;
		}
	}
}

