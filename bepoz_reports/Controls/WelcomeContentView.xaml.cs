﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Controls
{
	public partial class WelcomeContentView : ContentView
	{
		private bool animating = false, showingComboBoxView = false;

		public WelcomeContentView ()
		{
			InitializeComponent ();
		}

		public void DoneClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Done pressed on combo box view.");
		
			if (animating)
				return;

			ReportManager.SetDateText ();
			time_button.Text = ReportManager.CurrentDate;

			animateComboBoxView(combo_layout.Bounds.Height * -1, 0);
		}

		public void TimeClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Time pressed on combo box view.");

			if (animating)
				return;

			animateComboBoxView(combo_layout.Bounds.Height, 0.3f);

			showingComboBoxView = !showingComboBoxView;
		}

		private void animateComboBoxView(double movement, float opacity)
		{
			animating = true;
			black_overlay_view.FadeTo (opacity, 50);
			combo_layout.LayoutTo(new Rectangle (combo_layout.Bounds.X, combo_layout.Bounds.Y - movement, combo_layout.Bounds.Width, combo_layout.Bounds.Height), 150, null);
			animating = false;
		}
	}
}

