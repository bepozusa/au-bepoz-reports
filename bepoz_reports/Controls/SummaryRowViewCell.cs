﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public class SummaryRowViewCell : ViewCell
	{
		public SummaryRowViewCell()
		{
			this.View = new SummaryRowView ();
			Height = 60;
		}
	}
}