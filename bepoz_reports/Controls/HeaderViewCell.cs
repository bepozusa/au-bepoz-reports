﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public class HeaderViewCell : ViewCell
	{
		public HeaderViewCell()
		{
			this.View = new HeaderContentView ();
			Height = 50;
		}
	}
}

