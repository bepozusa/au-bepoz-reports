﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public class MenuViewCell : ViewCell
	{
		public MenuViewCell()
		{
			this.View = new MenuCellContentView ();
			Height = 50;
		}
	}
}