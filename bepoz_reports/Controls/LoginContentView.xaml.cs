﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Manager;
using bepoz_reports.ViewModels;

namespace bepoz_reports.Controls
{
	public partial class LoginContentView : ContentView
	{
		public LoginContentView ()
		{
			BackgroundColor = Color.White;

			InitializeComponent ();

			switch (DeviceManager.DeviceType) {
			case 0:
				login_button.BackgroundColor = Color.Gray;					
				break;
			case 1:
				break;
			}
		}

		private async void login()
		{
			ErrorLabel.Opacity = 0;
			ErrorLabel.Text = "";

			if (UsernameEntry.Text.Length == 0)
				ErrorLabel.Text = "Please enter username.";

			else if (PasswordEntry.Text.Length == 0)
				ErrorLabel.Text = "Please enter password.";

			// if any issues with entries
			if (ErrorLabel.Text.Length > 0) {
				ErrorLabel.Opacity = 1;
				return;
			}

			try
			{
				System.Diagnostics.Debug.WriteLine("Login successful.");

				login_button.IsEnabled = false;
				ServiceManager.AccessToken = await ServiceManager.LoginService.Login(UsernameEntry.Text, PasswordEntry.Text);
				ServiceManager.IsLoggedIn = true;
				AccountManager.Username = UsernameEntry.Text;

				if (!TaskManager.StoresVenuesLoaded) 
				{
					// user info retrieval http request
					System.Diagnostics.Debug.WriteLine("Starting task to retrieve user info.");

					TaskManager.HTTPRequestTask = AccountManager.CreateUserInfoRequest ();
					await TaskManager.HTTPRequestTask;

					System.Diagnostics.Debug.WriteLine("Finished task to retrieve user info.");

					loadStoresAndVenues ();
				}

				Content = new WelcomeContentView();
				NavManager.RootPage.IsGestureEnabled = true;

				return;
			}
			catch
			{
				System.Diagnostics.Debug.WriteLine("Login failed.");

				ServiceManager.AccessToken = "";
				ServiceManager.IsLoggedIn = false;
				ErrorLabel.Opacity = 1;
				ErrorLabel.Text = "Invalid login details.";
			}
			finally
			{
				login_button.IsEnabled = true;
			}				
		}

		private async void loadStoresAndVenues()
		{
			System.Diagnostics.Debug.WriteLine ("Starting task to venues and stores.");

			// function creates two requests to retrieve venues and stores information
			TaskManager.TaskComplete = false;

			// store retrieval http request
			TaskManager.HTTPRequestTask = StoreManager.CreateStoreDataRequest ();
			await TaskManager.HTTPRequestTask;

			// venue retrieval http request
			TaskManager.HTTPRequestTask = VenueManager.CreateVenueDataRequest ();
			await TaskManager.HTTPRequestTask;

			TaskManager.TaskComplete = true;
			TaskManager.StoresVenuesLoaded = true;

			System.Diagnostics.Debug.WriteLine ("Finished task to retrieve venues and stores.");
		}

		public async void LoginClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Login pressed.");

			login ();
		}

		public async void RememberMeClicked(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine ("Remember me pressed.");

			UsernameEntry.Text = AccountManager.Username;
		}
	}
}

