﻿using System;

using Xamarin.Forms;

namespace bepoz_reports.Controls
{
	public interface IPickerModelChanger
    {
		void ChangePickerModels(int pickerId);
    }
}