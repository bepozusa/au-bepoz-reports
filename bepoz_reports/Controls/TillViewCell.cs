﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public class TillViewCell : ViewCell
	{
		public TillViewCell()
		{
			this.View = new BankingContentView ();
			Height = 100;
		}
	}
}