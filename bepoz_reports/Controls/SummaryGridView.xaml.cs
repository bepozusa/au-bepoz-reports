﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Manager;
using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public partial class SummaryGridView : ContentView
	{
		private int rowIndex = 2;
		private int columnIndex = 0;

		public SummaryGridView ()
		{
			InitializeComponent ();

			switch (NavManager.Type)
			{
				case 0:
					BindingContext = App.BankingManager.GridModel;
					break;
				case 1:
					BindingContext = App.DiscountsManager.GridModel;
					break;
				case 2:
					BindingContext = App.DailyTotalsManager.GridModel;
					break;
			}
				
			item_list.ItemTemplate = new DataTemplate(typeof(SummaryRowViewCell));
		}
	}
}

