﻿using System;

using Xamarin.Forms;

namespace bepoz_reports.Controls
{
    public class SwitchView : View
    {
		public bool switchOn;

		public void NotifySwitchChanged(bool changed)
		{
			System.Diagnostics.Debug.WriteLine("Boolean 'switchOn' changed in SwitchView.");

			switchOn = !switchOn;

			if (SwitchChanged != null)
				SwitchChanged(this, changed);
		}

		public event EventHandler<bool> SwitchChanged;

		public SwitchView()
        {
		}

		public void Hide()
		{
			this.Opacity = 0;
		}

		public void Show()
		{
			this.Opacity = 1;
		}
    }
}
