﻿using System;

using Xamarin.Forms;

namespace bepoz_reports.Controls
{
    public class SwipeView : Frame
    {
		public SwipeView()
        {
        }
			
		public void NotifySwipeLeftOccured(bool swipedLeft)
		{
			System.Diagnostics.Debug.WriteLine ("Frame swipe left occured.");

			if (SwipeLeftOccured != null)
				SwipeLeftOccured(this, swipedLeft);
		}

		public void NotifySwipeRightOccured(bool swipedRight)
		{
			System.Diagnostics.Debug.WriteLine ("Frame swipe right occured.");

			if (SwipeRightOccured != null)
				SwipeRightOccured(this, swipedRight);
		}

		public event EventHandler<bool> SwipeLeftOccured;
		public event EventHandler<bool> SwipeRightOccured;

    }
}