﻿using System;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Controls
{
    public class ComboBoxViewDate : View
    {
		public ComboBoxViewDate()
        {
        }
			
		public void NotifyDateSelected(string period, int interval)
		{
			System.Diagnostics.Debug.WriteLine ("Date selection occured, interval " + period);

			ReportManager.ReportPeriod = period;
			ReportManager.CurrentInterval = interval;
			ReportManager.SetCurrentPeriod ();

			NavManager.DateChanged = true;

			if (DateSelected != null)
				DateSelected(this, period);
		}

		public event EventHandler<string> DateSelected;
    }
}