﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_reports.Date;
using bepoz_reports.Manager;
using bepoz_reports.Mvvm;
using bepoz_reports.ViewModels;

namespace bepoz_reports.Controls
{
	public partial class CustomDateContentView : ContentView
	{
		private bool dateFromSelected = false;

		public bool BothDatesSelected = false;

		private int numberOfFrom = 0;
		private int numberOfTo = 0;

		public CustomDateContentView ()
		{
			InitializeComponent ();

			calendar_view.DateSelected += (object sender, DateTime e) =>
			{
				if (!dateFromSelected) {
					DateToEntry.TextColor = Color.Red;
					DateFromEntry.TextColor = Color.Black;

					dateFromSelected = true;
					numberOfFrom++;
					DateFromEntry.Text = DateConverter.FormatDateFromCalendar(e.ToString("d"));
					ReportManager.CustomDateFrom = DateFromEntry.Text;
				}
				else
				{
					DateToEntry.TextColor = Color.Black;
					DateFromEntry.TextColor = Color.Red;

					dateFromSelected = false;
					numberOfTo++;
					DateToEntry.Text = DateConverter.FormatDateFromCalendar(e.ToString("d"));
					ReportManager.CustomDateTo = DateToEntry.Text;
				}

				if (numberOfFrom > 0 && numberOfTo > 0)
					BothDatesSelected = true;
			};
		}
	}
}