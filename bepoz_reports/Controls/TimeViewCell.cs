﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public class TimeViewCell : ViewCell
	{
		public TimeViewCell()
		{
			this.View = new TimeContentView ();
			Height = 100;
		}
	}
}