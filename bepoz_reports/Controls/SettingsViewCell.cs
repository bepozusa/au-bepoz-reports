﻿using System;

using Xamarin.Forms;

using bepoz_reports.Models;

namespace bepoz_reports.Controls
{
	public class SettingsViewCell : ViewCell
	{
		public SettingsViewCell()
		{
			this.View = new SettingsContentView ();
			Height = 120;
		}
	}
}