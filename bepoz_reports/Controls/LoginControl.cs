﻿using System;

using Xamarin.Forms;

namespace bepoz_reports.Controls
{
    public class LoginControl : View
    {
		public LoginControl()
        {
		}

		public void NotifyLoginChanged(bool accepted)
		{
			if (LoginChanged != null)
				LoginChanged(this, accepted);
		}

		public event EventHandler<bool> LoginChanged;
    }
}
