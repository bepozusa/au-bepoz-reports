﻿using System;

using Xamarin.Forms;

using bepoz_reports.Manager;

namespace bepoz_reports.Controls
{
    public class ComboBoxMixedView : View
    {
		public int Type = 0;

		public ComboBoxMixedView()
        {
        }

		public void NotifyStoreSelected(string name)
		{
			System.Diagnostics.Debug.WriteLine ("Store selection occured, name " + name);

			ReportManager.CurrentStore = name;

			Sort ();

			if (StoreSelected != null)
				StoreSelected(this, name);
		}

		public void NotifyVenueSelected(string name)
		{
			System.Diagnostics.Debug.WriteLine ("Venue selection occured, name " + name);

			ReportManager.CurrentVenue = name;

			Sort ();

			if (VenueSelected != null)
				VenueSelected(this, name);
		}

		public void NotifyTypeSelected(string name, int type)
		{
			System.Diagnostics.Debug.WriteLine ("Type selection occured, name " + name);

			ReportManager.CurrentTypeStr = name;
			ReportManager.CurrentType = type;

			// resort current results
			Sort ();

			if (TypeSelected != null)
				TypeSelected(this, name);
		}

		private void Sort()
		{
			App.BankingManager.ReSort();
		}

		public event EventHandler<string> StoreSelected;
		public event EventHandler<string> VenueSelected;
		public event EventHandler<string> TypeSelected;
    }
}