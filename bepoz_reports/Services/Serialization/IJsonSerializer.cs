﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace bepoz_reports.Services.Serialization
{
	/// <summary>
	/// The JsonSerializer interface.
	/// </summary>
	[SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
	public interface IJsonSerializer : ISerializer
	{
	}
}

