﻿using System;

#if __IOS__
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
#endif

namespace bepoz_reports.Shared.Colours
{
	public struct Colour
    {
		public static readonly Colour navigation_bar_colour_top = 0x0c2d62;
		public static readonly Colour navigation_bar_colour_bottom = 0xcdcdcd;
		public static readonly Colour navigation_bar_button_tint = 0x032c6a;
		public static readonly Colour menu_title_text_colour = 0xFFFFFF;
		public static readonly Colour menu_background_colour = 0xFFFFFF;
		public static readonly Colour menu_heading_text_colour = 0xAAAAAA;
		public static readonly Colour menu_table_view_seperator_colour = 0x444444;
		public static readonly Colour menu_detail_text_label_text_colour = 0xAAAAAA;
		public static readonly Colour default_text_colour = 0x000000;
		public static readonly Colour text_color_pressed_colour = 0xFFFFFF;
		public static readonly Colour header_background_colour = 0xdfd9d1;
		public static readonly Colour light_gray_colour = 0xffb7bab;
		public static readonly Colour library_page_background_colour = 0xFFFFFF;
		public static readonly Colour lesson_page_background_colour = 0xFFFFFF;
		public static readonly Colour menu_page_background_colour = 0xFFFFFF;
		public static readonly Colour lesson_table_background_colour = 0xFFFFFF;
		public static readonly Colour lesson_downloaded_text_colour = 0xf05932;
		public static readonly Colour logo_circle_colour = 0x6cc5c5;

        public double R, G, B;

		public static Colour FromHex(int hex)
        {
            Func<int, int> at = offset => (hex >> offset) & 0xFF;

			return new Colour
            {
                R = at(16) / 255.0,
                G = at(8) / 255.0,
                B = at(0) / 255.0
            };
        }

		public static implicit operator Colour(int hex)
        {
            return FromHex(hex);
        }

	#if __IOS__
		public UIColor ToUIColor ()
		{
			return UIColor.FromRGB ((float)R, (float)G, (float)B);
		}

		public static implicit operator UIColor (Colour colour)
		{
			return colour.ToUIColor ();
		}

		public static implicit operator CGColor (Colour colour)
		{
			return colour.ToUIColor ().CGColor;
		}

	#endif

	#if __ANDROID__

        public global::Android.Graphics.Color ToAndroidColor()
        {
          return global::Android.Graphics.Color.Rgb((int)(255 * R), (int)(255 * G), (int)(255 * B));
        }

		public static implicit operator global::Android.Graphics.Color(Colour colour)
        {
			return colour.ToAndroidColor();
        }

	#endif
    }
}
