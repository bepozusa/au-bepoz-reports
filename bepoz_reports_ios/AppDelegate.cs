﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin.Forms;
using Xamarin;

using bepoz_reports;
using bepoz_reports.Services.DI;
using bepoz_reports.Services.Serialization;
using bepoz_reports_ios;
using bepoz_reports.DeviceProperties;
using bepoz_reports.IOS.DeviceProperties;
using bepoz_reports.XForms;
using bepoz_reports.Services;
using bepoz_reports.Manager;
using bepoz_reports.IOS.Serialization;
using bepoz_reports.Shared.Colours;
using System.Drawing;

namespace bepoz_reports.IOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : XFormsApplicationDelegate
	{
		UIWindow window;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			AccountManager.Username = NSUserDefaults.StandardUserDefaults.StringForKey ("RememberKey");
			AccountManager.RememberUsername = NSUserDefaults.StandardUserDefaults.BoolForKey ("RememberKeyRequired");

			window = new UIWindow (UIScreen.MainScreen.Bounds);

			Forms.Init();
			FormsMaps.Init();

			setIoc ();
			setServices ();
			setPickerModelChanger ();

			DeviceManager.DeviceType = 1;

			UINavigationBar.Appearance.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
			UINavigationBar.Appearance.TintColor = Colour.navigation_bar_colour_top.ToUIColor ();
			UINavigationBar.Appearance.BarTintColor = UIColor.White;
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
				{
					TextColor = UIColor.Black,
					Font = UIFont.FromName("Arial-BoldMT", 16f)
				});
					
			UINavigationBar.Appearance.SetBackgroundImage (new UIImage("navbar.png"), UIBarMetrics.Default);
			System.Diagnostics.Debug.WriteLine ("Last login username: " + AccountManager.Username);

			window.RootViewController = App.GetMainPage().CreateViewController();
			window.MakeKeyAndVisible ();

			// creates startup service to initiate web api on Azure
			sendStartUpService ();

			return true;
		}

		private void setPickerModelChanger()
		{
			NavManager.PickerChanger = new bepoz_reports.iOS.Controls.PickerModelChanger ();
		}

		private void setServices()
		{
			ServiceManager.LoginService = new bepoz_reports.iOS.WebServices.LoginService ();
			ServiceManager.DataService = new bepoz_reports.iOS.WebServices.DataService ();
			ServiceManager.DataPeriodService = new bepoz_reports.iOS.WebServices.DataPeriodService ();
			ServiceManager.StartUpService = new bepoz_reports.iOS.WebServices.StartUpService ();
		}

		private void sendStartUpService()
		{
			ServiceManager.StartUpService.Start ();
		}

		private void setIoc()
		{
			var resolverContainer = new SimpleContainer();

			var app = new XFormsAppiOS();
			app.Init(this);

			resolverContainer.Register<IDevice>(t => AppleDevice.CurrentDevice)
				.Register<IDisplay>(t => t.Resolve<IDevice>().Display)
				.Register<IJsonSerializer, JsonSerializer>()
				.Register<IXFormsApp>(app)
				.Register<IDependencyContainer>(t => resolverContainer);

			Resolver.SetResolver(resolverContainer.GetResolver());
		}
	}
}

