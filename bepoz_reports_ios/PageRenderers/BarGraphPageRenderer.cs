﻿using System;
using System.Drawing;
using System.ComponentModel;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.MediaPlayer;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

// This ExportRenderer command tells Xamarin.Forms to use this renderer
// instead of the built-in one for this page
using bepoz_reports;
using bepoz_reports.Pages;
using bepoz_reports.Report;
using bepoz_reports.IOS.PageRenderers;
using bepoz_reports.IOS.Charts;
using bepoz_reports.Tasks;
using System.Threading.Tasks;
using System.IO;

[assembly:ExportRenderer(typeof(BarGraphPage), typeof(BarGraphPageRenderer))]

namespace bepoz_reports.IOS.PageRenderers
{
	/// <summary>
	/// Render this page using platform-specific UIKit controls
	/// </summary>
	public class BarGraphPageRenderer : PageRenderer
	{
		private LoadingOverlay loadingOverlay;
		private bool loadingOverlayAdded = false;
		private UIWebView webView;

		protected override void OnElementChanged (VisualElementChangedEventArgs visualElementChange)
		{
			base.OnElementChanged (visualElementChange);

			var page = Element as PieGraphPage;

			webView = new UIWebView (View.Bounds);
			webView.BackgroundColor = UIColor.Blue;

			string fileName = "Charts/Barchart/bar.html"; // remember case sensitive
			string localHtmlUrl = Path.Combine(NSBundle.MainBundle.BundlePath, fileName);
			webView.LoadRequest (new NSUrlRequest (new NSUrl (localHtmlUrl, false)));
			webView.ScalesPageToFit = true;
			webView.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;

			View.Add(webView);
			/*loadingOverlayAdded = true;
			loadingOverlay = new LoadingOverlay(View.Bounds);

			View.Add (loadingOverlay);

			displayChart ();*/
		}

		/*public async void displayChart()
		{
			await Task.Delay(5000);

			loadingOverlay.Hide();
			loadingOverlayAdded = false;

			// create a bar chart
			BarChart barChart = new BarChart ();
			barChart.CreateChart (View);
		}*/

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			// reload page so video stops
		}
	}
}

