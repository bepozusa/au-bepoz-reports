﻿using System;
using System.Drawing;
using System.ComponentModel;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.MediaPlayer;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

// This ExportRenderer command tells Xamarin.Forms to use this renderer
// instead of the built-in one for this page
using bepoz_reports;
using bepoz_reports.Pages;
using bepoz_reports.Report;
using bepoz_reports.IOS.PageRenderers;
using bepoz_reports.IOS.Charts;

[assembly:ExportRenderer(typeof(GraphSelectionPage), typeof(GraphSelectionPageRenderer))]

namespace bepoz_reports.IOS.PageRenderers
{
	/// <summary>
	/// Render this page using platform-specific UIKit controls
	/// </summary>
	public class GraphSelectionPageRenderer : PageRenderer
	{
		private LoadingOverlay loadingOverlay;
		private GraphSelectionPage page;

		private bool loadingOverlayAdded = false;

		protected override void OnElementChanged (VisualElementChangedEventArgs visualElementChange)
		{
			base.OnElementChanged (visualElementChange);

			page = Element as GraphSelectionPage;

			/*if (page.ToolbarItems.Count > 0)
				page.ToolbarItems.Clear ();

			if (!LessonManager.CurrentLesson.Downloaded || LessonManager.CurrentLesson.Downloading) {
				downloadButton = new ToolbarItem ("download", "download.png", async () => {
					System.Diagnostics.Debug.WriteLine ("Download pressed.");
					downloadHandler.CreateDownloadHandler (page.CurrentLesson);

					page.ToolbarItems.Remove (downloadButton);
				});

				page.ToolbarItems.Add (downloadButton);
			}

			downloadHandler = new DownloadHandler (downloadButton, page.ToolbarItems);

			// display downloaded mp4 lesson using a MPMoviePlayerController
			if (page.UsingMp4) {
				View.BackgroundColor = UIColor.Black;
				Console.WriteLine ("Loading mp4 lesson " + page.CurrentLesson.Title + ".mp4");

				moviePlayer = new MPMoviePlayerController (NSUrl.FromFilename (App.Library.mp4Path + "/" + page.CurrentLesson.Title + ".mp4"));

				View = moviePlayer.View;
				moviePlayer.SetFullscreen (true, true);
				moviePlayer.Play ();
			} else {
				// display web lesson
				// set base view controller and native view
				var baseView = NativeView;
				var viewController = ViewController;

				url = new NSUrl (page.CurrentLesson.Url); 
				req = new NSMutableUrlRequest (url);

				webView = new UIWebView (View.Bounds);

				//When the web view starts to load
				webView.LoadStarted += (object sender, EventArgs e) => {
					if (loadingOverlayAdded)
						return;

					loadingOverlayAdded = true;
					loadingOverlay = new LoadingOverlay(webView.Bounds);
					webView.Add(loadingOverlay);
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
				};

				//When the web view is finished loading
				webView.LoadFinished += (object sender, EventArgs e) => {
					loadingOverlay.Hide();
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
				};

				//If there is a load error
				webView.LoadError += (object sender, UIWebErrorArgs e) => {
					if (loadingOverlayAdded)
					{
						loadingOverlay.Hide();
						UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
						loadingOverlayAdded = false;
					}
				};

				webView.ScalesPageToFit = true;

				//open the webview with the desired request
				webView.LoadRequest (req);

				View.BackgroundColor = UIColor.Black;
				View = webView;
			}
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			// reload page so video stops

			if (page.UsingMp4) 
			{
				if (moviePlayer != null)
					moviePlayer.Stop ();
			}
			else {
				if (UIScreen.MainScreen.Bounds.Width > 500)
					// check if device is iPad 
					webView.LoadRequest(new NSMutableUrlRequest(new NSUrl("about:blank")));
			}*/
		}
	}
}

