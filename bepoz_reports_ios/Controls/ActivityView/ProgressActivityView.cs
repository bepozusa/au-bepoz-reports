﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.UIKit;

namespace bepoz_reports.Controls
{
	public class ProgressActivityView : UIView
	{
		UIActivityIndicatorView activitySpinner;

		public ProgressActivityView()
		{
			// create the activity spinner, center it horizontall and put it 5 points above center x
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activitySpinner.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;

			Add (activitySpinner);

			activitySpinner.StartAnimating ();
		}
	}
}

