﻿using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.IOS.Controls;
using bepoz_reports.Controls;

[assembly: ExportRenderer(typeof(ActivityView), typeof(ActivityViewRenderer))]
namespace bepoz_reports.IOS.Controls
{
	public class ActivityViewRenderer : ViewRenderer<ActivityView, ProgressActivityView>
	{
		public ActivityViewRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ActivityView> e)
		{
			base.OnElementChanged (e);
								
			var activityView = new ProgressActivityView();

			base.SetNativeControl(activityView);
		}
    }
}