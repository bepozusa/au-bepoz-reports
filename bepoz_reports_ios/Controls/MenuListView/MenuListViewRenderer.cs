using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using MonoTouch.UIKit;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Renderers;

[assembly: ExportRenderer(typeof(MenuListView), typeof(MenuListViewRenderer))]
namespace bepoz_reports.IOS.Renderers
{
	public class MenuListViewRenderer : ListViewRenderer
    {
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);

			var list = Control as UITableView;
			list.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
    }
}