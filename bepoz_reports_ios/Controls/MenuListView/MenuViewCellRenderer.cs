using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using MonoTouch.UIKit;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Renderers;
using bepoz_reports.Shared.Colours;

[assembly: ExportRenderer(typeof(MenuViewCell), typeof(MenuViewCellRenderer))]
namespace bepoz_reports.IOS.Renderers
{
	public class MenuViewCellRenderer : ViewCellRenderer
    {
		public override UITableViewCell GetCell(Cell item, UITableView tableView)
		{
			var cell = base.GetCell (item, tableView);

			if (cell != null) {
				System.Diagnostics.Debug.WriteLine ("NIPPLE");
				cell.SelectedBackgroundView = new UIView {
					BackgroundColor = Colour.bepoz_aqua.ToUIColor ()
				};
			}

			return cell;
		}
    }
}