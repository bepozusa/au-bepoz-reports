﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.UIKit;

namespace bepoz_reports.Controls
{
	public delegate void DateSelected(DateTime date);
	public delegate void MonthChanged(DateTime monthSelected);

	public class CalendarMonthView : UIView
	{
		public int BoxHeight = 30;
		public int BoxWidth = 46;
		int headerHeight = 0;

		public Action<DateTime> OnDateSelected;
		public Action<DateTime> OnFinishedDateSelection;
		public Action<DateTime> MonthChanged;
		public Action SwipedUp;

		public Func<DateTime, bool> IsDayMarkedDelegate;
		public Func<DateTime, bool> IsDateAvailable;

		public DateTime CurrentSelectedDate;
		public DateTime CurrentMonthYear;
		protected DateTime CurrentDate { get; set; }

		private UIScrollView _scrollView;
		private bool calendarIsLoaded, onCurrentMonth;

		private MonthGridView _monthGridView;
		bool _showHeader;

		private float calendarWidth;

		public CalendarMonthView(DateTime selectedDate, bool showHeader, float width = 320) 
		{
			calendarWidth = width;

			_showHeader = showHeader;

			if (_showHeader)
				headerHeight = 20;

			if (_showHeader)
				this.Frame = new RectangleF(0, 0, calendarWidth, 218);
			else 
				this.Frame = new RectangleF(0, 0, calendarWidth, 198);

			BoxWidth = Convert.ToInt32(Math.Ceiling( width / 7 ));

			BackgroundColor = UIColor.White;

			ClipsToBounds = true;

			CurrentSelectedDate = selectedDate;
			CurrentDate = DateTime.Now.Date;
			CurrentMonthYear = new DateTime(CurrentSelectedDate.Year, CurrentSelectedDate.Month, 1);

			var swipeLeft = new UISwipeGestureRecognizer(p_monthViewSwipedLeft);
			swipeLeft.Direction = UISwipeGestureRecognizerDirection.Left;
			this.AddGestureRecognizer(swipeLeft);

			var swipeRight = new UISwipeGestureRecognizer(p_monthViewSwipedRight);
			swipeRight.Direction = UISwipeGestureRecognizerDirection.Right;
			this.AddGestureRecognizer(swipeRight);

			var swipeUp = new UISwipeGestureRecognizer(p_monthViewSwipedUp);
			swipeUp.Direction = UISwipeGestureRecognizerDirection.Up;
			this.AddGestureRecognizer(swipeUp);

			var swipeDown = new UISwipeGestureRecognizer(p_monthViewSwipedDown);
			swipeDown.Direction = UISwipeGestureRecognizerDirection.Down;
			this.AddGestureRecognizer(swipeDown);
		}

		public void SetDate (DateTime newDate)
		{
			bool right = true;

			CurrentSelectedDate = newDate;

			var monthsDiff = (newDate.Month - CurrentMonthYear.Month) + 12 * (newDate.Year - CurrentMonthYear.Year);
			if (monthsDiff != 0)
			{
				if (monthsDiff < 0)
				{
					right = false;
					monthsDiff = -monthsDiff;
				}

				for (int i=0; i<monthsDiff; i++)
				{
					MoveCalendarMonths (right, true, false);
				}
			} 
			else
			{
				RebuildGrid(right, false, false);
			}

		}

		void p_monthViewSwipedUp (UISwipeGestureRecognizer ges)
		{
			MoveCalendarMonths(true, true, true);
		}

		void p_monthViewSwipedDown (UISwipeGestureRecognizer ges)
		{
			MoveCalendarMonths(false, true, true);
		}

		void p_monthViewSwipedRight (UISwipeGestureRecognizer ges)
		{
			MoveCalendarMonths(false, true, false);
		}

		void p_monthViewSwipedLeft (UISwipeGestureRecognizer ges)
		{
			MoveCalendarMonths(true, true, false);
		}

		public override void SetNeedsDisplay ()
		{
			base.SetNeedsDisplay();
			if (_monthGridView!=null)
				_monthGridView.Update();
		}

		public override void LayoutSubviews ()
		{
			if (calendarIsLoaded) return;

			_scrollView = new UIScrollView()
			{
				ContentSize = new SizeF(calendarWidth, 260),
				ScrollEnabled = false,
				Frame = new RectangleF(0, 16 + headerHeight, calendarWidth, this.Frame.Height - 16),
				BackgroundColor = UIColor.White
			};
					
			LoadInitialGrids();

			BackgroundColor = UIColor.Clear;

			AddSubview(_scrollView);

			_scrollView.AddSubview(_monthGridView);

			calendarIsLoaded = true;
		}

		public void DeselectDate(){
			if (_monthGridView!=null)
				_monthGridView.DeselectDayView();
		}

		private void HandlePreviousMonthTouch(object sender, EventArgs e)
		{
			MoveCalendarMonths(false, true, false);
		}
		private void HandleNextMonthTouch(object sender, EventArgs e)
		{
			MoveCalendarMonths(true, true, false);
		}

		public void MoveCalendarMonths (bool right, bool animated, bool byYear)
		{
			if (byYear)
				CurrentMonthYear = CurrentMonthYear.AddYears(right? 1 : -1);
			else
				CurrentMonthYear = CurrentMonthYear.AddMonths(right? 1 : -1);

			if (CurrentMonthYear.Month == DateTime.Now.Month) {
				System.Diagnostics.Debug.WriteLine ("Month on calendar same as now date month.");
				onCurrentMonth = true;
			}
			else
				onCurrentMonth = false;

			RebuildGrid(right, animated, onCurrentMonth);
		}

		public void RebuildGrid(bool right, bool animated, bool oCurrentMonth)
		{
			UserInteractionEnabled = false;

			var gridToMove = CreateNewGrid(CurrentMonthYear, oCurrentMonth);
			var pointsToMove = (right? Frame.Width : -Frame.Width);

			gridToMove.Frame = new RectangleF(new PointF(pointsToMove, 0), gridToMove.Frame.Size);

			_scrollView.AddSubview(gridToMove);

			if (animated){
				UIView.BeginAnimations("changeMonth");
				UIView.SetAnimationDuration(0.4);
				UIView.SetAnimationDelay(0.1);
				UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
			}

			_monthGridView.Center = new PointF(_monthGridView.Center.X - pointsToMove, _monthGridView.Center.Y);
			gridToMove.Center = new PointF(gridToMove.Center.X  - pointsToMove, gridToMove.Center.Y);

			_monthGridView.Alpha = 0;

			SetNeedsDisplay();

			if (animated)
				UIView.CommitAnimations();

			_monthGridView = gridToMove;

			UserInteractionEnabled = true;

			if (MonthChanged != null)
				MonthChanged(CurrentMonthYear);
		}

		private MonthGridView CreateNewGrid(DateTime date, bool oCurrentMonth){
			var grid = new MonthGridView(this, date, oCurrentMonth);
			grid.CurrentDate = CurrentDate;
			grid.BuildGrid();
			grid.Frame = new RectangleF(0, 0, calendarWidth, this.Frame.Height-16);
			return grid;
		}

		private void LoadInitialGrids()
		{
			_monthGridView = CreateNewGrid(CurrentMonthYear, false);
		}

		public override void Draw(RectangleF rect)
		{
			using(var context = UIGraphics.GetCurrentContext())
			{
				context.SetFillColor (UIColor.LightGray.CGColor);
				context.FillRect (new RectangleF (0, 0, calendarWidth, 18 + headerHeight));
			}

			DrawDayLabels(rect);

			if (_showHeader)
				DrawMonthLabel(rect);
		}

		private void DrawMonthLabel(RectangleF rect)
		{
			var r = new RectangleF(new PointF(0, 2), new SizeF {Width = calendarWidth, Height = 42});

			UIColor.DarkGray.SetColor();

			DrawString(CurrentMonthYear.ToString("MMMM yyyy"), 
				r, UIFont.BoldSystemFontOfSize(16),
				UILineBreakMode.WordWrap, UITextAlignment.Center);
		}

		private void DrawDayLabels(RectangleF rect)
		{
			var font = UIFont.BoldSystemFontOfSize(10);
			UIColor.DarkGray.SetColor();
			var context = UIGraphics.GetCurrentContext();
			context.SaveState();

			var i = 0;
			foreach (var d in Enum.GetNames(typeof(DayOfWeek)))
			{
				DrawString(d.Substring(0, 3), new RectangleF(i*BoxWidth, 2 + headerHeight, BoxWidth, 10), font,
					UILineBreakMode.WordWrap, UITextAlignment.Center);
				i++;
			}
			context.RestoreState();
		}
	}

	public class MonthGridView : UIView
	{
		private CalendarMonthView _calendarMonthView;

		public DateTime CurrentDate {get;set;}
		private DateTime _currentMonth;

		protected readonly IList<CalendarDayView> _dayTiles = new List<CalendarDayView>();
		public int Lines { get; set; }
		protected CalendarDayView SelectedDayView {get;set;}
		public int weekdayOfFirst;
		public IList<DateTime> Marks { get; set; }
		private bool onCurrentMonth;

		public MonthGridView(CalendarMonthView calendarMonthView, DateTime month, bool oCurrentMonth)
		{
			_calendarMonthView = calendarMonthView;
			_currentMonth = month.Date;
			onCurrentMonth = oCurrentMonth;

			var tapped = new UITapGestureRecognizer(p_Tapped);
			this.AddGestureRecognizer(tapped);
		}

		void p_Tapped(UITapGestureRecognizer tapRecg)
		{
			var loc = tapRecg.LocationInView(this);

			if (SelectDayView(loc)&& _calendarMonthView.OnDateSelected != null)
				_calendarMonthView.OnDateSelected(new DateTime(_currentMonth.Year, _currentMonth.Month, SelectedDayView.Tag));
		}

		public void Update(){
			foreach (var v in _dayTiles)
				updateDayView(v);

			this.SetNeedsDisplay();
		}

		public void updateDayView(CalendarDayView dayView)
		{
			dayView.Marked = _calendarMonthView.IsDayMarkedDelegate == null ? 
				false : _calendarMonthView.IsDayMarkedDelegate(dayView.Date);

			dayView.Available = _calendarMonthView.IsDateAvailable == null ? 
				true : _calendarMonthView.IsDateAvailable(dayView.Date);
		}

		public void BuildGrid ()
		{
			DateTime previousMonth = _currentMonth.AddMonths (-1);
			var daysInPreviousMonth = DateTime.DaysInMonth (previousMonth.Year, previousMonth.Month);
			var daysInMonth = DateTime.DaysInMonth (_currentMonth.Year, _currentMonth.Month);
			weekdayOfFirst = (int)_currentMonth.DayOfWeek;
			var lead = daysInPreviousMonth - (weekdayOfFirst - 1);

			int boxWidth = _calendarMonthView.BoxWidth;
			int boxHeight = _calendarMonthView.BoxHeight;

			// build last month's days
			for (int i = 1; i <= weekdayOfFirst; i++)
			{
				var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
				var dayView = new CalendarDayView (_calendarMonthView);
				dayView.Frame = new RectangleF ((i - 1) * boxWidth - 1, 0, boxWidth, boxHeight);
				dayView.Date = viewDay;
				dayView.Text = lead.ToString ();

				AddSubview (dayView);
				_dayTiles.Add (dayView);
				lead++;
			}

			var position = weekdayOfFirst + 1;
			var line = 0;

			// current month
			for (int i = 1; i <= daysInMonth; i++)
			{
				var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
				var dayView = new CalendarDayView(_calendarMonthView)
				{
					Frame = new RectangleF((position - 1) * boxWidth - 1, line * boxHeight, boxWidth, boxHeight),
					Today = ((CurrentDate.Date == viewDay.Date) || (onCurrentMonth && (CurrentDate.Date.Day == viewDay.Date.Day))),
					Text = i.ToString(),

					Active = true,
					Tag = i,
					Selected = (viewDay.Date == _calendarMonthView.CurrentSelectedDate.Date)
				};

				dayView.Date = viewDay;
				updateDayView (dayView);

				if (dayView.Selected)
					SelectedDayView = dayView;

				AddSubview (dayView);
				_dayTiles.Add (dayView);

				position++;
				if (position > 7)
				{
					position = 1;
					line++;
				}
			}

			//next month
			int dayCounter = 1;
			if (position != 1)
			{
				for (int i = position; i < 8; i++)
				{
					var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
					var dayView = new CalendarDayView (_calendarMonthView)
					{
						Frame = new RectangleF((i - 1) * boxWidth -1, line * boxHeight, boxWidth, boxHeight),
						Text = dayCounter.ToString(),
					};
					dayView.Date = viewDay;
					updateDayView (dayView);

					AddSubview (dayView);
					_dayTiles.Add (dayView);
					dayCounter++;
				}
			}

			while (line < 6)
			{
				line++;

				for (int i = 1; i < 8; i++)
				{
					var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
					var dayView = new CalendarDayView (_calendarMonthView)
					{
						Frame = new RectangleF((i - 1) * boxWidth -1, line * boxHeight, boxWidth, boxHeight),
						Text = dayCounter.ToString(),
					};
					dayView.Date = viewDay;
					updateDayView (dayView);

					AddSubview (dayView);
					_dayTiles.Add (dayView);
					dayCounter++;
				}
			}

			Frame = new RectangleF(Frame.Location, new SizeF(Frame.Width, (line + 1) * boxHeight));

			Lines = (position == 1 ? line - 1 : line);

			if (SelectedDayView!=null)
				this.BringSubviewToFront(SelectedDayView);
		}

		private bool SelectDayView(PointF p){

			int index = ((int)p.Y / _calendarMonthView.BoxHeight) * 7 + ((int)p.X / _calendarMonthView.BoxWidth);

			if(index < 0 || index >= _dayTiles.Count) 
				return false;

			var newSelectedDayView = _dayTiles[index];
			if (newSelectedDayView == SelectedDayView) 
				return false;

			if (!newSelectedDayView.Active)
			{
				var day = int.Parse(newSelectedDayView.Text);

				if (day > 15)
					_calendarMonthView.MoveCalendarMonths(false, true, false);
				else
					_calendarMonthView.MoveCalendarMonths(true, true, false);

				return false;
			} else if (!newSelectedDayView.Active && !newSelectedDayView.Available){
				return false;
			}

			if (SelectedDayView!=null)
				SelectedDayView.Selected = false;

			this.BringSubviewToFront(newSelectedDayView);
			newSelectedDayView.Selected = true;

			SelectedDayView = newSelectedDayView;
			_calendarMonthView.CurrentSelectedDate =  SelectedDayView.Date;
			SetNeedsDisplay();
			return true;
		}

		public void DeselectDayView(){
			if (SelectedDayView==null) return;
			SelectedDayView.Selected= false;
			SelectedDayView = null;
			SetNeedsDisplay();
		}
	}

	public class CalendarDayView : UIView
	{
		string _text;
		public DateTime Date {get;set;}
		bool _active, _today, _selected, _marked, _available;
		public bool Available {get {return _available; } set {_available = value; SetNeedsDisplay(); }}
		public string Text {get { return _text; } set { _text = value; SetNeedsDisplay(); } }
		public bool Active {get { return _active; } set { _active = value; SetNeedsDisplay();  } }
		public bool Today {get { return _today; } set { _today = value; SetNeedsDisplay(); } }
		public bool Selected {get { return _selected; } set { _selected = value; SetNeedsDisplay(); } }
		public bool Marked {get { return _marked; } set { _marked = value; SetNeedsDisplay(); }  }

		CalendarMonthView _mv;

		public CalendarDayView (CalendarMonthView mv)
		{
			_mv = mv;
			BackgroundColor = UIColor.White;
		}

		public override void Draw(RectangleF rect)
		{
			UIImage img = null;
			UIColor color = UIColor.FromRGBA(0.576f, 0.608f, 0.647f, 0.5f);

			if (!Active || !Available)
			{
				//color = UIColor.FromRGBA(0.576f, 0.608f, 0.647f, 1f);
				//img = UIImage.FromBundle("Images/Calendar/datecell.png");
			} 
			else if (Today && Selected)
			{
				//color = UIColor.White;
				img = UIImage.FromBundle("Calendar/todayselected.png").CreateResizableImage(new UIEdgeInsets(4,4,4,4));
			} 
			else if (Today)
			{
				//color = UIColor.White;
				img = UIImage.FromBundle("Calendar/today.png").CreateResizableImage(new UIEdgeInsets(4,4,4,4));
			} 
			else if (Selected || Marked)
			{
				//color = UIColor.White;
				img = UIImage.FromBundle("Calendar/datecellselected.png").CreateResizableImage(new UIEdgeInsets(4,4,4,4));
			}
			else
			{
				color = UIColor.FromRGBA(0.275f, 0.341f, 0.412f, 1f);
				//img = UIImage.FromBundle("Images/Calendar/datecell.png");
			}

			if (img != null)
				img.Draw(new RectangleF(0, 0, _mv.BoxWidth, _mv.BoxHeight));

			color.SetColor();

			var inflated = new RectangleF(0, 5, Bounds.Width, Bounds.Height);
			DrawString(Text, inflated, UIFont.BoldSystemFontOfSize(16), UILineBreakMode.WordWrap, UITextAlignment.Center);
		}
	}
}

