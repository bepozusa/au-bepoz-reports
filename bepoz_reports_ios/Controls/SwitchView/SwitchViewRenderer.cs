﻿using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.IOS.Controls;
using bepoz_reports.Controls;

[assembly: ExportRenderer(typeof(SwitchView), typeof(SwitchViewRenderer))]
namespace bepoz_reports.IOS.Controls
{
	public class SwitchViewRenderer : ViewRenderer<SwitchView, UISwitchView>
	{
		SwitchView _view;

		public SwitchViewRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SwitchView> e)
		{
			base.OnElementChanged (e);

			_view = Element;

			var switchView = new UISwitchView();

			switchView.OnSwitchChange += (change) =>
			{
				_view.NotifySwitchChanged(change);
			};

			base.SetNativeControl(switchView);
		}
    }
}