﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using bepoz_reports.iOS.Services.Extensions;

namespace bepoz_reports.Controls
{
	public class UISwitchView : UIView
	{
		public Action<bool> OnSwitchChange;
		public bool switchOn = NSUserDefaults.StandardUserDefaults.BoolForKey ("RememberKeyRequired");

		UISwitch rememberSwitch;

		public UISwitchView()
		{
			rememberSwitch = new UISwitch ();
			rememberSwitch.On = switchOn;
			rememberSwitch.TintColor = UIColor.Clear.FromHexString ("#0c2d62", 1.0f);
			rememberSwitch.OnTintColor = UIColor.Clear.FromHexString ("#0c2d62", 1.0f);

			rememberSwitch.TouchUpInside += (object sender, EventArgs e) => 
			{
				System.Diagnostics.Debug.WriteLine("Switch value changed.");

				switchOn = !switchOn;

				NSUserDefaults.StandardUserDefaults.SetBool(switchOn, "RememberKeyRequired");
				NSUserDefaults.StandardUserDefaults.Synchronize ();

				if (OnSwitchChange != null)
					OnSwitchChange(switchOn);
			};

			Add (rememberSwitch);
		}
	}
}

