﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.UIKit;

using bepoz_reports.Models;
using bepoz_reports.Manager;
using bepoz_reports.Controls;

namespace bepoz_reports.iOS.Controls.ComboBox
{
	public class ComboMixedView : UIView
	{
		private List<CustomPickerObject> stores;
		private List<CustomPickerObject> venues;
		private List<CustomPickerObject> types;

		public CustomPickerModel PickerModelStores;
		public CustomPickerModel PickerModelVenues;
		public CustomPickerModel PickerModelTypes;

		public Action<string> PickerChangedStores;
		public Action<string> PickerChangedVenues;
		public Action<string, int> PickerChangedTypes;

		private UIPickerView pickerView;

		private int currentPickerID = 0;

		public ComboMixedView (int initPickerId)
		{
			stores = new List<CustomPickerObject> ();
			venues = new List<CustomPickerObject> ();
			types = new List<CustomPickerObject> ();

			BackgroundColor = UIColor.Clear;
		
			// create venue picker items
			foreach (DTOGroupItem venueItem in VenueManager.Venues) 
			{
				// Venue Group
				venues.Add (new VenuePickerObject(){Title = venueItem.Name, Type = 0, VenueGroup = venueItem.GroupID, ImageName = "group.png"});

				// Venues
				foreach (DTOItem groupItemDTO in venueItem.Children)
					venues.Add (new VenuePickerObject(){Title = groupItemDTO.Name, Type = 1, VenueGroup = groupItemDTO.GroupID, Venue = groupItemDTO.ID, ImageName = "item.png"});
			}
				
			System.Diagnostics.Debug.WriteLine ("Created venues picker model with " + venues.Count + " objects");

			createStoreList ();
			createTypesList ();

			UIView controlView = new UIView (){TranslatesAutoresizingMaskIntoConstraints = false};

			PickerModelVenues = new CustomPickerModel (venues);

			PickerModelVenues.PickerChanged += (s, e) => {
				if (PickerChangedVenues != null)
					PickerChangedVenues(venues[PickerModelVenues.SelectedIndex].Title);
					
				createStoreList();
			};
				
			pickerView = new UIPickerView () 
			{
				ShowSelectionIndicator = true,
				TranslatesAutoresizingMaskIntoConstraints = false,
			};

			ChangePickerModels(initPickerId);

			// add elements to main view
			Add (controlView);

			// add pickers to segmentControlView
			controlView.Add (pickerView);

			// horizontal layout constraints
			controlView.AddConstraints (NSLayoutConstraint.FromVisualFormat ("H:|[pickerView]|", NSLayoutFormatOptions.DirectionLeftToRight, null, new MonoTouch.Foundation.NSDictionary ("pickerView", pickerView)));
			controlView.AddConstraints (NSLayoutConstraint.FromVisualFormat ("V:|[pickerView]|", NSLayoutFormatOptions.DirectionLeftToRight, null, new MonoTouch.Foundation.NSDictionary ("pickerView", pickerView)));

			// vertical constraints
			AddConstraints (NSLayoutConstraint.FromVisualFormat ("V:|[controlView]|", NSLayoutFormatOptions.DirectionLeftToRight, null, new MonoTouch.Foundation.NSDictionary ("controlView", controlView)));
			AddConstraints (NSLayoutConstraint.FromVisualFormat ("H:|[controlView]|", NSLayoutFormatOptions.AlignAllTop, null, new MonoTouch.Foundation.NSDictionary ("controlView", controlView)));
		}

		public void ChangePickerModels(int pickerID)
		{
			currentPickerID = pickerID;

			string selection = "";

			// show selected picker
			switch (currentPickerID) 
			{
				case 0:
					PickerModelVenues.SelectedIndex = 0;
					pickerView.Model = PickerModelVenues;
					selection = "Venues";
					break;
				case 1:
					PickerModelStores.SelectedIndex = 0;
					pickerView.Model = PickerModelStores;					
					selection = "Stores";
					break;
				case 2:
					PickerModelTypes.SelectedIndex = 0;
					pickerView.Model = PickerModelTypes;					
					selection = "Types";
					break;
			}

			System.Diagnostics.Debug.WriteLine(selection + " picker selected.");
		}

		private void createTypesList()
		{
			int typeIndex = 0;

			foreach (string typeTitle in ReportManager.ReportSettings) 
			{
				// add new date object
				if (typeTitle.Contains("Till"))
					types.Add (new TypePickerObject(){Title = typeTitle, Type = typeIndex, ImageName = "till.png"});
				else
					types.Add (new TypePickerObject(){Title = typeTitle, Type = typeIndex, ImageName = "item.png"});

				typeIndex++;
			}

			System.Diagnostics.Debug.WriteLine ("Created types picker model with " + types.Count + " objects");

			PickerModelTypes = new CustomPickerModel (types);
			PickerModelTypes.SelectedIndex = 0;

			PickerModelTypes.PickerChanged += (s, e) => {
				if (PickerChangedTypes != null)
					PickerChangedTypes (types [PickerModelTypes.SelectedIndex].Title, types [PickerModelTypes.SelectedIndex].Type);
			};
		}

		// create stores from selected venue
		private void createStoreList()
		{
			stores.Clear ();

			// create store picker items
			foreach (DTOGroupItem storeItem in StoreManager.Stores) 
			{
				// Store Group
				stores.Add (new StorePickerObject () {
					Title = storeItem.Name,
					Type = 0,
					StoreGroup = storeItem.GroupID,
					ImageName = "group.png"
				});

				// Stores
				foreach (DTOItem groupItemDTO in storeItem.Children) {
					stores.Add (new StorePickerObject () {
						Title = groupItemDTO.Name,
						Type = 1,
						StoreGroup = groupItemDTO.GroupID,
						Store = groupItemDTO.ID,
						ImageName = "item.png"
					});
				}
			}

			System.Diagnostics.Debug.WriteLine ("Created stores picker model with " + stores.Count + " objects");

			PickerModelStores = new CustomPickerModel (stores);
			PickerModelStores.SelectedIndex = 0;

			PickerModelStores.PickerChanged += (s, e) => {
				if (PickerChangedStores != null)
					PickerChangedStores (stores [PickerModelStores.SelectedIndex].Title);
			};
		}
	}
}

