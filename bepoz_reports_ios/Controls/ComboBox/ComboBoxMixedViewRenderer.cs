﻿using System;
using System.Drawing;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.Controls;
using bepoz_reports.iOS.Controls.ComboBox;

[assembly: ExportRenderer(typeof(ComboBoxMixedView), typeof(ComboBoxMixedViewRenderer))]
namespace bepoz_reports.iOS.Controls.ComboBox
{
	public class ComboBoxMixedViewRenderer : ViewRenderer<ComboBoxMixedView, ComboMixedView>
	{
		ComboBoxMixedView view;

		public ComboBoxMixedViewRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ComboBoxMixedView> e)
		{
			base.OnElementChanged (e);

			view = Element;
					
			var comboView = new ComboMixedView(view.Type);

			PickerModelChanger.ComboView = comboView;

			comboView.PickerChangedVenues += (name) =>
			{
				view.NotifyVenueSelected(name);
			};

			comboView.PickerChangedStores += (name) =>
			{
				view.NotifyStoreSelected(name);
			};

			comboView.PickerChangedTypes += (name, type) =>
			{
				view.NotifyTypeSelected(name, type);
			};
				
			base.SetNativeControl(comboView);
		}
    }
}