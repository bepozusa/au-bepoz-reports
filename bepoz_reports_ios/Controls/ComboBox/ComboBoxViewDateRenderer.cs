﻿using System;
using System.Drawing;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.Controls;
using bepoz_reports.iOS.Controls.ComboBox;

[assembly: ExportRenderer(typeof(ComboBoxViewDate), typeof(ComboBoxViewDateRenderer))]
namespace bepoz_reports.iOS.Controls.ComboBox
{
	public class ComboBoxViewDateRenderer : ViewRenderer<ComboBoxViewDate, ComboViewDate>
	{
		ComboBoxViewDate view;

		public ComboBoxViewDateRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ComboBoxViewDate> e)
		{
			base.OnElementChanged (e);

			view = Element;
					
			var comboView = new ComboViewDate();

			comboView.PickerChangedDate += (name, interval) =>
			{
				view.NotifyDateSelected(name, interval);
			};
				
			base.SetNativeControl(comboView);
		}
    }
}