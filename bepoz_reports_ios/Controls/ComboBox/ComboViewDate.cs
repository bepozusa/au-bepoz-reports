﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.UIKit;

using bepoz_reports.Models;
using bepoz_reports.Manager;
using bepoz_reports.Controls;

namespace bepoz_reports.iOS.Controls.ComboBox
{
	public class ComboViewDate : UIView
	{
		private List<DatePickerObject> dates;

		public DatePickerModel PickerModelDates;

		public Action<string, int> PickerChangedDate;

		private UIPickerView pickerView;

		public ComboViewDate ()
		{
			dates = new List<DatePickerObject> ();

			BackgroundColor = UIColor.Clear;
		
			int dateIndex = 0;

			// create venue picker items
			foreach (string periodTitle in ReportManager.PeriodTitles) 
			{
				// add new date object
				dates.Add (new DatePickerObject(){Title = periodTitle, Interval = ReportManager.Intervals[dateIndex]});
				dateIndex++;
			}
				
			System.Diagnostics.Debug.WriteLine ("Created dates picker model with " + dates.Count + " objects");

			UIView controlView = new UIView (){TranslatesAutoresizingMaskIntoConstraints = false};
				
			pickerView = new UIPickerView () 
			{
				ShowSelectionIndicator = true,
				TranslatesAutoresizingMaskIntoConstraints = false,
			};
				
			createPickerModel();

			// if we are on the dashboard page 'custom' option should not be available
			NavManager.OnDashboardChanged += (s, e) => 
			{
				System.Diagnostics.Debug.WriteLine("On dashboard changed occured.");

				if (NavManager.OnDashboardPage)
					dates.RemoveAt(dates.Count - 1);
				else {
					if (dates.Count < 9)
						dates.Add(new DatePickerObject(){Title = "Custom", Interval = 0});
				}

				System.Diagnostics.Debug.WriteLine("Dates size is now " + dates.Count);

				// re create picker items on picker view
				createPickerModel();
			};

			// add elements to main view
			Add (controlView);

			// add pickers to segmentControlView
			controlView.Add (pickerView);

			// horizontal layout constraints
			controlView.AddConstraints (NSLayoutConstraint.FromVisualFormat ("H:|[pickerView]|", NSLayoutFormatOptions.DirectionLeftToRight, null, new MonoTouch.Foundation.NSDictionary ("pickerView", pickerView)));
			controlView.AddConstraints (NSLayoutConstraint.FromVisualFormat ("V:|[pickerView]|", NSLayoutFormatOptions.DirectionLeftToRight, null, new MonoTouch.Foundation.NSDictionary ("pickerView", pickerView)));

			// vertical constraints
			AddConstraints (NSLayoutConstraint.FromVisualFormat ("V:|[controlView]|", NSLayoutFormatOptions.DirectionLeftToRight, null, new MonoTouch.Foundation.NSDictionary ("controlView", controlView)));
			AddConstraints (NSLayoutConstraint.FromVisualFormat ("H:|[controlView]|", NSLayoutFormatOptions.AlignAllTop, null, new MonoTouch.Foundation.NSDictionary ("controlView", controlView)));
		}

		private void createPickerModel()
		{
			System.Diagnostics.Debug.WriteLine ("Creating picker model.");

			PickerModelDates = new DatePickerModel (dates);
			PickerModelDates.PickerChanged += PickerChangedEvent;
			pickerView.Model = PickerModelDates;
		}

		private void PickerChangedEvent (object sender, EventArgs e)
		{
			if (PickerChangedDate != null)
				PickerChangedDate(dates[PickerModelDates.SelectedIndex].Title, dates[PickerModelDates.SelectedIndex].Interval);
		}
	}
}

