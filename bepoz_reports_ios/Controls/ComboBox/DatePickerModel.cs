using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using bepoz_reports.Manager;
using bepoz_reports.Controls;

namespace bepoz_reports.iOS.Controls.ComboBox 
{
	public class DatePickerModel : UIPickerViewModel 
	{
		public List<CustomPickerView> views;
		public List<DatePickerObject> objects;

		public int SelectedIndex = 0;

		public event EventHandler<PickerChangedEventArgs> PickerChanged;

		public DatePickerModel (List<DatePickerObject> pickerObjects) : base () 
		{
			objects = pickerObjects;
			views = new List <CustomPickerView> ();

			foreach (DatePickerObject pickerObject in pickerObjects) 
			{
				addView (pickerObject.Title, "calendar.png");
			}
		}

		private void addView(string title, string icon)
		{
			var empty = RectangleF.Empty;

			views.Add (new CustomPickerView (empty) { Title = title, Image = UIImage.FromFile (icon) });
		}

		public override float GetComponentWidth (UIPickerView pickerView, int component) {
			return CustomPickerView.Width;
		}
		
		public override float GetRowHeight (UIPickerView pickerView, int component) {
			return CustomPickerView.Height;
		}
		
		public override int GetRowsInComponent (UIPickerView pickerView, int component) {
			return views.Count;
		}
		
		public override int GetComponentCount (UIPickerView pickerView) {
			return 1;
		}

		public override void Selected (UIPickerView picker, int row, int component)
		{
			if (views.Count == 0)
				return;

			System.Diagnostics.Debug.WriteLine ("Picker value changed to " + views[row].Title);

			SelectedIndex = row;

			if (this.PickerChanged != null)
			{
				this.PickerChanged(this, new PickerChangedEventArgs{SelectedValue = views[row]});
			}
		}

		public override UIView GetView (UIPickerView pickerView, int row, int component, UIView view) 
		{
			view = new CustomPickerView(new RectangleF(0, 0, pickerView.Frame.Width, 44)); 

			UIGraphics.BeginImageContextWithOptions (view.Frame.Size, false, 0);
			views[row].Layer.RenderInContext(UIGraphics.GetCurrentContext());
			UIImage image = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			return new UIImageView (image);
		}
	}
}