﻿using System;
using System.Drawing;

using MonoTouch.UIKit;

namespace bepoz_reports.iOS.Controls.ComboBox
{
	public class CustomPickerView : UIView
	{
		const float MAIN_FONT_SIZE = 18.0f;
		const float MIN_MAIN_FONT_SIZE = 16.0f;

		public CustomPickerView (RectangleF frame) : base (new RectangleF (PointF.Empty, new SizeF (Width, Height))) 
		{
			AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			BackgroundColor = UIColor.Clear;
		}

		public override void Draw (RectangleF rect) 
		{
			float yCoord = (Bounds.Size.Height - Image.Size.Height) / 2;
			PointF point = new PointF (10.0f, yCoord);

			Image.Draw (point);

			yCoord = (Bounds.Size.Height - MAIN_FONT_SIZE) / 2;
			point = new PointF (10.0f + Image.Size.Width + 10.0f, yCoord);

			DrawString (Title, point, UIFont.SystemFontOfSize (MAIN_FONT_SIZE) );
		}

		public string Title { get; set; }
		public UIImage Image { get; set; }

		public const float Width = 200f;
		public const float Height = 44f;
	}
}

