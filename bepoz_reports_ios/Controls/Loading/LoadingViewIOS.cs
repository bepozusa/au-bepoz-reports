﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.UIKit;

using bepoz_reports.IOS;

namespace bepoz_reports.Controls
{
	public class LoadingViewIOS : UIView
	{
		public LoadingViewIOS()
		{
			BackgroundColor = UIColor.Black;

			System.Diagnostics.Debug.WriteLine ("Michael " + UIScreen.MainScreen.Bounds.Width );

			LoadingOverlay loadingOverlay = new LoadingOverlay(new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width / 2, UIScreen.MainScreen.Bounds.Height / 2));

			AddSubview(loadingOverlay);
		}
	}
}

