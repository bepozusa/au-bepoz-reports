﻿using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.IOS.Controls;
using bepoz_reports.Controls;

[assembly: ExportRenderer(typeof(LoadingView), typeof(LoadingViewRenderer))]
namespace bepoz_reports.IOS.Controls
{
	public class LoadingViewRenderer : ViewRenderer<LoadingView, LoadingViewIOS>
	{
		LoadingView view;

		public LoadingViewRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<LoadingView> e)
		{
			base.OnElementChanged (e);

			view = Element;
					
			var loadingView = new LoadingViewIOS();
				
			base.SetNativeControl(loadingView);
		}
    }
}