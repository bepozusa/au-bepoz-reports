using System;
using System.ComponentModel;
using System.Drawing;

using MonoTouch.UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Controls;

[assembly: ExportRenderer(typeof(LoginButton), typeof(LoginButtonRenderer))]
namespace bepoz_reports.IOS.Controls
{
    using System.Threading.Tasks;

	public partial class LoginButtonRenderer : ButtonRenderer
    {
		UIButton UIRememberButton;
	
		protected override void OnElementChanged (ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

			UIRememberButton = Control as UIButton;

			UIRememberButton.TouchUpInside += (sender, eve) => {
				System.Diagnostics.Debug.WriteLine ("Remember me pressed.");

			};
        }
    }
}