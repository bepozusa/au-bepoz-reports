using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using MonoTouch.UIKit;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Renderers;

[assembly: ExportRenderer(typeof(SummaryGridListView), typeof(SummaryGridListViewRenderer))]
namespace bepoz_reports.IOS.Renderers
{
	public class SummaryGridListViewRenderer : ListViewRenderer
    {
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);

			var list = Control as UITableView;

			//list.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
    }
}