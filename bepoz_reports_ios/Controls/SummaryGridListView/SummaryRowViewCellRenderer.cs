using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using MonoTouch.UIKit;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Renderers;

[assembly: ExportRenderer(typeof(SummaryRowViewCell), typeof(SummaryGridListViewRenderer))]
namespace bepoz_reports.IOS.Renderers
{
	public class SummaryRowViewCellRenderer : ViewCellRenderer
    {
		public override UITableViewCell GetCell(Cell item, UITableView tableView)
		{
			var summaryRowViewCell = (SummaryRowViewCell)item;
			var cell = base.GetCell (item, tableView);

			if (cell != null) {
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}

			return cell;
		}
    }
}