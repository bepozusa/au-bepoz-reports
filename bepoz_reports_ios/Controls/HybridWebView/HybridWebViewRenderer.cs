using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Controls;

[assembly: ExportRenderer(typeof(HybridWebView), typeof(HybridWebViewRenderer))]

namespace bepoz_reports.IOS.Controls
{
    /// <summary>
    /// The hybrid web view renderer.
    /// </summary>
    public partial class HybridWebViewRenderer : ViewRenderer<HybridWebView, UIWebView>
    {
        private UIWebView webView;

		UISwipeGestureRecognizer swipeLeftGestureRecognizer;
		UISwipeGestureRecognizer swipeRightGestureRecognizer;

        /// <summary>
        /// The on element changed callback.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnElementChanged(ElementChangedEventArgs<HybridWebView> e)
        {
            base.OnElementChanged(e);

            if (this.webView == null)
            {
                this.webView = new UIWebView();

				swipeLeftGestureRecognizer = new UISwipeGestureRecognizer (() => {
					e.NewElement.NotifySwipeLeftOccured(true);
				});

				swipeRightGestureRecognizer = new UISwipeGestureRecognizer (() => {
					e.NewElement.NotifySwipeRightOccured(true);
				});

				swipeLeftGestureRecognizer.Direction = UISwipeGestureRecognizerDirection.Left;
				swipeRightGestureRecognizer.Direction = UISwipeGestureRecognizerDirection.Right;

				webView.AddGestureRecognizer (swipeLeftGestureRecognizer);
				webView.AddGestureRecognizer (swipeRightGestureRecognizer);

                this.webView.LoadFinished += LoadFinished;
                this.webView.ShouldStartLoad += this.HandleStartLoad;
                this.InjectNativeFunctionScript();
                this.SetNativeControl(this.webView);
            }

            this.Unbind(e.OldElement);
            this.Bind();
        }

        void LoadFinished(object sender, EventArgs e)
        {
            this.Element.OnLoadFinished(sender, e);
        }

        private bool HandleStartLoad(UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
        {
            return !this.CheckRequest(request.Url.RelativeString);
        }

        partial void Inject(string script)
        {
            this.webView.EvaluateJavascript(script);
        }

        partial void Load(Uri uri)
        {
            if (uri != null)
            {
                this.webView.LoadRequest(new NSUrlRequest(new NSUrl(uri.AbsoluteUri)));
            }
        }

        partial void LoadFromContent(object sender, string contentFullName)
        {
            this.Element.Uri = new Uri(NSBundle.MainBundle.BundlePath + "/" + contentFullName);
            //string homePageUrl = NSBundle.MainBundle.BundlePath + "/" + contentFullName;
            //this.webView.LoadRequest(new NSUrlRequest(new NSUrl(homePageUrl, false)));
        }

        partial void LoadContent(object sender, string contentFullName)
        {
            this.webView.LoadHtmlString(contentFullName, new NSUrl(NSBundle.MainBundle.BundlePath, true));
        }
    }
}