using System;
using System.IO;
using System.Net;
using System.Web;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;
using bepoz_reports.Services;
using bepoz_reports.Manager;
using bepoz_reports.Controls;

using bepoz_reports.iOS.Controls.ComboBox;

namespace bepoz_reports.iOS.Controls
{
	public class PickerModelChanger : IPickerModelChanger
    {
		public static ComboMixedView ComboView;

		public void ChangePickerModels(int pickerId)
		{
			System.Diagnostics.Debug.WriteLine ("Picker Model Changed");

			ComboView.ChangePickerModels (pickerId);
		}
    }
}