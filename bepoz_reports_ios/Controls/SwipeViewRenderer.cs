using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Controls;

[assembly: ExportRenderer(typeof(SwipeView), typeof(SwipeViewRenderer))]

namespace bepoz_reports.IOS.Controls
{
	public class SwipeViewRenderer : ViewRenderer<SwipeView, UIView>
	{
		SwipeView view;

		UISwipeGestureRecognizer swipeLeftGestureRecognizer;
		UISwipeGestureRecognizer swipeRightGestureRecognizer;

		public SwipeViewRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SwipeView> e)
		{
			base.OnElementChanged (e);

			view = Element;

			swipeLeftGestureRecognizer = new UISwipeGestureRecognizer (() => {
				view.NotifySwipeLeftOccured(true);
			});

			swipeRightGestureRecognizer = new UISwipeGestureRecognizer (() => {
				view.NotifySwipeRightOccured(true);
			});

			swipeLeftGestureRecognizer.Direction = UISwipeGestureRecognizerDirection.Left;
			swipeRightGestureRecognizer.Direction = UISwipeGestureRecognizerDirection.Right;

			this.AddGestureRecognizer (swipeLeftGestureRecognizer);
			this.AddGestureRecognizer (swipeRightGestureRecognizer);

		}
	}
}