using System;
using System.ComponentModel;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.Foundation;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Controls;
using bepoz_reports.Manager;

[assembly: ExportRenderer(typeof(UserPreferenceLabel), typeof(UserPreferenceLabelRenderer))]
namespace bepoz_reports.IOS.Controls
{
    using System.Threading.Tasks;

	public partial class UserPreferenceLabelRenderer : LabelRenderer
    {	
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			NSUserDefaults.StandardUserDefaults.SetString(AccountManager.Username, "RememberKey");
			NSUserDefaults.StandardUserDefaults.Synchronize ();

			System.Diagnostics.Debug.WriteLine ("Synchronize IOS StandardUserDefaults.");
		}
    }
}