﻿using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.IOS.Controls;
using bepoz_reports.Controls;

using MonoTouch.UIKit;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace bepoz_reports.IOS.Controls
{
	public class CustomEntryRenderer : EntryRenderer
	{
		public CustomEntryRenderer()
		{
		}

		// Override the OnElementChanged method so we can tweak this renderer post-initial setup
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) 
			{ 
				var textField = (UITextField) Control;

				textField.Font = UIFont.FromName("Segoe UI", 16f);
			}
		}
    }
}