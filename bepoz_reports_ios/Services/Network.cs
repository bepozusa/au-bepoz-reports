using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using bepoz_reports.Services;

[assembly: Xamarin.Forms.Dependency(typeof(bepoz_reports.iOS.Services.Network))]

namespace bepoz_reports.iOS.Services
{
    public class Network : INetwork
    {
        public Task<bool> IsReachable(string host, TimeSpan timeout)
        {
            return Task<bool>.Run(() => Reachability.IsHostReachable(host));
        }
    }
}