﻿using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

namespace bepoz_reports.iOS.Services.Extensions
{
    public static class LabelExtensions
    {
        public static void AdjustHeight(this Label label)
        {
            label.HeightRequest = label.Text.StringHeight(label.Font.ToUIFont(), (float)label.Width);
        }
    }
}

