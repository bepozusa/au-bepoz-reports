﻿using System;
using System.Drawing;
using System.ComponentModel;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.MediaPlayer;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using bepoz_reports.IOS;

[assembly:ExportRenderer(typeof(bepoz_reports.Pages.LoginPage), typeof(LoginPageRenderer))]

namespace bepoz_reports.IOS
{
	/// <summary>
	/// Render this page using platform-specific UIKit controls
	/// </summary>
	public class LoginPageRenderer : PageRenderer
	{
		private bepoz_reports.Pages.LoginPage page;

		protected override void OnElementChanged (VisualElementChangedEventArgs visualElementChange)
		{
			base.OnElementChanged (visualElementChange);

			page = Element as bepoz_reports.Pages.LoginPage;

			UIImage logo = UIImage.FromBundle("navbar.png");
			UIImageView imageView = new UIImageView(new System.Drawing.Rectangle(0, 0, 100, 44));
			imageView.Image = logo;

			NavigationItem.TitleView = imageView;
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			// reload page so video stops

		}
	}
}

