using System;
using System.IO;
using System.Net;
using System.Web;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;
using bepoz_reports.Services;
using bepoz_reports.Manager;

namespace bepoz_reports.iOS.WebServices
{
	public class DataPeriodService : IDataPeriodService
	{
		public async Task<string> GetValues(string accessToken, string dataType)
		{
			System.Diagnostics.Debug.WriteLine ("Retrieving " + dataType + " using IOS service...");

			string requestString = "";
			string venueSearchFilterStr = "";

			bool usingFiler = false;

			if (VenueManager.CurrentVenueGroup != -1) {
				venueSearchFilterStr += String.Format ("?venueGroup={0}&", VenueManager.CurrentVenueGroup);
				usingFiler = true;
			}

			if (VenueManager.CurrentVenue != -1) {
				venueSearchFilterStr += String.Format ("venueID={0}&", VenueManager.CurrentVenue);
				usingFiler = true;
			}	

			if (!usingFiler)
				venueSearchFilterStr = "?";

			if (ReportManager.isCustom)
				requestString = String.Format ("{0}api/" + dataType + venueSearchFilterStr + "&startDate1=" + ReportManager.CurrentPeriod.DateFrom_oData + "&endDate1=" + ReportManager.CurrentPeriod.DateTo_oData, Constants.BaseAddress);
			else
				requestString = String.Format("{0}api/" + dataType + venueSearchFilterStr + "interval1=" + ReportManager.CurrentPeriod.Interval, Constants.BaseAddress);
				
			HttpWebRequest request = new HttpWebRequest(new Uri(requestString));

			if (request is HttpWebRequest)
				((HttpWebRequest) request).AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

			request.Method = "GET";
			request.Accept = "application/json";
			request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken));

			try
			{
				HttpWebResponse httpResponse = (HttpWebResponse)(await request.GetResponseAsync());
				string json;
				using (Stream responseStream = httpResponse.GetResponseStream())
				{
					json = new StreamReader(responseStream).ReadToEnd();
				}

				return json;
			}
			catch (Exception ex)
			{
				throw new SecurityException("Bad credentials", ex);
			}
		}
	}
}