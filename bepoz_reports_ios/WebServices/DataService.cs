using System;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;
using bepoz_reports.Services;
using System.Collections.Generic;

namespace bepoz_reports.iOS.WebServices
{
	public class DataService : IDataService
    {
		public async Task<string> GetValues(string accessToken, string dataType)
		{
			System.Diagnostics.Debug.WriteLine ("Retrieving " + dataType + " using IOS service...");

			HttpWebRequest request = new HttpWebRequest(new Uri(String.Format("{0}api/" + dataType, Constants.BaseAddress)));

			if (request is HttpWebRequest)
				((HttpWebRequest) request).AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

			request.Method = "GET";
			request.Accept = "application/json";
			request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken));

			try
			{
				HttpWebResponse httpResponse = (HttpWebResponse)(await request.GetResponseAsync());
				string json;

				using (Stream responseStream = httpResponse.GetResponseStream())
				{
					json = new StreamReader(responseStream).ReadToEnd();
				}
					
				return json;
			}
			catch (Exception ex)
			{
				throw new SecurityException("Bad credentials", ex);
			}
		}
    }
}