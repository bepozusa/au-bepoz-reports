using System;
using System.Drawing;
using System.Collections.Generic;

using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;

using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

using Infragistics;

using bepoz_reports.Discount;
using bepoz_reports.Models;

namespace bepoz_reports.IOS.Charts
{
    public class SparklineChart : Chart
    {
		public SparklineChart() 
		{
		}

		public override void CreateChart(UIView View)
		{
			List<NSObject> data = this.CreateSimpleData ();

			IGSparklineViewDataSourceHelper dsh = new IGSparklineViewDataSourceHelper(data.ToArray(), "value", "label");
			IGSparklineView sparklineView = new IGSparklineView();

			RectangleF sparklineRect = new RectangleF(0, 0, View.Bounds.Size.Width, View.Bounds.Size.Height);

			sparklineView.Frame = sparklineRect;
			sparklineView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleTopMargin|UIViewAutoresizing.FlexibleBottomMargin;
			sparklineView.BackgroundColor = UIColor.Black;
			sparklineView.MarkerVisibility = false;
			sparklineView.NegativeMarkerVisibility = false;
			sparklineView.FirstMarkerVisibility = false;
			sparklineView.LastMarkerVisibility = false;
			sparklineView.HighMarkerVisibility = false;
			sparklineView.LowMarkerVisibility = false;
			sparklineView.DataSource = dsh;

			View.AddSubview(sparklineView);
		}

		List<NSObject> CreateSimpleData()
		{
			List<NSObject> retValue = new List<NSObject>();

			foreach (DiscountsItem discountItem in DiscountsManager.Discounts) 
			{
				SimpleData data = new SimpleData(discountItem.Amount, string.Format(DiscountsManager.DiscountType + " {0}", discountItem.ID));
				retValue.Add (data);
			}

			return retValue;
		}
	}
}