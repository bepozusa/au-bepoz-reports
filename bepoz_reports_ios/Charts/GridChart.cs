﻿using System;

using MonoTouch.UIKit;

using Infragistics;

using bepoz_reports.IOS.Charts;
using System.Drawing;

namespace bepoz_reports.IOS.Charts
{
	public class GridChart : Chart
	{
		public GridChart() 
		{
		}

		public override void CreateChart(UIView View)
		{
			IGGridView gridView = new IGGridView(new RectangleF(0,0, View.Frame.Size.Width, View.Frame.Size.Height), IGGridViewStyle.IGGridViewStyleDefault);
			gridView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			gridView.WeakDataSource = new DataProvider();
			gridView.UpdateData();

			View.AddSubview(gridView);
		}
	}

	public class DataProvider : IGGridViewDataSource
	{
		public override int NumberOfRowsInSection (IGGridView grid, int sectionIndex)
		{
			return 100;
		}

		public override int NumberOfColumns (IGGridView gridView)
		{
			return 5;
		}

		public override IGGridViewCell CreateCell (IGGridView grid, IGCellPath path)
		{
			IGGridViewCell cell = (IGGridViewCell)grid.DequeueReusableCell("Cell");

			if(cell == null)
				cell = new IGGridViewCell("Cell");

			cell.TextLabel.Text = path.RowIndex + "." + path.SectionIndex + "." + path.ColumnIndex;

			return cell;
		}
	}
}

