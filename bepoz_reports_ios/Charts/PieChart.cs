﻿using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Infragistics;

using Cirrious.MvvmCross.Touch.Views;

using bepoz_reports.Discount;
using bepoz_reports.Models;

namespace bepoz_reports.IOS.Charts
{
	public class PieChart : Chart
	{
		IGPieChartView chart;
		IGPieChartViewDataSourceHelper _source;
		IGLegend _legend;

		List<NSObject> _data;

		public PieChart ()
		{
		}

		public override void CreateChart (UIView View)
		{
			_data = this.CreateSimpleData();

			_source = new IGPieChartViewDataSourceHelper(_data.ToArray(), "value", "label");

			chart = new IGPieChartView();
			chart.Frame = new RectangleF(0, 0, View.Frame.Size.Width, View.Frame.Size.Height);
			chart.AutoresizingMask = UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleHeight;
			chart.DataSource = _source;
			chart.Theme = IGPieChartGradientThemes.IGThemeDark();
			chart.BackgroundColor = UIColor.Clear;

			IGLegend legend = new IGLegend(IGChartLegendType.IGChartLegendTypeItem);

			legend.Layer.BorderColor = UIColor.Gray.CGColor;
			legend.Layer.BorderWidth = 2;
			legend.Frame = new RectangleF(5, 5, View.Frame.Size.Width - 10, 45);
			legend.AutoresizingMask = UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleBottomMargin;
			legend.HorizontalAlignment = IGHorizontalAlign.IGHorizontalAlignStretch;
			legend.VerticalAlignment = IGVerticalAlign.IGVerticalAlignCenter;
			legend.Orientation = IGOrientation.IGOrientationHorizontal;
			legend.Layer.CornerRadius = 8;
			legend.Layer.MasksToBounds = true;

			chart.Legend = legend;

			View.AddSubview(legend);
			View.AddSubview(chart);
		}

		List<NSObject> CreateSimpleData()
		{
			List<NSObject> retValue = new List<NSObject>();

			foreach (DiscountsItem discountItem in DiscountsManager.Discounts) 
			{
				double val = discountItem.Amount;

				if (val < 0) val *= -1;

				SimpleData data = new SimpleData(val, string.Format(DiscountsManager.DiscountType + " {0}", discountItem.ID));
				retValue.Add (data);
			}

			return retValue;
		}
	}
}

