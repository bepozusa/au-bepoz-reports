using System;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;

using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch;
using MonoTouch.CoreGraphics;

using Infragistics;
using bepoz_reports.Models;
using bepoz_reports.Discount;

namespace bepoz_reports.IOS.Charts
{
    public class RangeChart : Chart
    {
		[DllImport (Constants.CoreGraphicsLibrary)]
		static extern void CGRectDivide (RectangleF rect, out RectangleF slice, out RectangleF remainder, float amount, CGRectEdge edge);

		public RangeChart() 
		{
		}

		public override void CreateChart(UIView View)
		{
			View.BackgroundColor = UIColor.White;
			RectangleF chartRect, rangeRect;

			CGRectDivide (View.Bounds, out chartRect, out rangeRect, View.Bounds.Height * 0.85f, CGRectEdge.MinYEdge); 

			List<NSObject> data = new List<NSObject>();

			foreach (DiscountsItem discountItem in DiscountsManager.Discounts) {
				data.Add (new NSNumber (discountItem.Amount));
			}

			IGCategorySeriesDataSourceHelper source = new IGCategorySeriesDataSourceHelper(data.ToArray());

			IGChartView chartView = new IGChartView ();
			chartRect.Inflate (-5, -5);
			chartView.Frame = chartRect;
			chartView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			chartView.ZoomDisplayType = IGChartZoom.IGChartZoomHorizontal;

			chartView.Frame = View.Frame;

			var chartFrame = chartView.Frame;
			chartFrame.Height -= 140;
			chartView.Frame = chartFrame;

			View.Add (chartView);

			IGAreaSeries areaSeries = chartView.AddSeries (new Class ("IGAreaSeries"), "areaSeries", source, "xAxis", "yAxis") as IGAreaSeries;
			areaSeries.XAxis.LabelsVisible = false;
			areaSeries.YAxis.Minimum = 0;
			areaSeries.YAxis.Maximum = 45;

			IGChartView chartViewSelector = new IGChartView();
			chartViewSelector.AutoresizingMask = UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleHeight;

			IGAreaSeries areaSeries1 = chartViewSelector.AddSeries (new Class ("IGAreaSeries"), "areaSeries", source, "xAxis", "yAxis") as IGAreaSeries;
			areaSeries1.XAxis.LabelsVisible = false;
			areaSeries1.YAxis.LabelsVisible = false;

			IGRangeSelectorView rangeSelectorView = new IGRangeSelectorView();
			rangeRect.Inflate (-5, -5);
			rangeSelectorView.Frame = rangeRect;
			rangeSelectorView.AutoresizingMask = UIViewAutoresizing.FlexibleHeight|UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleTopMargin;

			View.Add (rangeSelectorView);
			chartView.Zoombar = rangeSelectorView;

			UIView minValueView = new UIView ();
			minValueView.BackgroundColor = UIColor.DarkGray;

			UIView maxValueView = new UIView ();
			maxValueView.BackgroundColor = UIColor.DarkGray;

			UIView thumbView = new UIView();
			thumbView.BackgroundColor = UIColor.FromWhiteAlpha (0.15f, 0.25f);

			rangeSelectorView.ShadeColor = UIColor.FromWhiteAlpha (0.4f, 0.75f);
			rangeSelectorView.MinimumThumbView = minValueView;
			rangeSelectorView.MaximumThumbView = maxValueView;
			rangeSelectorView.ThumbView = thumbView;
			rangeSelectorView.ContentView = chartViewSelector;
		}
    }
}