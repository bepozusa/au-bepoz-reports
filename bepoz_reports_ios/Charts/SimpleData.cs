﻿using System;

using MonoTouch.Foundation;

namespace bepoz_reports.IOS.Charts
{
	public class SimpleData : NSObject
	{
		[Export("value")]
		public double value { get; set;}
		[Export("label")]
		public string label { get; set; }

		public SimpleData (double data_value, string data_label)
		{
			value = data_value;
			label = data_label;
		}
	}
}

