using System;
using System.Drawing;
using System.Collections.Generic;

using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;

using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

using bepoz_reports.Discount;
using bepoz_reports.Models;
using bepoz_reports.Report;
using bepoz_reports.DailyTotals;

namespace bepoz_reports.IOS.Charts
{
    public class BarChart : Chart
    {
		public BarChart() 
		{
		}

		public override void CreateChart(UIView View)
		{
			List<NSObject> data = new List<NSObject>();

			if (ReportManager.CurrentTitle.Contains ("Venues") || ReportManager.CurrentTitle.Contains ("Stores")) {
				System.Diagnostics.Debug.WriteLine ("Loading chart with discounts.");

				foreach (DiscountsItem discountItem in DiscountsManager.Discounts) {
					data.Add (new NSNumber (discountItem.Amount));
				}
			} else if (ReportManager.CurrentTitle.Contains ("Daily Totals")) {
				System.Diagnostics.Debug.WriteLine ("Loading chart with daily totals.");

				foreach (DailyTotalsItem dailyTotalsItem in DailyTotalsManager.DailyTotals) {
					data.Add (new NSNumber (dailyTotalsItem.NettTotal));
				}
			}

			IGCategoryDateSeriesDataSourceHelper source = new IGCategoryDateSeriesDataSourceHelper();
			source.Values = data.ToArray();

			IGChartView chart = new IGChartView(View.Bounds);
			chart.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
			chart.Theme = IGChartGradientThemes.IGThemeDark();

			IGCategoryXAxis xAxis = new IGCategoryXAxis("xAxis");
			IGNumericYAxis yAxis = new IGNumericYAxis("yAxis");

			chart.AddAxis(xAxis);
			chart.AddAxis(yAxis);

			IGColumnSeries columnSeries = new IGColumnSeries(DiscountsManager.DiscountType);
			columnSeries.XAxis = xAxis;
			columnSeries.YAxis = yAxis;
			columnSeries.DataSource = source;
			chart.AddSeries(columnSeries);

			View.AddSubview(chart);
		}
    }
}