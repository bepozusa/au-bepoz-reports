﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WorkingWithWebview;
using WorkingWithWebview.iOS;
using bepoz_reports;

[assembly: ExportRenderer (typeof (Xamarin.Forms.WebView), typeof (BaseUrlWebViewRenderer))]

namespace WorkingWithWebview.iOS
{
	public class BaseUrlWebViewRenderer : WebViewRenderer 
	{
		public override void LoadHtmlString (string s, NSUrl baseUrl) 
		{
			if (baseUrl == null) {
				baseUrl = new NSUrl (NSBundle.MainBundle.BundlePath, true);
			}
			base.LoadHtmlString (s, baseUrl);
		}
	}
}

