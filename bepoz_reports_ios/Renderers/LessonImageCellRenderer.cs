using System.Drawing;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.UIKit;

using bepoz_reports.Shared;
using bepoz_reports.Shared.Colours;
using bepoz_reports.IOS.Renderers;

[assembly: ExportCell (typeof (ImageCell), typeof (LessonImageCellRenderer))]

namespace bepoz_reports.IOS.Renderers
{
	public class LessonImageCellRenderer : ImageCellRenderer
    {
		public override UITableViewCell GetCell (Cell item, UITableView tableView)
        {
			var cellView = base.GetCell (item, tableView);

			cellView.BackgroundColor = Colour.menu_background_colour.ToUIColor ();
			cellView.TextLabel.Font = UIFont.FromName("Arial", 14);

			cellView.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
			cellView.TextLabel.Lines = 2;

            return cellView;
        }
    }
}
