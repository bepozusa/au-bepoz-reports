using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using bepoz_reports.IOS.Renderers;
using System;

[assembly: Dependency (typeof (BaseUrl_iOS))]

namespace bepoz_reports.IOS.Renderers
{
	public interface IBaseUrlIOS { string Get(); }

	public class BaseUrl_iOS : IBaseUrlIOS {
		public string Get () {
			return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // to load resources in the iOS app itself
		}
	}
}
