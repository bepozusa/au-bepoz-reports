﻿using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using bepoz_reports.IOS.Controls;
using bepoz_reports.Controls;
using MonoTouch.UIKit;

[assembly: ExportRenderer(typeof(CalendarView), typeof(CalendarViewRenderer))]
namespace bepoz_reports.IOS.Controls
{
	public class TabbedPageRenderer : TabbedPageRenderer
	{
		CalendarView view;

		public CalendarViewRenderer()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<CalendarView> e)
		{
			base.OnElementChanged (e);

			view = Element;

			var calendarView = new CalendarMonthView(DateTime.Now, true, UIScreen.MainScreen.Bounds.Width * 0.8f);

			calendarView.OnDateSelected += (date) =>
			{
				view.NotifyDateSelected(date);
			};

			base.SetNativeControl(calendarView);
		}
	}
}