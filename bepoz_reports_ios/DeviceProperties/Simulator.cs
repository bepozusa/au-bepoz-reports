﻿using System;

using MonoTouch.UIKit;

namespace bepoz_reports.IOS.DeviceProperties
{
    /// <summary>
    /// Apple device Simulator.
    /// </summary>
    public class Simulator : AppleDevice
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Xamarin.Forms.Labs.Simulator"/> class.
        /// </summary>
        internal Simulator ()
        {
            var b = UIScreen.MainScreen.Bounds;
            var h = b.Height * UIScreen.MainScreen.Scale;
            var w = b.Width * UIScreen.MainScreen.Scale;
            var dpi = UIScreen.MainScreen.Scale * 163;
            this.Display = new Display ((int)h, (int)w, 44, dpi, dpi);

            this.Name = this.HardwareVersion = "Simulator";
        }
    }
}

