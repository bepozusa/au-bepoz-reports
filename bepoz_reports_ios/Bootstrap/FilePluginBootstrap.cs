using Cirrious.CrossCore.Plugins;

namespace bepoz_reports.IOS.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.File.PluginLoader, Cirrious.MvvmCross.Plugins.File.Touch.Plugin>
    {
    }
}