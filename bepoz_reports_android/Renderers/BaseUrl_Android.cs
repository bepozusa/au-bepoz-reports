using System;
using System.Drawing;

using Xamarin.Forms;

using bepoz_reports_android;
using bepoz_reports_android.Renderers;

[assembly: Dependency (typeof (BaseUrl_Android))]

namespace bepoz_reports_android.Renderers
{
	public interface IBaseUrlAndroid { string Get(); }

	public class BaseUrl_Android : IBaseUrlAndroid {
		public string Get () {
			return Environment.GetFolderPath(Environment.SpecialFolder.Personal); // to load resources in the Android app itself
		}
	}
}