using System;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;
using bepoz_reports.Services;
using System.Collections.Generic;
using bepoz_reports.Report;

namespace bepoz_reports.Droid.WebServices
{
	public class DataPeriodService : IDataPeriodService
    {
		public async Task<string> GetValues(string accessToken, string dataType)
		{
			System.Diagnostics.Debug.WriteLine ("Retrieving " + dataType + " using IOS service...");

			string requestString = "";

			if (ReportManager.isCustom)
				requestString = String.Format ("{0}api/" + dataType + "?startDate1=" + ReportManager.CurrentPeriod.DateFrom_oData + "&endDate1=" + ReportManager.CurrentPeriod.DateTo_oData, Constants.BaseAddress);
			else
				requestString = String.Format("{0}api/" + dataType + "?interval1=" + ReportManager.CurrentPeriod.Interval, Constants.BaseAddress);
				
			HttpWebRequest request = new HttpWebRequest(new Uri(requestString));

			request.Method = "GET";
			request.Accept = "application/json";
			request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken));

			try
			{
				HttpWebResponse httpResponse = (HttpWebResponse)(await request.GetResponseAsync());
				string json;

				using (Stream responseStream = httpResponse.GetResponseStream())
				{
					json = new StreamReader(responseStream).ReadToEnd();
				}
					
				return json;
			}
			catch (Exception ex)
			{
				throw new SecurityException("Bad credentials", ex);
			}
		}
    }
}