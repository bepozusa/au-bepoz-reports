﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using Android.Webkit;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

using bepoz_reports.Pages;
using bepoz_reports_android.Renderers;

// This ExportRenderer command tells Xamarin.Forms to use this renderer
// instead of the built-in one for this page
[assembly:ExportRenderer(typeof(GraphPage), typeof(LessonPage_Android))]

namespace bepoz_reports_android.Renderers
{
	/// <summary>
	/// Render this page using platform-specific Android.Views controls
	/// </summary>
	public class LessonPage_Android : PageRenderer
	{
		Android.Views.View view;
		VideoView video_view;

		private MediaController mediaController;
		private bool loadingOverlayAdded = false;
		public static bool started = false;

		/*protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged(e);

			// So uneasy somehow I tried to weak 
			var activity = Context as Activity;

			LessonViewPage page = Element as LessonViewPage;

			if (!started) {
				started = true;
				
				Intent lessonViewPageActivity = new Intent (activity, typeof(LessonViewPageActivity));
				lessonViewPageActivity.PutExtra ("lessonurl", page.getDownloadUrl());
				lessonViewPageActivity.PutExtra ("lessontitle", page.getLessonTitle());

				activity.StartActivity (lessonViewPageActivity);
			}
		}*/

		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged(e);

			//LessonLibraryPage_Android.started = false;

			GraphPage page = Element as GraphPage;

			// this is a ViewGroup - so should be able to load an AXML file and FindView<>
			var activity = Context as Activity;

			Console.WriteLine ("Loading lesson " + page.CurrentLesson.Title + " with url - " + page.CurrentLesson.DownloadUrl);

			view = activity.LayoutInflater.Inflate(Resource.Layout.lesson_page, this, false);
			view.SetBackgroundColor (Android.Graphics.Color.Black);

			activity.SetContentView (view);

			// display downloaded mp4 lesson using a MPMoviePlayerController
			video_view = view.FindViewById<VideoView> (Resource.Id.video_view); 

			Android.Net.Uri uri = Android.Net.Uri.Parse (page.CurrentLesson.DownloadUrl);

			mediaController = new MediaController (activity);
			mediaController.SetAnchorView (video_view);

			video_view.SetMediaController (mediaController);
			video_view.RequestFocus ();
			video_view.SetVideoURI (uri);

			var progressDialog = ProgressDialog.Show(activity, "Loading Lesson...", page.CurrentLesson.Title, true);

			video_view.Prepared += (sender, args) => {
				video_view.Start ();
				progressDialog.Dismiss ();
			};
				
			/*HTML5WebView html5webView = new HTML5WebView(activity);
			html5webView.LoadUrl(page.lessonUrl);
			activity.SetContentView (html5webView);*/

			/*else {
				//Console.WriteLine ("Loading web lesson " + page.getLessonTitle());

				//view = activity.LayoutInflater.Inflate(Resource.Layout.lesson_view, this, false);

				try {
					HTML5WebView html5webView = new HTML5WebView(activity);
					html5webView.LoadUrl(page.lessonUrl);
					activity.SetContentView (html5webView);
					//AddView (html5webView);
				}
				catch (Exception exc)
				{
					Console.WriteLine("Failed to create a htlm5 webview - " + exc.Message);
				}

				//
			}*/
		}

		/*protected override void OnModelChanged (VisualElement oldModel, VisualElement newModel)
		{
			base.OnModelChanged (oldModel, newModel);
			var page = newModel as LessonViewPage;

			// this is a ViewGroup - so should be able to load an AXML file and FindView<>
			var activity = this.Context as Activity;

			view = activity.LayoutInflater.Inflate(Resource.Layout.lesson_view, this, false);

			HTML5WebView html5webView = new HTML5WebView(activity);

			html5webView.LoadUrl(page.lessonUrl);

			VideoView video_view = view.FindViewById<VideoView> (Resource.Id.video_view); 

			// if a url has been received via intent
			var uri = Android.Net.Uri.Parse (page.lessonUrl);

			MediaController mediaController = new MediaController (activity);
			mediaController.SetAnchorView (video_view);
			video_view.SetMediaController (mediaController);

			video_view.SetVideoURI (uri);
			video_view.Start ();

			//AddView (html5webView);
			//activity.SetContentView (html5webView);
		}*/

		protected override void OnLayout (bool changed, int l, int t, int r, int b)
		{
			base.OnLayout (changed, l, t, r, b);

			var msw = MeasureSpec.MakeMeasureSpec (r - l, MeasureSpecMode.Exactly);
			var msh = MeasureSpec.MakeMeasureSpec (b - t, MeasureSpecMode.Exactly);

			view.Measure(msw, msh);

			view.SetBackgroundColor (Android.Graphics.Color.Black);

			view.Layout (0, 0, r - l, b - t);
		}
	}
}