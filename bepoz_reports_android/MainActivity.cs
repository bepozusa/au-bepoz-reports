﻿using System;
using System.IO;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Views.InputMethods;
using Android.Preferences;
using Android.Content.PM;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_reports.Pages;
using bepoz_reports.DeviceProperties;
using bepoz_reports.XForms;
using bepoz_reports.Services;
using bepoz_reports.Services.DI;
using bepoz_reports.Services.Serialization;
using bepoz_reports.Droid.DeviceProperties;
using bepoz_reports.Droid.Serialization;
using bepoz_reports.Service;
using bepoz_reports.Device;
using bepoz_reports.Account;
using bepoz_reports.Nav;

namespace bepoz_reports.Droid
{
	[Activity(Label = "bepoz_reports", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize, WindowSoftInputMode = SoftInput.StateAlwaysVisible)]
	public class MainActivity : XFormsApplicationDroid
	{
		bool _initialized = false;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			if (!_initialized)
			{
				System.Diagnostics.Debug.WriteLine ("Initialising Activity");

				setIoc ();
				setServices ();
				setPickerModelChanger ();

				DeviceManager.DeviceType = 0;
			}
				
			Xamarin.Forms.Forms.Init(this, bundle);
			Xamarin.FormsMaps.Init(this, bundle);

			var prefs = Application.Context.GetSharedPreferences("bepoz_reports", FileCreationMode.Private);           

			AccountManager.Username = prefs.GetString("RememberKey", "");
			AccountManager.RememberUsername = prefs.GetBoolean("RememberKeyRequired", false);

			/*ActionBar.NavigationMode = ActionBarNavigationMode.Standard;

			ActionBar.SetDisplayShowTitleEnabled (false);
			ActionBar.SetDisplayShowHomeEnabled (false); 
			ActionBar.SetDisplayShowCustomEnabled (true);

			ActionBar.SetCustomView (Resource.Menu.lesson_page_menu);*/

			SetPage (App.GetMainPage());
		}

		private void setPickerModelChanger()
		{
			NavManager.PickerChanger = new bepoz_reports.Droid.Controls.PickerModelChanger ();
		}

		private void setServices()
		{
			ServiceManager.LoginService = new bepoz_reports.Droid.WebServices.LoginService ();
			ServiceManager.DataService = new bepoz_reports.Droid.WebServices.StoreVenueService ();
			ServiceManager.DataPeriodService = new bepoz_reports.Droid.WebServices.DataPeriodService ();
		}

		private void setIoc()
		{
			var resolverContainer = new SimpleContainer();

			var app = new XFormsAppDroid();

			app.Init(this);

			var documents = app.AppDataDirectory;

			resolverContainer.Register<IDevice>(t => AndroidDevice.CurrentDevice)
				.Register<IDisplay>(t => t.Resolve<IDevice>().Display)
				.Register<IJsonSerializer, JsonSerializer>()
				.Register<IDependencyContainer>(resolverContainer)
				.Register<IXFormsApp>(app);

			Resolver.SetResolver(resolverContainer.GetResolver());

			_initialized = true;
		}
	}
}


