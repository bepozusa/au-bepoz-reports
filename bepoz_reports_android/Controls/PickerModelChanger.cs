using System;
using System.IO;
using System.Net;
using System.Web;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;
using bepoz_reports.Services;
using bepoz_reports.Report;
using bepoz_reports.Venue;
using bepoz_reports.Controls;
using bepoz_reports.Droid.Controls.Swipe;

namespace bepoz_reports.Droid.Controls
{
	public class PickerModelChanger : IPickerModelChanger
    {
		public static ComboBoxViewVenueStoreRenderer ComboViewVenueStore;

		public void ChangePickerModels(int pickerId)
		{
			System.Diagnostics.Debug.WriteLine ("Picker Model Changed");

			ComboViewVenueStore.ChangePickerModels (pickerId);
		}
    }
}