﻿using System;

using Android.Views;  

using bepoz_reports.Controls;
using bepoz_reports.Droid.Controls.Calendar;

namespace bepoz_reports.Droid.Controls.Calendar
{
	public class CalendarGestureListener : GestureDetector.SimpleOnGestureListener
	{
		private static int minSwipeDistance = 60;

		public CalendarPickerView CalendarPickerView;

		public override void OnLongPress (MotionEvent e)
		{
			Console.WriteLine ("OnLongPressCalandar");
			base.OnLongPress (e);
		}

		public override bool OnDoubleTap (MotionEvent e)
		{
			Console.WriteLine ("OnDoubleTapCalandar");
			return base.OnDoubleTap (e);
		}

		public override bool OnDoubleTapEvent (MotionEvent e)
		{
			Console.WriteLine ("OnDoubleTapEventCalandar");
			return base.OnDoubleTapEvent (e);
		}

		public override bool OnSingleTapUp (MotionEvent e)
		{
			Console.WriteLine ("OnSingleTapUpCalandar");
			return base.OnSingleTapUp (e);
		}

		public override bool OnDown (MotionEvent e)
		{
			Console.WriteLine ("OnDownCalandar");
			return base.OnDown (e);
		}

		public override bool OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			try {
				if (e1.GetX() - e2.GetX() > minSwipeDistance) {
					CalendarPickerView.MonthViewSwipedRight ();
				} 
				else if (e2.GetX() - e1.GetX() > minSwipeDistance) {
					CalendarPickerView.MonthViewSwipedLeft ();
				}
				else if (e1.GetY() - e2.GetY() > minSwipeDistance) {
					CalendarPickerView.MonthViewSwipedUp ();
				}
				else if (e2.GetY() - e1.GetY() > minSwipeDistance) {
					CalendarPickerView.MonthViewSwipedDown ();
				}
			} catch (Exception e) {

			}

			Console.WriteLine ("OnFlingCalandar");

			return false;//base.OnFling (e1, e2, velocityX, velocityY);
		}

		public override bool OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			/*if ((e1 != null) && (e2 != null)) {
				if (e1.GetY () > e2.GetY ())
					CalendarPickerView.MonthViewSwipedDown ();
				else if (e1.GetY () < e2.GetY ())
					CalendarPickerView.MonthViewSwipedUp ();
			}*/

			Console.WriteLine ("OnScrollCalandar");

			return base.OnScroll (e1, e2, distanceX, distanceY);
		}

		public override void OnShowPress (MotionEvent e)
		{
			Console.WriteLine ("OnShowPressCalandar");
			base.OnShowPress (e);
		}

		public override bool OnSingleTapConfirmed (MotionEvent e)
		{
			Console.WriteLine ("OnSingleTapConfirmedCalandar");
			return base.OnSingleTapConfirmed (e);
		}
	}
}