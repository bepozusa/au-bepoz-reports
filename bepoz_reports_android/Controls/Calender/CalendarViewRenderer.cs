using System;

using Android.Views;
using Android.Widget;
using Android.Renderscripts;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_reports.Droid.Controls.Calendar;
using bepoz_reports.Droid.Controls.Swipe;

[assembly: ExportRenderer(typeof(bepoz_reports.Controls.CalendarView), typeof(bepoz_reports.Droid.Controls.CalendarViewRenderer))]
namespace bepoz_reports.Droid.Controls
{
	public class CalendarViewRenderer : ViewRenderer<bepoz_reports.Controls.CalendarView, LinearLayout>
    {
		private const string TAG = "bepoz_reports.Droid.Controls.Calendar";

		bepoz_reports.Controls.CalendarView _view;
        CalendarPickerView _pickerView;

		private readonly CalendarGestureListener _listener;
		private readonly GestureDetector _detector;

        public CalendarViewRenderer()
        {
			_listener = new CalendarGestureListener ();
			_detector = new GestureDetector (_listener);
        }

		protected override void OnElementChanged(ElementChangedEventArgs<bepoz_reports.Controls.CalendarView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                _view = e.NewElement;

                LayoutInflater inflatorservice = (LayoutInflater)Context.GetSystemService(Android.Content.Context.LayoutInflaterService);
                var containerView = (LinearLayout)inflatorservice.Inflate(Resource.Layout.calendar_picker, null, false);

                _pickerView = containerView.FindViewById<CalendarPickerView>(Resource.Id.calendar_view);
				_listener.CalendarPickerView = _pickerView;
				_pickerView.Init(DateTime.Now, DateTime.Now) .InMode(CalendarPickerView.SelectionMode.Single);

				_pickerView.Touch += HandleTouch;
				_pickerView.GenericMotion += HandleGenericMotion;

				_pickerView.OnDateSelected += (date) =>
				{
					_view.NotifyDateSelected(date);
				};

                SetNativeControl(containerView);
            }
        }

		void HandleTouch (object sender, TouchEventArgs e)
		{
			_detector.OnTouchEvent (e.Event);
		}

		void HandleGenericMotion (object sender, GenericMotionEventArgs e)
		{
			_detector.OnTouchEvent (e.Event);
		}
    }
}