using System;

using Android.Views;
using Android.Widget;
using Android.Renderscripts;
using Android.Content;
using Android.Preferences;
using Android.App;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using bepoz_reports.Account;

[assembly: ExportRenderer(typeof(bepoz_reports.Controls.SwitchView), typeof(bepoz_reports.Droid.Controls.SwitchViewRenderer))]
namespace bepoz_reports.Droid.Controls
{
	public class SwitchViewRenderer : ViewRenderer<bepoz_reports.Controls.SwitchView, LinearLayout>
    {
		bepoz_reports.Controls.SwitchView switchView;

		public SwitchViewRenderer()
        {
        }

		protected override void OnElementChanged(ElementChangedEventArgs<bepoz_reports.Controls.SwitchView> e)
        {
            base.OnElementChanged(e);

			if (e.OldElement == null)
            {
				switchView = e.NewElement;

				var prefs = Application.Context.GetSharedPreferences("bepoz_reports", FileCreationMode.Private);           

                LayoutInflater inflatorservice = (LayoutInflater)Context.GetSystemService(Android.Content.Context.LayoutInflaterService);
                var containerView = (LinearLayout)inflatorservice.Inflate(Resource.Layout.switch_view, null, false);

				Android.Widget.Switch rememberSwitch = containerView.FindViewById<Android.Widget.Switch> (Resource.Id.remember_switch);
				rememberSwitch.Checked = prefs.GetBoolean("RememberKeyRequired", false);

				rememberSwitch.CheckedChange += delegate(object sender, CompoundButton.CheckedChangeEventArgs ev) {
					AccountManager.RememberUsername = rememberSwitch.Checked;

					// store rememberusername in shared preference
					var prefEditor = prefs.Edit();
					prefEditor.PutBoolean("RememberKeyRequired", rememberSwitch.Checked);
					prefEditor.Commit();

					System.Diagnostics.Debug.WriteLine("Switch value changed.");

					switchView.NotifySwitchChanged(rememberSwitch.Checked);
				};

                SetNativeControl(containerView);
            }
        }
    }
}