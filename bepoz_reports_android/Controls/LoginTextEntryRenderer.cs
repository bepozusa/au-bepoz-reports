﻿using System;

using Android.Webkit;
using Android.Widget;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_reports.Controls;
using bepoz_reports.Droid.Controls;
using Android.Views.InputMethods;

[assembly: Xamarin.Forms.ExportRenderer(typeof(LoginTextEntry), typeof(LoginTextEntryRenderer))]
namespace bepoz_reports.Droid.Controls
{
	public partial class LoginTextEntryRenderer : EntryRenderer
    {
		EditText editText;
	
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) {   // perform initial setup
				// lets get a reference to the native control
				var nativeEditText = (global::Android.Widget.EditText) Control;
				// do whatever you want to the textField here!
				nativeEditText.SetBackgroundColor(global::Android.Graphics.Color.LightGray);
				//nativeEditText.EditorAction += HandleEditorAction;
			}
		}

		// Add this method to your class
		private void HandleEditorAction(object sender, EditText.EditorActionEventArgs e)
		{
			e.Handled = false;
			if (e.ActionId == ImeAction.Done)
			{
				System.Diagnostics.Debug.WriteLine ("Handled enter key.");
				e.Handled = true;   
			}
		}
    }
}

