using System;
using System.IO;
using System.Net;
using System.Web;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Android.Widget;
using Android.Views;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Newtonsoft.Json;

using bepoz_reports.Models;
using bepoz_reports.Constant;
using bepoz_reports.Services;
using bepoz_reports.Report;
using bepoz_reports.Venue;
using bepoz_reports.Controls;
using bepoz_reports.Droid.Controls.Swipe;
using Android.Gestures;
using Android.App;

[assembly: ExportRenderer(typeof(bepoz_reports.Controls.SwipeView), typeof(SwipeViewRenderer))]
namespace bepoz_reports.Droid.Controls.Swipe
{
	public class SwipeViewRenderer : ViewRenderer<SwipeView, Android.Views.View>
	{
		SwipeView view;

		private readonly CustomGestureListener _listener;
		private readonly GestureDetector _detector;

		public SwipeViewRenderer()
		{
			_listener = new CustomGestureListener ();
			_detector = new GestureDetector (_listener);
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SwipeView> e)
		{			
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				view = e.NewElement;
				_listener.SwipeView = view;	
			}
				
			this.Touch += HandleTouch;
			this.GenericMotion += HandleGenericMotion;
		}

		void HandleTouch (object sender, TouchEventArgs e)
		{
			_detector.OnTouchEvent (e.Event);
		}

		void HandleGenericMotion (object sender, GenericMotionEventArgs e)
		{
			_detector.OnTouchEvent (e.Event);
		}
	}
}