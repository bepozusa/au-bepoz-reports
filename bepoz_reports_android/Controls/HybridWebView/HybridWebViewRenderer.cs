﻿using System;

using Android.Webkit;
using Android.Views;

using Xamarin.Forms.Platform.Android;

using bepoz_reports.Controls;
using bepoz_reports.Droid.Controls;

[assembly: Xamarin.Forms.ExportRenderer(typeof(HybridWebView), typeof(HybridWebViewRenderer))]
namespace bepoz_reports.Droid.Controls
{
    public partial class HybridWebViewRenderer : ViewRenderer<HybridWebView, Android.Webkit.WebView>
    {
		private readonly HybridWebViewGestureListener _listener;
		private readonly GestureDetector _detector;

		Android.Webkit.WebView webView;

		static HybridWebView hybridWebView;

		static bool sentLoadFinished = false;

		public HybridWebViewRenderer()
		{
			_listener = new HybridWebViewGestureListener ();
			_detector = new GestureDetector (_listener);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<HybridWebView> e)
		{
			base.OnElementChanged (e);

			if (this.Control == null)
			{
				webView = new Android.Webkit.WebView(this.Context);
				webView.Settings.JavaScriptEnabled = true;
				webView.Touch += HandleTouch;
				webView.GenericMotion += HandleGenericMotion;
				webView.SetWebViewClient(new Client(this));
				webView.SetWebChromeClient(new ChromeClient());
				webView.AddJavascriptInterface(new Xamarin(this), "Xamarin");

				_listener.webView = e.NewElement;
				hybridWebView = e.NewElement;

				this.SetNativeControl(webView);
			}

			this.Unbind(e.OldElement);

			this.Bind();
		}

		void HandleTouch (object sender, TouchEventArgs e)
		{
			_detector.OnTouchEvent (e.Event);
		}

		void HandleGenericMotion (object sender, GenericMotionEventArgs e)
		{
			_detector.OnTouchEvent (e.Event);
		}

		partial void Inject(string script)
		{
			this.Control.LoadUrl(string.Format("javascript: {0}", script));
		}

		partial void Load(Uri uri)
		{
			if (uri != null)
			{
				this.Control.LoadUrl(uri.AbsoluteUri);
				this.InjectNativeFunctionScript ();
			}
		}

		partial void LoadFromContent(object sender, string contentFullName)
		{
			this.Element.Uri = new Uri("file:///android_asset/" + contentFullName);
		}

		partial void LoadContent(object sender, string contentFullName)
		{
			this.Control.LoadDataWithBaseURL("file:///android_asset/", contentFullName, "text/html", "UTF-8", null);
		}

		private class Client : WebViewClient
		{
			private readonly WeakReference<HybridWebViewRenderer> webHybrid;

			public Client(HybridWebViewRenderer webHybrid)
			{
				this.webHybrid = new WeakReference<HybridWebViewRenderer>(webHybrid);
			}

			public override bool ShouldOverrideUrlLoading(Android.Webkit.WebView view, string url)
			{
				HybridWebViewRenderer hybrid;

				if (!this.webHybrid.TryGetTarget(out hybrid) || !hybrid.CheckRequest(url))
				{
					return base.ShouldOverrideUrlLoading(view, url);
				}

				return true;
			}

			public override void OnPageStarted (WebView view, string url, Android.Graphics.Bitmap favicon)
			{
				sentLoadFinished = false;
			}

			public override void OnPageFinished (WebView view, string url)
			{
				if (sentLoadFinished)
					return;

				base.OnPageFinished (view, url);

				object sender = null;
				EventArgs e = null;

				hybridWebView.OnLoadFinished(sender, e);

				sentLoadFinished = true;
			}
		}

		public class Xamarin : Java.Lang.Object
		{
			private readonly WeakReference<HybridWebViewRenderer> webHybrid;

			public Xamarin(HybridWebViewRenderer webHybrid)
			{
				this.webHybrid = new WeakReference<HybridWebViewRenderer>(webHybrid);
			}

			[JavascriptInterface]
			[Java.Interop.Export("call")]
			public void Call(string function, string data)
			{
				HybridWebViewRenderer hybrid;

				if (this.webHybrid.TryGetTarget(out hybrid))
				{
					hybrid.TryInvoke(function, data);
				}
			}
		}

		private class ChromeClient : WebChromeClient 
		{
			public override bool OnJsAlert(Android.Webkit.WebView view, string url, string message, JsResult result)
			{
				// the built-in alert is pretty ugly, you could do something different here if you wanted to
				return base.OnJsAlert(view, url, message, result);
			}
		}
    }
}

