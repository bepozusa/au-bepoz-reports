using System;

using Android.Views;
using Android.Widget;
using Android.Renderscripts;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_reports.Droid.Controls.Calendar;
using System.Collections.Generic;
using bepoz_reports.Report;
using bepoz_reports.Controls;

[assembly: ExportRenderer(typeof(bepoz_reports.Controls.ComboBoxViewDate), typeof(bepoz_reports.Droid.Controls.ComboBoxViewDateRenderer))]
namespace bepoz_reports.Droid.Controls
{
	public class ComboBoxViewDateRenderer : ViewRenderer<bepoz_reports.Controls.ComboBoxViewDate, LinearLayout>
    {
		private List<CustomPickerObject> dates = new List<CustomPickerObject>();

		private SpinnerAdapter adapter;
		public Spinner Spinner;

		bepoz_reports.Controls.ComboBoxViewDate _view;
        CalendarPickerView _pickerView;

		public ComboBoxViewDateRenderer()
        {
			createDateList ();
			adapter = new SpinnerAdapter(dates, Context);
        }

		protected override void OnElementChanged(ElementChangedEventArgs<bepoz_reports.Controls.ComboBoxViewDate> e)
        {
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				_view = e.NewElement;

				LayoutInflater inflatorservice = (LayoutInflater)Context.GetSystemService(Android.Content.Context.LayoutInflaterService);
				var containerView = (LinearLayout)inflatorservice.Inflate(Resource.Layout.combox_box_view, null, false);

				Spinner = containerView.FindViewById<Spinner> (Resource.Id.spinner);
				Spinner.Adapter = adapter;

				Spinner.ItemSelected += (object sender, AdapterView.ItemSelectedEventArgs ev) => {
					System.Diagnostics.Debug.WriteLine("Spinner item selected");

					_view.NotifyDateSelected(dates[ev.Position].Title, (dates[ev.Position] as DatePickerObject).Interval);
				};

				SetNativeControl(containerView);
			}
		}

		private void createDateList()
		{
			int dateIndex = 0;

			// create venue picker items
			foreach (string periodTitle in ReportManager.PeriodTitles) 
			{
				// add new date object
				dates.Add (new DatePickerObject(){Title = periodTitle, Interval = ReportManager.Intervals[dateIndex], ImageId = Resource.Drawable.calendar});
				dateIndex++;
			}

			System.Diagnostics.Debug.WriteLine ("Created dates list " + dates.Count + " objects");
		}
	}
}