﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.OS;
using Android.Content;
using Android.Graphics;
using Android.Text;
using Android.Views;
using Android.Widget;

using bepoz_reports.Controls;

namespace bepoz_reports.Droid
{
	public class SpinnerAdapter : BaseAdapter, ISpinnerAdapter 
	{
		public List<CustomPickerObject> Items = new List<CustomPickerObject>();
		Context context;
 
		public SpinnerAdapter (List<CustomPickerObject> items, Context context): base ()
		{
			Items = items;

			this.context = context;
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return position;
		}

		public override int Count
		{
			get { return Items.Count; }
		}

		public override long GetItemId (int position)
		{
			return (long)position;
		}
			
		public Color TextColor
		{
			get;
			set;
		}
			
		public Color Background
		{
			get;
			set;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			LayoutInflater inflater = (LayoutInflater)this.context.GetSystemService(Android.Content.Context.LayoutInflaterService);
			View row = inflater.Inflate(Resource.Layout.custom_spinner_row, parent, false);

			TextView label = row.FindViewById<TextView>(Resource.Id.title_textview);
			ImageView icon = (ImageView)row.FindViewById(Resource.Id.image);

			label.Text = Items[position].Title;
			icon.SetImageResource(Items[position].ImageId);
			icon.SetColorFilter(Color.Argb(255, 255, 255, 255));

			return row;
		}

		protected override void Dispose (bool disposing)
		{
			context = null;
			base.Dispose (disposing);
		}
	}
}