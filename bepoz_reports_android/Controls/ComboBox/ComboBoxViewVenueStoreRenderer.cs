using System;
using System.Collections.Generic;

using Android.Views;
using Android.Widget;
using Android.Renderscripts;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_reports.Droid.Controls.Calendar;
using bepoz_reports.Report;
using bepoz_reports.Controls;
using bepoz_reports.Venue;
using bepoz_reports.Models;
using bepoz_reports.Store;

[assembly: ExportRenderer(typeof(bepoz_reports.Controls.ComboBoxMixedView), typeof(bepoz_reports.Droid.Controls.ComboBoxViewVenueStoreRenderer))]
namespace bepoz_reports.Droid.Controls
{
	public class ComboBoxViewVenueStoreRenderer : ViewRenderer<bepoz_reports.Controls.ComboBoxMixedView, LinearLayout>
	{
		private List<CustomPickerObject> stores = new List<CustomPickerObject>();
		private List<CustomPickerObject> venues = new List<CustomPickerObject>();

		private SpinnerAdapter adapter;
		private Spinner spinner;

		private int currentPickerID = 0;
		private string selectedTitle = "";

		bepoz_reports.Controls.ComboBoxMixedView _view;
		CalendarPickerView _pickerView;

		public ComboBoxViewVenueStoreRenderer()
		{
			PickerModelChanger.ComboViewVenueStore = this;
		}

		protected override void OnElementChanged(ElementChangedEventArgs<bepoz_reports.Controls.ComboBoxMixedView> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				_view = e.NewElement;

				LayoutInflater inflatorservice = (LayoutInflater)Context.GetSystemService(Android.Content.Context.LayoutInflaterService);
				var containerView = (LinearLayout)inflatorservice.Inflate(Resource.Layout.combox_box_view, null, false);

				createVenueList ();
				createStoreList ();

				adapter = new SpinnerAdapter (venues, Context);

				spinner = containerView.FindViewById<Spinner> (Resource.Id.spinner);
				spinner.Adapter = adapter;

				spinner.ItemSelected += (object sender, AdapterView.ItemSelectedEventArgs ev) => {
					switch (currentPickerID) 
					{
						case 0:
							_view.NotifyVenueSelected(venues[ev.Position].Title);
							break;

						case 1:
							_view.NotifyStoreSelected(stores[ev.Position].Title);
							break;
					}
				};

				SetNativeControl(containerView);
			}
		}

		public void ChangePickerModels(int pickerID)
		{
			currentPickerID = pickerID;

			string selection = "";

			// show selected picker
			switch (currentPickerID) 
			{
				case 0:
					adapter.Items = venues;
					adapter.NotifyDataSetChanged ();
					selection = "Venues";
					break;

				case 1:
					//createStoreList ();
					adapter.Items = stores;
					adapter.NotifyDataSetChanged ();
					selection = "Stores";
					break;
			}

			spinner.SetSelection (0);

			System.Diagnostics.Debug.WriteLine(selection + " picker selected.");
		}

		private void createVenueList()
		{
			// create venue picker items
			foreach (DTOGroupItem venueItem in VenueManager.Venues) 
			{
				// Venue Group
				venues.Add (new VenueStorePickerObject(){Title = venueItem.GroupName, Type = 0, GroupType = 0, VenueGroup = venueItem.GroupID, ImageId = Resource.Drawable.group});

				// Venues
				foreach (DTOItem groupItemDTO in venueItem.Children)
					venues.Add (new VenueStorePickerObject(){Title = groupItemDTO.Name, Type = 1, GroupType = 0, VenueGroup = groupItemDTO.GroupID, Venue = groupItemDTO.ID, ImageId = Resource.Drawable.item});
			}

			System.Diagnostics.Debug.WriteLine ("Created venues list with " + venues.Count + " objects");

		}

		// create stores from selected venue
		private void createStoreList()
		{
			stores.Clear ();

			// create store picker items
			foreach (DTOGroupItem storeItem in StoreManager.Stores) 
			{
				// Store Group
				stores.Add (new VenueStorePickerObject(){Title = storeItem.GroupName, Type = 0, GroupType = 1, StoreGroup = storeItem.GroupID, ImageId = Resource.Drawable.group});

				// Stores
				foreach (DTOItem groupItemDTO in storeItem.Children) {
					stores.Add (new VenueStorePickerObject () {
						Title = groupItemDTO.Name,
						Type = 1,
						GroupType = 1,
						StoreGroup = groupItemDTO.GroupID,
						Store = groupItemDTO.ID,
						ImageId = Resource.Drawable.item});
				}
			}

			System.Diagnostics.Debug.WriteLine ("Created stores list with " + stores.Count + " objects");
		}
	}
}