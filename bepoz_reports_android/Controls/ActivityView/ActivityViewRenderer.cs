using System;

using Android.Views;
using Android.Widget;
using Android.Renderscripts;
using Android.App;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_reports.Controls;

[assembly: ExportRenderer(typeof(bepoz_reports.Controls.ActivityView), typeof(bepoz_reports.Droid.Controls.ActivityViewRenderer))]
namespace bepoz_reports.Droid.Controls
{
	public class ActivityViewRenderer : ViewRenderer<bepoz_reports.Controls.ActivityView, LinearLayout>
    {
		ActivityView _view;

		public ActivityViewRenderer()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<bepoz_reports.Controls.ActivityView> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				_view = e.NewElement;

				LayoutInflater inflatorservice = (LayoutInflater)Context.GetSystemService(Android.Content.Context.LayoutInflaterService);
				var containerView = (LinearLayout)inflatorservice.Inflate(Resource.Layout.progress_activity, null, false);

				SetNativeControl(containerView);
			}
		}
    }
}