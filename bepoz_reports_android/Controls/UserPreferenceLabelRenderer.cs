using System;
using System.ComponentModel;
using System.Drawing;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Android.Content;
using Android.Preferences;
using Android.App;

using bepoz_reports.Controls;
using bepoz_reports.IOS.Controls;
using bepoz_reports.Account;

[assembly: ExportRenderer(typeof(UserPreferenceLabel), typeof(UserPreferenceLabelRenderer))]
namespace bepoz_reports.IOS.Controls
{
    using System.Threading.Tasks;

	public partial class UserPreferenceLabelRenderer : LabelRenderer
    {	
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			// store username in shared preference
			var prefs = Application.Context.GetSharedPreferences("bepoz_reports", FileCreationMode.Private);
			var prefEditor = prefs.Edit();
			prefEditor.PutString("RememberKey", AccountManager.Username);
			prefEditor.Commit();

			System.Diagnostics.Debug.WriteLine ("Synchronize IOS StandardUserDefaults.");
		}
    }
}